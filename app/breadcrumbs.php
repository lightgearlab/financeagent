<?php

Breadcrumbs::register('home', function($breadcrumbs) {
    $breadcrumbs->push('Home', URL::to('/'));
});

Breadcrumbs::register('product.show', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Product', route('product.show'));
});


Breadcrumbs::register('profile.show', function($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Profile', route('profile.show'));
});

