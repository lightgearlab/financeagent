<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductcategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('productcategory', function(Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('uid')->nullable();
			$table->string('name')->nullable();
			$table->string('link')->nullable();
			$table->text('description')->nullable();
			$table->string('photo_url')->nullable();
			// $table->string('city')->nullable();
			$table->string('createdby')->nullable();
			$table->string('modifiedby')->nullable();
			$table->boolean('active')->default(true);	
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('productcategory');
	}

}
