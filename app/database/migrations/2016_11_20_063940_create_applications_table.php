<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('applications', function(Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('code')->nullable();
			$table->integer('applicant_id')->unsigned()->index(); 
			$table->integer('application_status_id')->unsigned()->index();   
            $table->integer('userscore_id')->unsigned()->index();   
			$table->boolean('isExistingCustomer')->default(false);
            $table->boolean('active')->default(true);

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('applications');
	}

}
