<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function(Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('uid');
			// $table->integer('productcategory_id')->unsigned()->index();
			// $table->integer('agent_id')->unsigned()->index();
			// $table->foreign('agent_id')->references('id')->on('agents')->onDelete('cascade');
			$table->string('name')->nullable();
			$table->text('description')->nullable();
			$table->string('photo_url')->nullable();
			// $table->boolean('primary')->default(true);
            $table->text('feecharges')->nullable();
            $table->text('requirements')->nullable();
            $table->text('reviews')->nullable();
			$table->string('createdby')->nullable();
			$table->string('modifiedby')->nullable();
			$table->boolean('active')->default(true);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('products');
	}

}
