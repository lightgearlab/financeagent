<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('profiles', function(Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('uid');
			$table->integer('user_id')->unsigned()->index();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->string('first_name')->nullable();
			$table->string('last_name')->nullable();
			$table->string('name')->nullable();
			$table->string('phonenumber')->nullable();
			$table->integer('age')->nullable();
			$table->string('gender')->nullable();
            $table->string('photo_url')->nullable();
			$table->text('biography')->nullable();
			$table->boolean('photointernal')->default(true);
            $table->integer('location_id')->unsigned()->index();
			$table->float('rating_cache')->default(3.5);
		    $table->integer('rating_count')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('profiles');
	}

}
