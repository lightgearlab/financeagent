<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('uid');
            $table->string('email')->nullable();
			$table->string('password')->nullable();		
			$table->string('remember_token')->nullable();
			$table->string('devicesys')->nullable();
			$table->text('devicetoken')->nullable();
			$table->integer('logincounter')->default(0);
			$table->datetime('logindate')->nullable();
			$table->datetime('logoutdate')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
