<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUserscoreTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('userscore', function(Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('productcategory_id')->unsigned()->index();        
			$table->double('monthlyincome')->nullable();
			$table->integer('loantenure')->nullable();
			$table->integer('interestrate_id')->unsigned()->index();     
			$table->double('totalhousefinance')->nullable();
			$table->double('totalvehiclefinance')->nullable();
			$table->double('totalpersonalfinance')->nullable();
			$table->double('totalcreditcardfinance')->nullable();
            $table->double('monthlyrepayment')->nullable();
			$table->double('maximumloan')->nullable();
			$table->double('maximummonthlyrepayment')->nullable();
			$table->string('user')->nullable();
			$table->boolean('status')->default(false);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('userscore');
	}

}
