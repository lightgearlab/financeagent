<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('messages', function(Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
            $table->integer('user_id')->unsigned()->index();     
            $table->integer('conversation_id')->unsigned()->index();          
            //$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            //$table->foreign('conversation_id')->references('id')->on('conversations')->onDelete('cascade');    	
            $table->text('content')->nullable();
            $table->softDeletes();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('messages');
	}

}
