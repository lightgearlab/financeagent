<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMessageStatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('message_states', function(Blueprint $table) {
		
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index();           
            $table->integer('message_id')->unsigned()->index();           
			//$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            //$table->foreign('message_id')->references('id')->on('messages')->onDelete('cascade');
        
            $table->integer('state');

			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('message_states');
	}

}
