<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsermessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('usermessages', function(Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->integer('sender_id')->unsigned()->index();
			$table->integer('receiver_id')->unsigned()->index();
			$table->string('from')->nullable();
			$table->string('to')->nullable();
			$table->string('subject')->nullable();
			$table->text('body')->nullable();
			$table->boolean('send')->default(false);	
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('usermessages');
	}

}
