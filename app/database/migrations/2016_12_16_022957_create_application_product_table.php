<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicationProductTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('application_product', function(Blueprint $table) {
            $table->engine = 'InnoDB';
			$table->increments('id');
            $table->integer('application_id')->unsigned()->index();
			$table->foreign('application_id')->references('id')->on('applications')->onDelete('cascade');
            $table->integer('productcategory_id')->unsigned()->index();
			$table->foreign('productcategory_id')->references('id')->on('productcategory')->onDelete('cascade');
            $table->integer('product_id')->unsigned()->index();
			$table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->integer('agent_id')->unsigned()->index();
            $table->foreign('agent_id')->references('id')->on('agents')->onDelete('cascade');            
            $table->boolean('active')->default(true);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('application_product');
	}

}
