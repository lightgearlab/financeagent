<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicationDocumentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('application_document', function(Blueprint $table) {
            $table->engine = 'InnoDB';
			$table->increments('id');
            $table->integer('application_id')->unsigned()->index();
			$table->foreign('application_id')->references('id')->on('applications')->onDelete('cascade');
            $table->integer('document_id')->unsigned()->index();
			$table->foreign('document_id')->references('id')->on('documents')->onDelete('cascade');
            $table->boolean('active')->default(true);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('application_document');
	}

}
