<?php

class ProductCategoryTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('role')->truncate();

		ProductCategory::create(array(
			'uid'	=>	Uuid::generate()->string,
			'name'     => 'Insurance',
			'createdby'	=> 'Administrator',
			'modifiedby' => 'Administrator',
			'photo_url'	=> '/images/products/main/sample.jpg'
			
		))->save();
		
		ProductCategory::create(array(
			'uid'	=>	Uuid::generate()->string,
			'name'     => 'Credit Card',
			'createdby'	=> 'Administrator',
			'modifiedby' => 'Administrator',
			'photo_url'	=> '/images/products/main/sample.jpg'
		))->save();


		ProductCategory::create(array(
			'uid'	=>	Uuid::generate()->string,
			'name'     => 'Hire Purchase',
			'createdby'	=> 'Administrator',
			'modifiedby' => 'Administrator',
			'photo_url'	=> '/images/products/main/sample.jpg'
		))->save();


		ProductCategory::create(array(
			'uid'	=>	Uuid::generate()->string,
			'name'     => 'Mortgage',
			'createdby'	=> 'Administrator',
			'modifiedby' => 'Administrator',
			'photo_url'	=> '/images/products/main/sample.jpg'
		))->save();

		// Uncomment the below to run the seeder
		// DB::table('role')->insert($role);
	}

}