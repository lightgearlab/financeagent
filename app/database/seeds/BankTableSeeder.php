<?php

class BankTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('role')->truncate();

		Bank::create(array(
			'name'     => 'Maybank'
		))->save();
		
		Bank::create(array(
			'name'     => 'CIMB'
		))->save();


		Bank::create(array(
			'name'     => 'Bank Rakyat'
		))->save();

		// Uncomment the below to run the seeder
		// DB::table('role')->insert($role);
	}

}