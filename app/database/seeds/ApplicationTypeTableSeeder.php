<?php

class ApplicationTypeTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('role')->truncate();

		ApplicationType::create(array(
			'Id'	=> '1',
			'name'     => 'Credit Card'
		))->save();
		
		ApplicationType::create(array(
			'Id'	=> '2',
			'name'     => 'Personal Loan'
		))->save();


		ApplicationType::create(array(
			'Id'	=> '3',
			'name'     => 'Hire Purchase'
		))->save();


		ApplicationType::create(array(
			'Id'	=> '4',
			'name'     => 'Insurance'
		))->save();


		// Uncomment the below to run the seeder
		// DB::table('role')->insert($role);
	}

}