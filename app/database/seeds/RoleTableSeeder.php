<?php

class RoleTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('role')->truncate();

		Role::create(array(
			'name'     => 'Administrator'
		))->save();
		
		Role::create(array(
			'name'     => 'Visitor'
		))->save();


		Role::create(array(
			'name'     => 'Agent'
		))->save();

		// Uncomment the below to run the seeder
		// DB::table('role')->insert($role);
	}

}