<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('RoleTableSeeder');
		$this->call('BankTableSeeder');
		$this->call('ProductCategoryTableSeeder');
		$this->call('ProductTableSeeder');	
        $this->call('LocationTableSeeder');	
		$this->call('UserTableSeeder');
		$this->call('ApplicationTypeTableSeeder');
		$this->call('ApplicationStatusTableSeeder');
		$this->call('AgentProductTableSeeder');
        $this->call('InterestRateTableSeeder');
}

}
