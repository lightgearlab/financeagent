<?php

class LocationTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('role')->truncate();

		Location::create(array(
			'city'      => 'Petaling Jaya',
            'state'     => 'Selangor',
            'country'   => 'Malaysia'
		))->save();
		
		Location::create(array(
			'city'      => 'Cheras',
            'state'     => 'Kuala Lumpur',
            'country'   => 'Malaysia'
		))->save();
        

		// Uncomment the below to run the seeder
		// DB::table('role')->insert($role);
	}

}