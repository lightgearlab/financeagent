<?php

class UserTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		//DB::table('users')->truncate();

		//** Dummy User 1 **//
		$user = User::create(array(
				'uid'	   => Str::random(40),
				'email'	   => 'administrator@financeagent.co',
				'password' => Hash::make('Su74P@ssw0rd1')
				));

		$roles = $user->roles();

		$roles->save(Role::find(1));

		$profile = new Profile;
		$profile->name = "Administrator";
		$profile->user_id = $user->id;
		$profile->uid = Str::random(20);
		$profile->photo_url = "images/users/default.png";
		$profile->biography = "I will provide good service and assist your online application smoothly";
		$profile->photointernal = true;
		$profile->user()->associate($user);
        $profile->location()->associate(Location::find(rand(1, 2)));
		$profile->save();
		

		//** Dummy User 2 **//

		$user = User::create(array(
			'uid'	   => Str::random(40),
			'email'	   => 'visitor1@abc.com',
			'password' => Hash::make('password')
		));

		$roles = $user->roles();

		$roles->save(Role::find(2));

		$profile = new Profile;
		$profile->name = "Visitor 1";
		$profile->user_id = $user->id;		
		$profile->uid = Str::random(20);
		$profile->photo_url = "images/users/default.png";
		$profile->biography = "I will provide good service and assist your online application smoothly";
		$profile->photointernal = true;
		$profile->user()->associate($user);
        $profile->location()->associate(Location::find(rand(1, 2)));
		$profile->save();


		for ($i = 1; $i <= 20; $i++) {

			$email = 'agent' . $i . '@abc.com';

			//** Dummy User 3 **//
			$user = User::create(array(
				'uid'	   => Str::random(40),
				'email'	   => $email,
				'password' => Hash::make('password')
			));

			$roles = $user->roles();

			$roles->save(Role::find(3));

			$profile = new Profile;
			$profile->name = 'Agent ' . $i ;
			$profile->user_id = $user->id;		
			$profile->uid = Str::random(20);
			$profile->photo_url = "images/users/default.png";
			$profile->biography = "I will provide good service and assist your online application smoothly";
			$profile->photointernal = true;
			$profile->user()->associate($user);
            $profile->location()->associate(Location::find(rand(1, 2)));
			$profile->save();

			$agent = new Agent;
			$agent->uid = Str::random(10);
			$agent->user()->associate($user);
			$agent->bank()->associate(Bank::find(3));		
			$agent->save();

//			$location = new  Location;
//
//			$location->address = "38-56, Jalan 14/36, Seksyen 14, 46100 Petaling Jaya, Selangor, Malaysia";
//			$location->lat = 3.1058527;
//			$location->lng = 101.6323918;
//			$location->city = "Petaling Jaya";
//			$location->state = "Selangor";
//			$location->country = "Malaysia";
//			
//			$location->user()->associate($user)->save();
            
            
//            $location = Location::find(rand(1, 2));
//			
//			$userLocation = new UserLocation;
//			$userLocation->location()->associate($location);
//			$userLocation->user()->associate($user);
//			$userLocation->save();
	

		} 

		

		// Uncomment the below to run the seeder
		// DB::table('users')->insert($users);
	}

}
