<?php

class ApplicationStatusTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('role')->truncate();

		ApplicationStatus::create(array(
			'id'	=> '1',
			'name'     => 'Approved'
		))->save();
		
		ApplicationStatus::create(array(
			'id'	=> '2',
			'name'     => 'Rejected'
		))->save();


		ApplicationStatus::create(array(
			'id'	=> '3',
			'name'     => 'Pending'
		))->save();


		ApplicationStatus::create(array(
			'id'	=> '4',
			'name'     => 'Cancelled'
		))->save();


		ApplicationStatus::create(array(
			'id'	=> '5',
			'name'     => 'Incompleted'
		))->save();

		// Uncomment the below to run the seeder
		// DB::table('role')->insert($role);
	}

}