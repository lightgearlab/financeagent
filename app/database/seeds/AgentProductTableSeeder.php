<?php

class AgentProductTableSeeder extends Seeder {

	public function run()
	{



		for ($j = 1; $j <= 20; $j++) {

			$product = Product::find(rand(1, 20));

			$agent = Agent::find(rand(1, 20));

			$productcategory = ProductCategory::find(rand(1, 4));
			
			$agentProduct = new AgentProduct;
			$agentProduct->product()->associate($product);
			$agentProduct->agent()->associate($agent);
			$agentProduct->productcategory()->associate($productcategory);
			$agentProduct->description = 'Best offer contact me.';
			$agentProduct->save();
		}

		// Uncomment the below to run the seeder
		// DB::table('users')->insert($users);
	}

}
