<?php

class InterestRateTableSeeder extends Seeder {

	public function run()
	{
		// Uncomment the below to wipe the table clean before populating
		// DB::table('role')->truncate();

		InterestRate::create(array(
			'Id'	=> '1',
			'name'     => 'Credit Card'
		))->save();
		
		InterestRate::create(array(
			'Id'	=> '2',
			'name'     => 'Personal Financing'
		))->save();

		InterestRate::create(array(
			'Id'	=> '3',
			'name'     => 'Health Insurance'
		))->save();

		InterestRate::create(array(
			'Id'	=> '4',
			'name'     => 'Micro Financing'
		))->save();
        
        InterestRate::create(array(
			'Id'	=> '5',
			'name'     => 'Fixed Deposi'
		))->save();
        
        InterestRate::create(array(
			'Id'	=> '6',
			'name'     => 'Wealth Management'
		))->save();


		// Uncomment the below to run the seeder
		// DB::table('role')->insert($role);
	}

}