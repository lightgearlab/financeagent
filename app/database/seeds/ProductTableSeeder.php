<?php

class ProductTableSeeder extends Seeder {

	public function run()
	{
	
		for ($j = 1; $j <= 20; $j++) {
			$product = new Product;
			$product->uid = Uuid::generate()->string;
  			$product->name = 'Product ' . $j;
  			$product->description = "Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.";
  			$product->photo_url = "/images/products/sub/sample.png";
  			$product->createdby	= 'Administrator';
			$product->modifiedby = 'Administrator';
  			$product->save();
  		}
			
	}

}
