<section class="header-10-sub v-center">               

    <div class="container">

	<h1>User profile</h1>

	{{ Form::model($profile, array('method' => 'PATCH', 'route' => array('profile.update', $profile->id), 'files' => true)) }}



	<div class="form-group">

		<label for="">avatar</label>

	    <div class="profile-avatar-wrap">

				<img src="{{ $profile->getAvatar() }}" class="img-rounded" id="profile-avatar" alt="Image for Profile">

		</div>

	  		<input type="file" id="uploader" name="uploader">

	  </div>



		  <div class="form-group">

	    <label for="txtFirstName">First Name</label>

	    <input id="txtFirstName" name="txtFirstName" type="text" class="form-control" value="{{ $profile->first_name }}"  placeholder="First Name">

	  </div>

	   	  <div class="form-group">

	    <label for="txtLastName">Last Name</label>

	    <input id="txtLastName" name="txtLastName" type="text" class="form-control" value="{{ $profile->last_name }}"  placeholder="Last Name">

	  </div>

		  <div class="form-group">

	    <label for="txtName">Name</label>

	    <input id="txtName" name="txtName" type="text" class="form-control" value="{{ $profile->name }}"  placeholder="Name">

	  </div>

	  <div class="form-group">

	    <label for="txtEmail">Email</label>

	    <input id="txtEmail" name="txtEmail"  type="email" class="form-control" value="{{ $profile->user->email }}" placeholder="mail@abc.com">

	  </div>

	   <div class="form-group">

	    <label for="txtAge">Age</label>

	    <input id="txtAge" name="txtAge"  type="text" class="form-control" value="{{ $profile->age }}" placeholder="age">

	  </div>

		  <div class="form-group">

	    <label for="txtPhoneNumber">Phone Number</label>

	    <input id="txtPhoneNumber" name="txtPhoneNumber"  type="text" class="form-control" value="{{ $profile->phonenumber }}" placeholder="+60-XXXXXXXX">

	  </div>

	  <div class="form-group">

	    <label for="ddlGender">Gender</label>



	    {{ Form::select('ddlGender',array('male' => 'Male', 'female' => 'Female'), $profile->gender , ['class' => 'form-control']) }}

	     

	  </div>

	  <button type="submit" class="btn btn-primary">Save</button>

	  {{ link_to_route('profile.show', 'Cancel' ,array($profile->id), array('class' => 'btn btn-default')) }}

	{{Form::close()}}



	    </div>

</section>