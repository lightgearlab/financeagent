<section class="profile-page">

<div class="container">

			<div class="row">

				<div class="col-md-12">      

					<ol class="breadcrumb">

						<li><a href="#">Home</a></li>

						<li class="active">Profile</li>

					</ol>

				</div>



			</div>

            

            

			<div class="row">

				<div class="col-md-8 agent-details-left">



						<div class="col-md-12">

							<div class="col-md-3 paddingzero">

								<img src="{{ $profile->getAvatar() }}" width="150" class="img-rounded">

							</div>

							<div class="col-md-7">

								<h3>{{ $profile->name }} <span><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> verified </span></h3>

								<p>

									{{ $profile->biography }}

								</p>

								<p>Insurance</p>

		                        <p>Maybank</p>

		                        <p>Petaling Jaya 1.5km from you</p>



		                        @if(Auth::check() && $profile->user->isLoginUser())

								{{ link_to_route('profile.edit', 'Edit Profile' ,array(Auth::user()->profile->id), array('class' => 'btn btn-primary')) }}

								@endif

							</div> 

						</div>

				</div>

			</div>



			<div class="row">

			

			</div>

</div>

</section>