<div class="col-lg-6 mt20">
       <form id="reviewForm" name="reviewForm"  action="<?=URL::to('/review') . '/' . $profile->id ?>" method="POST">
        {{Form::hidden('rating', null, array('id'=>'ratings-hidden'))}}

    <div class="col-lg-12 feed-back-advice  bg-white">
     
        <div class="container-fluid mt20">
            <h4>Ratings</h4>
        </div>
        <div class="container-fluid">
            <div class="col-lg-9 pd0">
                <p class="font-thin">Submission & testimonial rating is for internal use of bank rakyat to provide your the best support and service</p>
            </div>
        </div>
        <div class="container-fluid mt10">
            <div class="form-group">
                <label for="comment ">Feedback</label>
                {{Form::textarea('comment', null, array('rows'=>'5','id'=>'new-review','class'=>'form-control animated','placeholder'=>'Enter your review here...'))}}

                
            </div>
        </div>
        <div class="container-fluid">
            <button type="submit" class="btn btn-primary">Submit</button>
            <div class="pull-right mt10 stars starrr" data-rating="{{Input::old('rating',0)}}"></div>
         
        </div>
        
    </div>
</form>
   
</div>


@section('footerscript')
    
 {{HTML::script('js/expanding.js')}}

 {{HTML::script('js/starrr.js')}}

 <script type="text/javascript">

  $(function(){


         var ratingsField = $('#ratings-hidden');

         // Bind the change event for the star rating - store the rating value in a hidden field

         $('.starrr').on('starrr:change', function(e, value){

           ratingsField.val(value);

         });


      });

 </script>

@append