<section class="profile-page">

<div class="container">

<div class="row">

   <div class="col-md-8 agent-details-left">

      <!--agent details-->                 

      <div class="col-md-12 agent-detail-section">

         <div class="col-md-3 paddingzero">

            <img src="{{ $profile->getAvatar() }}" width="150" class="img-rounded pull-left">

         </div>

         <div class="col-md-9 agent-info">

            <h4 class="disabled">{{ $profile->getDisplayName() }}</h4>

            <div class="agent-rating">{{  $profile->getRating() }}/5</div>

            <p class="disabled">{{ $profile->biography }}</p>

           

            @if($profile->user->location != null)

                  <p>

                     {{ $profile->user->location->address }} 

                     <br>

                     @if(Auth::check() && Auth::user()->hasLocation() && !$profile->user->isLoginUser())

                     {{ $profile->getDistance() }} 

                     @endif

                  </p>

            @endif

            <!-- <div class="ratings">



              @if($profile->rating_count != null)

               <p class="pull-right">{{$profile->rating_count}} {{ Str::plural('review', $profile->rating_count);}}</p>

              @endif 

               <p>

                  @for ($i=1; $i <= 5 ; $i++)

                  <span class="glyphicon glyphicon-star rate-star{{ ($i <= $profile->rating_cache) ? '' : '-empty'}}"></span>

                  @endfor

                  {{ number_format($profile->rating_cache, 1);}} stars

               </p>

            </div> -->

            

            @if(!$profile->user->isLoginUser() && $profile->user->hasEmail())

            <div class="col-md-6 paddingzero">

              <a id="lnkMessage" href="<?=URL::to('/profile/message') . '/'. $profile->id ?> " class="btn btn-contact-me messageme">Message</a>

            </div>

            @endif



            @if($user->isLoginUser())

               

            <div class="col-md-6 paddingzero">

               <a id="lnkEditProfile" class="pull-right" href=""><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>

            </div>

            @endif

         </div>

      </div>

      <!-- End Agent Detail -->

      

       <!--agent details-->

      <!--This Editing Form show after click edit-->

      <div id="divEditProfile" class="col-md-12" style="display:none;">

            @include("profile._edit",array('profile' => $profile))

      </div>





        <div class="col-md-12">

            @include('profile.review', array('profile' => $profile))

      </div>

  



   </div>







</div>

</div>

</section>





<script type="text/javascript">

   

    $(".messageme").click(function(e){

           e.preventDefault();



           var isLogin = '<?=Auth::check()?>';



           if(isLogin){

              $.ajax({

                  type: "GET",

                  url : $(this).attr('href'),

                  cache: false,

                  success : function(data){

                      

                     $("#dialog-edit-body").html(data.result);



                     $("#dialog-edit").modal({ show: true });

                  },

                  error: function(xhr, textStatus, errorThrown){

                     console.log(xhr.responseText);

                  }

              });



              }else{

                 window.location.href = '<?=URL::to('/login')?>'; 

              }

   });

    

   $("#lnkEditProfile").click(function(e){



      e.preventDefault();

      $("#divEditProfile").slideToggle( "slow", function(){



        if ($(this).is(':visible')) {

           $("html, body").animate({scrollTop: $(this).offset().top});

        }

      });



   });



   



</script>