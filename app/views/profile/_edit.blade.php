<div class="panel panel-default">

            <div class="panel-heading">

               <h3 class="panel-title">Edit Profile</h3>

            </div>

            <div class="panel-body">

               <div id="divError" style="display:none;" class="error">
                  Error
                  <p>
                    <ul id="errorlist"></ul>
                  </p>
               </div>

               <div class="form-group col-md-3 paddingzero">

                  <img src="{{ $profile->getAvatar() }}" id="profile-avatar" alt="Image for Profile" class="pull-left" />

               </div>

               {{ Form::model($profile, array('id' => 'formProfile' ,'method' => 'PATCH',  'route' => array('profile.update', $profile->uid), 'files' => true)) }}



               <div class="col-md-9">

                  <span class="file-input btn btn-upload btn-file">

                        Upload Photo <input type="file" id="uploader" name="uploader" multiple/>

                  </span>

               </div>

               <div class="col-md-12 paddingzero">

               <div class="form-group">

                  <label for="txtName">Name</label>

                  <input id="txtName" name="name" type="text" class="form-control" value="{{ $profile->name }}"  placeholder="Name" required="required">

               </div>

               <div class="form-group">

                <label for="txtBiography">Biograph</label>

                      <textarea id="txtBiography" name="biography" rows="4" cols="20" class="form-control" placeholder="140 Words" required="required" >{{ $profile->biography }}</textarea>

               </div>

               <div class="form-group">

                   <label for="txtLocation">Location</label>

                   <input id="txtLocation" name="location"  type="text" class="form-control" value="{{ ($user->location != null ? $user->location->address : '') }}" placeholder="" >

                   <input id="hidLat" name="hidLat" type="hidden" value="">

                   <input id="hidLng" name="hidLng" type="hidden" value="">

               </div>



               @if($profile->user->isAgent())

               <div class="form-group">

                     <label for="ddlBank">Company</label>

                     {{ Form::select('ddlBank', ['' => 'Select'] + $banks , ( $agent->hasBank() ? $agent->bank->id :  null),['class' => 'form-control']) }}

                       

               </div>

               @endif

               <div class="form-group">

                  <label for="txtEmail">Email</label>

                  <input id="txtEmail" name="email"  type="email" class="form-control" value="{{ $profile->user->email }}" placeholder="mail@abc.com" required="required" >

               </div>

               <div class="form-group">

                  <label for="txtPhoneNumber">Phone Number</label>
                  <div class="input-group">
                  <div class="input-group-addon">+60</div>
                  <input id="txtPhoneNumber" name="phonenumber"  type="tel" class="form-control" value="{{ $profile->phonenumber }}" placeholder="XXXXXXXX">
                </div>
               </div>

               

               

               <button id="btnSave" name="btnSave"  type="submit" class="btn btn-success pull-right">Save</button>

               {{Form::close()}}

               </div>

            </div>


    <script type="text/javascript">

$(document).ready(function(){
        $('#formProfile').validate({ // initialize the plugin

                rules: {

                    name: {

                        required: true

                    },

                    biography: {

                        required: true

                    },

                    email: {
                       required: true
                    },
                    phonenumber:{
                       number: true
                    }

                },

                messages: {

                    name: {

                       required: '* Name is required.'

                    },

                    biography: {

                       required: '* Biography is required.'

                    },

                    email: {

                       required: '* Email is required.'

                    },
                    phonenumber: {

                       number: '* Please enter a valid number.'

                    }

                 },

                submitHandler: function(form) { 

                    $.ajax( {
                    url: $(form).attr('action'),
                    type: 'POST',
                    data: new FormData( form ),
                    processData: false,
                    contentType: false,
                     success: function(data, textStatus, jqXHR){           
                     
                   
                    if(!data.error){
                      window.location.href = data.result;
                    }else{

                      $("#errorlist").empty();
                      $.each(data.messages, function(i, val) {
                        $("#errorlist").append("<li>" + val + "</li>");
                      });
                      $("#divError").show();
                      
                    }
                  },

                  error: function(xhr, textStatus, errorThrown){

                     console.log(xhr.responseText);

                  }
                  } );

                }

            });

});

    
            
    </script>
</div>


