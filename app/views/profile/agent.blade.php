<section class="profile-page">

<div class="container">

<div class="row">

   <div class="col-md-8 agent-details-left">

      <!--agent details-->                 

      <div class="col-md-12 agent-detail-section">

         <div class="col-md-3 paddingzero">

            <img src="{{ $profile->getAvatar() }}" class="img-rounded" width="150" class=" pull-left">

         </div>

         <div class="col-md-9 agent-info">

            <h4 class="disabled">{{ $profile->getDisplayName() }}</h4>

            <div class="agent-rating">{{  $profile->getRating() }}/5</div>

            <p class="disabled">{{ $profile->biography }}</p>

            @if($agent != null && $agent->bank != null) 

                  <p><strong>{{ $agent->bank->name }}</strong></p>

            @endif

            @if($profile->user->location != null)

                  <p>

                     {{ $profile->user->location->address }} 

                     @if(Auth::check() && Auth::user()->hasLocation() && !$profile->user->isLoginUser())

                     {{ $profile->getDistance() }} 

                     @endif

                  </p>

            @endif

            <!-- <div class="ratings">



              @if($profile->rating_count != null)

               <p class="pull-right">{{$profile->rating_count}} {{ Str::plural('review', $profile->rating_count);}}</p>

              @endif 

               <p>

                  @for ($i=1; $i <= 5 ; $i++)

                  <span class="glyphicon glyphicon-star rate-star{{ ($i <= $profile->rating_cache) ? '' : '-empty'}}"></span>

                  @endfor

                  {{ number_format($profile->rating_cache, 1);}} stars

               </p>

            </div> -->



            @if(!$agent->user->isLoginUser() )

            <div class="col-md-6 paddingzero">

              <a id="lnkMessage" href="<?=URL::to('/profile/message') . '/'. $profile->id ?> " class="btn btn-contact-me messageme">Message</a>

            </div>

            @endif

            @if($user->isLoginUser())

               

            <div class="col-md-12">

               <a id="lnkEditProfile" class="pull-right" href="">Edit &nbsp; &nbsp;<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>

            </div>

            @endif

         </div>

      </div>

      <!-- End Agent Detail -->

      

       <!--agent details-->

      <!--This Editing Form show after click edit-->

      <div id="divEditProfile" class="col-md-12" style="display:none;">

            @include("profile._edit",array('profile' => $profile, 'agent' => $agent))

      </div>

         @if($user->isLoginUser())

            <a id="lnkAddProduct" class="pull-right" href="<?=URL::to('/product/create') ?>">
              <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
              Add Product
            </a>

            @endif

      @if($agent->hasMainProduct())

      <!-- Start Main PRoduct Display -->

      <div class="col-md-12 main-product">

            <div class="col-md-12 title-header"><h5>{{ $agent->getMainProduct()->name }}</h5>

         

            </div>



         <div class="row">

            

            <div class="col-md-4">

               <a href="#" class="thumbnail">

               <img src="{{ $agent->getMainProduct()->getImageUrl() }}"  class="img-rounded" alt="...">

               </a>

            </div>



            <div class="col-md-8">

               <p class="description">{{ $agent->getMainProduct()->description }}</p>

                @if($user->isLoginUser())

                <a class="editproduct pull-right" href="{{ URL::route('product.edit', array('id' => $agent->getMainProduct()->id)) }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>

                @endif

            </div> 



         </div>



      </div>

      <!-- End Main PRoduct Display -->

      @endif



      @if($agent->hasOtherProduct())

      <!-- Start Other PRoduct Display -->

      <div class="col-md-12 other-products">

         <div class="col-md-12 title-header">

         <h5>Other Products</h5>

         </div>

         <div class="row">

            @for($i = 0; $i < count($agent->getOtherProduct()); $i++) 

            <div class="col-xs-6 col-md-4">

               <a href="{{ URL::route('product.show', array('id' => $agent->getOtherProduct()[$i]->id)) }}" class="thumbnail">

               <img src="{{ $agent->getOtherProduct()[$i]->getImageUrl() }}" class="img-rounded" alt="...">

               </a>

               <p class="description">{{ $agent->getOtherProduct()[$i]->description }}</p>

               @if($user->isLoginUser())

               <a class="editproduct pull-right" href="{{ URL::route('product.edit', array('id' => $agent->getOtherProduct()[$i]->id)) }}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>

               @endif

            </div>

            @endfor

            

         </div>

      </div>

      <!-- End Other PRoduct Display -->

      @endif

      <div id="divEditProduct" class="col-md-12" style="display:none;"></div>    



       <div class="col-md-12">

            @include('profile.review', array('profile' => $profile))

      </div>



   </div>







</div>

</div>

</section>





<script type="text/javascript">



   $(".messageme").click(function(e){

           e.preventDefault();



           var isLogin = '<?=Auth::check()?>';



           if(isLogin){

              $.ajax({

                  type: "GET",

                  url : $(this).attr('href'),

                  cache: false,

                  success : function(data){

                      

                     $("#dialog-edit-body").html(data.result);



                     $("#dialog-edit").modal({ show: true });

                  },

                  error: function(xhr, textStatus, errorThrown){

                     console.log(xhr.responseText);

                  }

              });



              }else{

                 window.location.href = '<?=URL::to('/login')?>'; 

              }

   });

   

   $("#lnkEditProfile").click(function(e){



      e.preventDefault();

      $("#divEditProfile").slideToggle( "slow", function(){



        if ($(this).is(':visible')) {

           $("html, body").animate({scrollTop: $(this).offset().top});

        }

      });



   });



   $("#lnkAddProduct").click(function(e){



      e.preventDefault();



      if ($("#divEditProduct").is(':visible'))

         $("#divEditProduct").toggle();



      $.get($(this).attr('href'), function( data ) {

      

         $('#divEditProduct').html( data.result );

          

         $("#divEditProduct").slideToggle( "slow", function(){

           if ($(this).is(':visible')) {

              $("html, body").animate({scrollTop: $(this).offset().top});

           }

         });

       

       

      }).error(function(jqXHR, textStatus, errorThrown) { console.log(errorThrown); });



      



   });





   $(".editproduct").click(function(e){



      e.preventDefault();



      if ($("#divEditProduct").is(':visible'))

         $("#divEditProduct").toggle();



      $.get($(this).attr('href'), function( data ) {

      

         $('#divEditProduct').html( data.result );

          

         $("#divEditProduct").slideToggle( "slow", function(){

           if ($(this).is(':visible')) {

              $("html, body").animate({scrollTop: $(this).offset().top});

           }

         });

       

       

      }).error(function(jqXHR, textStatus, errorThrown) {  console.log(errorThrown); });



      



   });



</script>