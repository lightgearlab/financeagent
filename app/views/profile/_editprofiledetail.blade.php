

{{ Form::open(array('id' => 'editProfile' ,'url' => url('profile/updateProfile', $profile->uid))) }}

               <div class="col-lg-12 col-sm-6 col-mini-6   mt20 ">
                   <div class="container-fluid bg-white">
                   <div class="container-fluid mt20">
                       <div class="col-lg-5 ">
                       	<div class="form-group">
                             <h4><p>{{ $profile->getDisplayName() }}</p></h4>
                             <input type="text" name="name" class="form-control" placeholder="Name" value="{{ $profile->getDisplayName() }}" style="display:none;" >
                         </div>
							<div class="form-group">
								  	 <p class="font-thin">{{ $user->getUserLocation() }}</p>
							   {{ Form::select('location', ['' => 'Select'] + $locations , ( $user->hasLocation() ? $user->profile->location->id :  null),['class' => 'form-control','style' => 'display:none;']) }}

							</div>
                         	<div class="user-rating"></div>
                       </div>
                       @if(Auth::check() && $profile->isUserProfile(Auth::user()))
						<div class="col-lg-2 mt10 ">
							<a id="lnkEdit" href="#">Edit</a>

							<a id="lnkCancel" href="#" style="display:none;">Cancel</a>
						</div>
                         <div class="col-lg-7 ">
                            <button type="button" class="btn btn-primary btn-md pull-right mt5">Message</button>
                       </div>
                       @endif     
                    </div>
                    <div class="form-group">
	                    <div class="container-fluid mt50 pb10">
	                        <h4>Summary</h4>
	                        <textarea class="form-control" name="summary" rows="3" maxlength="140" placeholder="140 character" style="display:none;">{{ $profile->biography }}</textarea>
	                        <p class="font-thin">{{ $profile->biography }}</p>
	                    </div>
                	</div>
                    	<span class="pull-right">
                    		<button id="btnSave" class="btn btn-default" type="button" style="display:none;">Save</button>
                    	</span>
                       </div>
                </div>


{{Form::close()}}
<script type="text/javascript">
	
	$(function(){

		$("#btnSave").click(function(e){

			e.preventDefault();

			 var form = $("#editProfile");
			 var url = form.attr("action");
          
	          $.post(url,form.serialize()).done(function( data ) {
	             if(!data.error){
			    	
			    	window.location = data.url;
	             }else{

	             }

	          });

			
		});

		$("#lnkEdit").click(function(e){

			e.preventDefault();

			$(this).hide();
			$("#lnkCancel").show();
			$("#editProfile .form-control").show();
			$("#editProfile p").hide();
			$("#editProfile #btnSave").show();

		});

		$("#lnkCancel").click(function(e){

			e.preventDefault();
			$(this).hide();
			$("#lnkEdit").show();
			$("#editProfile .form-control").hide();
			$("#editProfile p").show();
			$("#editProfile #btnSave").hide();

		});
	});
</script>



