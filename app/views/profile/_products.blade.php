 @foreach ($products as $ap)

<div class="col-lg-4 col-sm-6 col-mini-6   mt20 ">
    <div class="blue-hover">
        @if( Auth::check() && $agent->isAgent(Auth::user()))
        <div class="col-lg-12 exit-blue">
            <a name="{{ $ap->id }}" href="javascript:void(0);" data-url="{{ url('/profile/agent/removeproduct', ['id' => $ap->id ])}}" class="remove-product" onclick="removeProduct({{ $ap->id }})">
                <p class="pull-right"><i class="fa fa-times" aria-hidden="true"></i></p>
            </a>
        </div>
        @endif

        <div class="list-of-service ">
            <h4><a href="{{ $ap->product->getProductDetailUrl() }}">{{ $ap->productcategory->name }}</a></h4>
            <p class="sub-subject font-thin">{{ $ap->product->name }}</p>
            <p>{{ $ap->description }}</p>
        </div>
        
        @if(Auth::check() && !$agent->isAgent(Auth::user()))
        <div class="all-prod">
            <h5>
                <a href="" class="btn btn-default">
                    MESSAGE ME
                </a>
                <span class="pull-right">
                    <form action="{{ URL::to('/application/apply') }}">
                    <input type="hidden" name="MainProductId" value="{{$ap->productcategory->uid}}" />
                    <input type="hidden" name="SubProductId" value="{{$ap->product->uid}}" />
                    <input type="hidden" name="AgentId" value="{{$agent->uid}}" />
                        
                    <button class="btn btn-default btn-apply">APPLY</button>
                    </form>
                </span>
            </h5>
        </div>
        @endif
    </div>
</div>

@endforeach



<script type="text/javascript">
      
    $(document).on("click",".btn-apply",function(e){
        
        var button = $(this);
        var form = $(button).closest("form");
        e.preventDefault();
    
        $.post(form.attr('action'),$(form).serialize()).done(function(data) {      
          if(!data.error)
            window.location = data.result.redirect_url;
        });
    });
    
    
</script>



