<section class="profile-page">               



            <div class="container">

               <div class="row">

                  <div class="col-md-8 agent-details-left">

                     <div class="col-md-12 agent-detail-section">

                        <div class="col-md-3 paddingzero">

                         	<img src="{{ $profile->getAvatar() }}" id="profile-avatar" class="img-rounded" alt="Image for Profile" />

                        </div>

                        <div class="col-md-7 agent-info">

                           <h4 class="disabled">{{ $profile->name }}</h4>

                           <p class="agent-rating">{{ number_format($profile->rating_cache, 1)}}</p>

                           <p class="disabled">{{ $profile->biography }}</p>

                           <p>{{ implode(",",$agent->getServices()) }}

                           	<br/>

                           	 @if($agent != null && $agent->bank != null) 

		                        		<strong>{{ $agent->bank->name }}</strong>

		                        @endif

                           </p>

                            @if($profile->user->location != null)

                           <p>{{ $profile->user->location->address }} </p>

                            @endif

                        </div>

                     </div>



                     

                     <!--This Editing Form show after click edit-->

					{{ Form::model($profile, array('method' => 'PATCH',  'route' => array('profile.update', $profile->id), 'files' => true)) }}



						<div class="form-group">

							<label for=""></label>

						  		<input type="file" id="uploader" name="uploader" />

						</div>



						  <!-- <div class="form-group">

						    <label for="txtFirstName">First Name</label>

						    <input id="txtFirstName" name="txtFirstName" type="text" class="form-control" value="{{ $profile->first_name }}"  placeholder="First Name">

						  </div>

						  <div class="form-group">

						    <label for="txtLastName">Last Name</label>

						    <input id="txtLastName" name="txtLastName" type="text" class="form-control" value="{{ $profile->last_name }}"  placeholder="Last Name">

						  </div> -->

						  <div class="form-group">

						    <label for="txtName">Name</label>

						    <input id="txtName" name="txtName" type="text" class="form-control" value="{{ $profile->name }}"  placeholder="Name">

						  </div>

						  <div class="form-group">

						    <label for="txtEmail">Email</label>

						    <input id="txtEmail" name="txtEmail"  type="email" class="form-control" value="{{ $profile->user->email }}" placeholder="mail@abc.com">

						  </div>

					<!-- 	  <div class="form-group">

						    <label for="txtAge">Age</label>

						    <input id="txtAge" name="txtAge"  type="text" class="form-control" value="{{ $profile->age }}" placeholder="age">

						  </div> -->

						<!--   <div class="form-group">

						    <label for="txtPhoneNumber">Phone Number</label>

						    <input id="txtPhoneNumber" name="txtPhoneNumber"  type="text" class="form-control" value="{{ $profile->phonenumber }}" placeholder="+60-XXXXXXXX">

						  </div> -->

						 <!--  <div class="form-group">

						    <label for="ddlGender">Gender</label>



						    {{ Form::select('ddlGender',array('male' => 'Male', 'female' => 'Female'), $profile->gender , ['class' => 'form-control']) }}

						     

						  </div> -->



						   <div class="form-group">

						    <label for="txtBiography">Biograph</label>

						    <textarea id="txtBiography" name="txtBiography" rows="4" cols="40" class="form-control">

						    	{{ $profile->biography }}

						    </textarea>

						    

						  </div>

						  <div class="form-group">

						    <label for="ddlBank">Bank</label>

					 		{{ Form::select('ddlBank', ['' => 'Select'] + $banks , ( $agent->hasBank() ? $agent->bank->id :  null),['class' => 'form-control'])}}

						     

						  </div>



						  <div class="form-group">

						    <label for="txtLocation">Location</label>



						    <input id="txtLocation" name="txtLocation"  type="text" class="form-control" value="{{ ($user->location != null ? $user->location->address : '') }}" placeholder="" onchange="GetLatlong()">

						    <input id="hidLat" name="hidLat" type="hidden" value="">

						    <input id="hidLng" name="hidLng" type="hidden" value="">

						    

						  </div>

						   <!-- <div class="form-group">

						    <label for="product">Products</label>



						     @foreach ($products as $i => $product)



							 <label class="form-control" >

							 	{{ Form::checkbox( 'products[]', 

					                  $product->id,

					                  in_array($products[$i]->id, ($agent->products() != null ? $agent->products->lists('id') : array())),

					                  ['class' => '', 'id' => $product->id] 

					                  ) 



					            }}

							 	{{ $product->name }}

							 </label>



							 @endforeach

						  </div> -->

						

						  <button type="submit" class="btn btn-primary">Publish</button>

						  {{ link_to_route('profile.show', 'Cancel' ,array($profile->id), array('class' => 'btn btn-default')) }}

						{{Form::close()}}



              		</div>



               </div>

            </div>

      

</section>