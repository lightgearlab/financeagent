@extends('layouts.default')

@section('contenthead')

 <link rel="stylesheet" href="{{ asset('/DragAvatar/css/style.css') }}">
    
@stop


@section('content')

	@if($user->isAgent())

		@include('profile.agent-edit', array('profile' => $profile , 'products' => $products,'banks' => $banks , 'agent' => $agent))
	@else
		@include('profile.normal-edit', array('profile' => $profile))
	@endif

@stop


@section('footerscript')
	<script src="{{ asset('/DragAvatar/resample.js') }}"></script>
	<script src="{{ asset('/DragAvatar/avatar.js') }}"></script>
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>


	<script type="text/javascript">
		function initialize() {

		var input = document.getElementById('txtLocation');
		var autocomplete = new google.maps.places.Autocomplete(input);
		}

		google.maps.event.addDomListener(window, 'load', initialize);


		function GetLatlong()
    	{
	        var geocoder = new google.maps.Geocoder();
	        var address = document.getElementById('txtLocation').value;

	        geocoder.geocode({ 'address': address }, function (results, status) {

	            if (status == google.maps.GeocoderStatus.OK) {
	                var latitude = results[0].geometry.location.lat();
	                var longitude = results[0].geometry.location.lng();

	                $("#hidLat").val(latitude);
	                $("#hidLng").val(longitude);
	            }
	        });
    	}
	</script>
@stop