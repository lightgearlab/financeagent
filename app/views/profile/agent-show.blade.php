

<section class="profile-page">

   <div class="container">

      <!--<div class="row">

         <div class="col-md-12">

            <ol class="breadcrumb">

               <li><a href="#">Home</a></li>

               <li class="active">Profile</li>

            </ol>

         </div>

      </div>-->

      <div class="row">

         <div class="col-md-8 agent-details-left">



               <div class="col-md-3 text-left paddingzero">

                  <img src="{{ $profile->getAvatar() }}" width="150" class="img-rounded">

               </div>

               <div class="col-md-7">

                  <h5>{{ $profile->getDisplayName() }} 

                  <!--<span><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> verified </span>--></h5>

                  <p>

                     {{ $profile->biography }}

                  </p>

                  <br />

                  <p>

                     {{ implode(",",$agent->getServices()) }}

                  </p>

                  <br />

                  @if($agent != null && $agent->bank != null) 

                  <p>{{ $agent->bank->name }}</p>

                  @endif

                  @if($profile->user->location != null)

                  <br />

                  <p>

                     {{ $profile->user->location->address }} 

                     @if(Auth::check() && Auth::user()->location != null && Auth::user()->location->lng != null && Auth::user()->location->lat != null)

                     {{ General::distance(Auth::user()->location->lat, Auth::user()->location->lng, $profile->user->location->lat, $profile->user->location->lng, "K")  }} km from you

                     @endif

                  </p>

                  @endif

                  <div class="ratings">

                     <p class="pull-right">{{$profile->rating_count}} {{ Str::plural('review', $profile->rating_count);}}</p>

                     <p>

                        @for ($i=1; $i <= 5 ; $i++)

                        <span class="glyphicon glyphicon-star rate-star{{ ($i <= $profile->rating_cache) ? '' : '-empty'}}"></span>

                        @endfor

                        {{ number_format($profile->rating_cache, 1);}} stars

                     </p>

                  </div>

                  @if(Auth::check() && ($user->id == Auth::user()->id))

                  {{ link_to_route('profile.edit', 'Edit Profile' ,array($user->profile->id), array('class' => 'btn btn-primary btn-editprofile')) }}

                  @endif

               </div>



            @include('product.list', array('products' => $agent->products))

         </div>

      </div>

      <div class="row">

         <div class="col-md-12">

            @include('profile.review', array('profile' => $profile))

         </div>

      </div>

   </div>

</section>