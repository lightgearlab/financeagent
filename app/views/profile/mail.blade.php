{{ Form::open(array('id' => 'formMail','role' => 'form' , 'url' => '/profile/message' . '/' . $profile->id  , 'method' => 'post')) }}

<div class="modal-header">
     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
     <h4 class="modal-title">Message to {{ $profile->getDisplayName() }}</title>
  </div>
  <div class="modal-body">
	 @if(count($errors)>0)
	 <p>
	      {{ $errors->first('subject') }}
	      {{ $errors->first('message') }}
	 </p>
	 @endif
  	 <div class="form-group">
	      <label for="subject"></label>
	      <input id="txtSubject" name="subject" type="text" class="form-control" value=""  placeholder="Subject" required="required">
     </div>
    <div class="form-group">
	    <label for="message"></label>
	    <textarea id="txtMessage" name="message" rows="4" cols="20" class="form-control" placeholder="Message here" required="required"></textarea>
	</div>


  </div>
  <div class="modal-footer">
  	 <button id="btnSend" type="submit" class="btn btn-success" value="Send">Send</button>
  	 <button type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">Cancel</button>
  </div>

{{Form::close()}}

<script type="text/javascript">

$(document).ready(function(){


	$('#formMail').validate({ // initialize the plugin
        rules: {
            subject: {
                required: true
            },
            message: {
                required: true
            }
        },
        messages: {
            subject: {
               required: '* Subject is required.'
            },
            message: {
               required: '* Message is required.'
            }
         },
        submitHandler: function(form) { 
           

        	var url = '<?=URL::to('/profile/message') . '/' . $profile->id ?>';

			$.ajax({
				type: 'POST',
			    url: url,
			    data: $("#formMail").serialize(),
			    success: function(result) {		        
			        $('#dialog-edit').modal('hide');
			    },
			    error: function(xhr, textStatus, errorThrown){
		                   console.log(xhr.responseText);
		                   return false;
		        }
			});
           
        }
    });

	/*$("#btnSend").click(function(e){

		e.preventDefault();
     	

     
     	var url = '<?=URL::to('/profile/message') . '/' . $profile->id ?>';

		$.ajax({
			type: 'POST',
		    url: url,
		    data: $("#formMail").serialize(),
		    success: function(result) {		        
		        $('#dialog-edit').modal('hide');
		    },
		    error: function(xhr, textStatus, errorThrown){
	                   console.log(xhr.responseText);
	                   return false;
	        }
		});
	});*/

});



	

</script>