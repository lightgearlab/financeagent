

     <style type="text/css">

     /* Enhance the look of the textarea expanding animation */

     .animated {

        -webkit-transition: height 0.2s;

        -moz-transition: height 0.2s;

        transition: height 0.2s;

      }

      .stars {

        margin: 20px 0;

        font-size: 18px;

        color: #FFB300;

      }
	  .glyphicon-star-empty {}
	  
	  .btn-post-review {    background: #616161;
    border: 1px solid #424242; color:#FFF;
    padding: 10px 40px;}

  </style>







   <h5>Testimonials</h5>

   <div class="row">





      @if( Auth::check() && (Auth::user()->profile->id != $profile->id) && !$user->isLoginUser())



      <form id="reviewForm" name="reviewForm" class="form-inline" action="<?=URL::to('/review') . '/' . $profile->id ?>" method="POST">

         <div class="col-md-12">

            

            {{Form::hidden('rating', null, array('id'=>'ratings-hidden'))}}

            {{Form::textarea('comment', null, array('rows'=>'5','id'=>'new-review','class'=>'form-control animated','placeholder'=>'Enter your review here...'))}}

            <div class="text-right">

            <div class="stars starrr" data-rating="{{Input::old('rating',0)}}"></div>

              <a href="#" class="btn btn-danger btn-sm" id="close-review-box" style="display:none; margin-right:10px;"> <span class="glyphicon glyphicon-remove"></span>Cancel</a>

              <button type="submit" class="btn btn-post-review pull-right">Post</button>

            </div>

            

         </div>

      </form>



      @endif









   </div>



   <div class="row">

     <div id="divReviewList">

        

       

     </div>

   </div>

   <script type="text/javascript">





    $.get('<?=URL::to('/review') . '/' . $profile->id ?>', function( data ) {

      

      $('#divReviewList').html( data.result );

     

    });



    $('#divReviewList').on('click','.pagination a', function (event) {

        event.preventDefault();

        

        if ( $(this).attr('href') != '#' ) {

            $("#divReviewList").animate({ scrollTop: 0 }, "fast");



            $.get($(this).attr('href'), function( data ) {

      

              $('#divReviewList').html( data.result );

             

            });

            

        }

    });



   </script>





