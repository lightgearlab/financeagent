      @if(count($reviews) > 0)

        @foreach ($reviews as $i => $review)

        <div class="reviewsection">

        <div class="col-md-2 ">

           <img src="{{ $review->user->profile->getAvatar() }}" class="img-rounded" width="64px" height="64px" alt="...">

        </div>

        <div class="col-md-10">

           <h6>{{ link_to_route('profile.show', $review->user->profile->getDisplayName()  ,array($review->user->profile->id), array('class' => '')) }}

             @if(Auth::check() && $review->profile->id == Auth::user()->profile->id)

             <a class="removereview pull-right" href="{{ URL::to('/deleteReview', array('id' => $review->id)) }}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>

             @endif

           </h6> 

           <p class="review">

              {{ $review->comment }}

           </p>

           <p>

              {{ $review->created_at->format('M Y') }}

           </p>

        </div>

        @endforeach



        {{  $reviews->links(); }}



      @else

      <div class="col-md-10">

        <p>No Reviews</p>

      </div>

      @endif

    





      <script type="text/javascript">

          $(".removereview").click(function(e){



              console.log("clicked");

             e.preventDefault();



             var link = $(this);

  

             deleteItem($(this).attr('href'),link);

               

          });





          function deleteItem(url,link){

          $.ajax({

              type: 'POST',

                url: url,

                data: { "_token": "{{ csrf_token() }}" },

                success: function(result) {



                   $(link).closest(".reviewsection").remove();

                  return true;

                },

                error: function(xhr, textStatus, errorThrown){

                                 console.log(xhr.responseText);

                                 return false;

                      }

            });

        }

      </script>

