<div class="container-fluid">
    <div class="col-lg-9">
        <div class="row">
            <div class="col-lg-4 col-xs-12  mt20">
                <div class="col-lg-12">
                    <div class="row">
                        <a href="{{ $user->profile->getProfileUrl() }}">
                          @if( Auth::check() && $user->isLoginUser())
                            <div class="row ">
                                <div class="col-lg-12 mt10 ml10 "  style="position:absolute; pull-right"> 

                                    <form id="profileUploadForm" action="{{URL::to('/profile/updatephoto')}}" enctype="multipart/form-data" method="post" >
                                      <input type="file" id="uploader" name="uploader" multiple style="display:none;" />
                                    </form>
                                    <a href="#" id="lnkImgUpload" >
                                      <p class="pull-right btn btn-default btn-file">Upload</p>
                                    </a>

                                </div>
                            </div>
                            @endif
                                <div class="blue-hover">
                                    <img src="{{ $profile->getAvatar() }}" id="profile-avatar" class="img-responsive img-full-width">
                                </div>
                            </a>
                    </div>

                </div>
            </div>
            <div class="col-lg-8 col-xs-12 pd0">
                @include("profile._editprofiledetail",array('profile' => $profile, 'user' => $user))   
            </div>
          </div>
       
       
    </div>
     <div class="col-lg-3 col-xs-12 mt20 ">
        <div class="col-lg-12">
            <div class="row ">

                <img src="{{ asset('img/assets/ads9.jpg') }}" class="img-responsive img-full-width" alt="Cerulean">
            </div>
            <div class="row  bg-white ">
                <div class="col-lg-8 col-md-8  col-md-offset-2  col-lg-offset-2 mt60 pb40">
                    <button type="button" class=" btn-block round-outer-btn">
                        <p>Discover</p>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="container-fluid">
           @include('profile.review2', array('profile' => $profile))
    </div>

  @if( Auth::check() && (Auth::user()->profile->id != $profile->id) && !$user->isLoginUser())
    <div class="container-fluid">
           @include('profile.review-form', array('profile' => $profile))
    </div>
  @endif


<div class="col-lg-12 col-xs-12 ">
    <div class="mt20 pb80">
        <div class="col-lg-12 col-xs-12 tab-section-product pull-right">

        </div>

    </div>
</div>


@section('footerscript')


    <script type="text/javascript">


        $(function(){

            var userRating = {{ $profile->getRating() }};
            displayUserRating(userRating);

            $(".description").shorten({

                "showChars" : 300,

                "moreText"  : "More Detail",

            });


         $("#uploader").change(function(e){
            
            e.preventDefault();

            var form = $("#profileUploadForm");

            var formData = new FormData();
           
            formData.append('uploader', $('#uploader')[0].files[0]);

            $.ajax( {
                    url: $(form).attr('action'),
                    type: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(data, textStatus, jqXHR){ }
            
            });
         });

         $("#lnkImgUpload").click(function(e){

            e.preventDefault();
            

            $("#uploader").click();

         });


         $("#lnkEditProfile").click(function(e){

            e.preventDefault();

            $("#divEditProfile").slideToggle( "slow", function(){

              if ($(this).is(':visible')) {

                 $("html, body").animate({scrollTop: $(this).offset().top});

              }
            });

         });
        });


    </script>

@append