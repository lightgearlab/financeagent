{{ Form::open(array('id' => 'addProductForm' ,'url' => URL::to('/profile/addproduct'))) }}

<div class="container-fluid bg-white add-more-profile ">
  <div class="container-fluid pd0 "> 
      <div class="form-group">
      {{ Form::select('MainProduct', ['0' => 'Select'] +  $mainproducts , null,['id' => 'MainProduct','class' => 'form-control']) }}
      </div>
  </div>
  <div class="container-fluid pd0 ">
      <div class="form-group">
         {{ Form::select('SubProduct', ['0' => 'Select'] +  $subproducts , null,['id' => 'SubProduct','class' => 'form-control']) }}
      </div>
  </div>
  <div class="container-fluid pd0">
      <textarea id="Description" name="Description" class="form-control" rows="3" placeholder="Description of product"></textarea>
      <br><a id="lnkSaveProduct" href="#" class="btn btn-default pull-right"><p>Done</p></a>
  </div>
</div>


{{Form::close()}}


@section('footerscript')

  <script type="text/javascript">


      $('#lnkSaveProduct').click(function(e){


          e.preventDefault();

          var url = $("#addProductForm").attr("action");
          
          $.post(url,$("#addProductForm").serialize()).done(function( data ) {
             if(!data.error){
                $(document).trigger("populateProductList");

                $('#MainProduct').prop('selectedIndex',0);
                $('#SubProduct').prop('selectedIndex',0);
                $('#Description').val('');
             }else
             {

             }

          });
      });
  </script>

@append