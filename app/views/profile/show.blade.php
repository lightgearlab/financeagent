@extends('layouts.default')



@section('contenthead')



   

@stop



@section('body_class', 'bg-grey')



@section('content')


    {{ Breadcrumbs::render() }}
	@if($user->isAgent())

		@include('profile.agent2', array('user' => $user, 'profile' => $profile, 'agent' => $agent))

	@else

		@include('profile.normal2', array('profile' => $profile))

	@endif





@stop





@section('footerscript')

  <script src="{{ asset('/DragAvatar/resample.js') }}"></script>

  <script src="{{ asset('/DragAvatar/avatar.js') }}"></script>

<!--  <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>-->



   <script type="text/javascript">

//        function initialize() {
//
//
//
//        var input = document.getElementById('txtLocation');
//
//        var autocomplete = new google.maps.places.Autocomplete(input);
//
//
//        google.maps.event.addListener(autocomplete, 'place_changed', function() {
//           
//
//              var geocoder = new google.maps.Geocoder();
//
//              var address = $('#txtLocation').val();
//
//              console.log(address);
//
//              geocoder.geocode({ 'address': address }, function (results, status) {
//
//
//
//                  if (status == google.maps.GeocoderStatus.OK) {
//
//                      var latitude = results[0].geometry.location.lat();
//
//                      var longitude = results[0].geometry.location.lng();
//
//
//
//                      $("#hidLat").val(latitude);
//
//                      $("#hidLng").val(longitude);
//
//                  }
//
//              });
//           
//            return false;
//        });
//
//        }
//
//
//
//        google.maps.event.addDomListener(window, 'load', initialize);



   </script>

@append