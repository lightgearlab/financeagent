@extends('layouts.default')



@section('contenthead')



@stop





@section('content')



<div class="content-section-a-no-bottom app_step-head">
  <div class="container-fluid "> 
    <!--step 1-->
    
    <div class="col-md-4 mt10">
      @include('shared._calculator',array('appCode' => $appCode))
    </div>
    <!--listing-->
    <div class="col-md-4 mt10">
        @include('application._personaldetail',array('title' => '','applicant' => $applicant))
    </div>
    <!--ssss-->
    <div class="col-md-4 mt10">
        @include('application._productdetail',array('title' => ''))
    </div>
  </div>
  <!--asdsad--> 
</div>

<div id="divRecommendList" data-url="<?=URL::to('/application/product/recommend?code=') . $appCode ?>" ></div>

@stop





@section('footerscript')
	<script type="text/javascript">
       
        $(document).on( "populateRecommendList", function(event,result) {
		
			var url = $("#divRecommendList").data("url");

			$.get(url).done(function( data ) {
			   	$("#divRecommendList").empty();
			   	$("#divRecommendList").html(data.result);
			});
		});

		$(function(){
            $(document).trigger("populateRecommendList");
			
		});
               
	</script>

@append