@foreach ($appProducts as $index => $appProduct) 
<div class="row mt40">
  <div class="col-md-12 price-list-step pd10">
    <p>Micro Finance<span class="pull-right">RM 100 / Month</span></p>
    <p>Interest Rate<span class="pull-right">1.00 %</span></p>
  </div>
</div>
<div class="row">
  <div class="col-md-12  pd10 img-step-card">
    <div class="col-md-3  col-xs-5 col-mini-4  col-md-5 row"> <img src="img/bank-rakyat-classic-credit-card-i.png" class="img-responsive"> </div>
    <div class="col-md-4  col-xs-4 ">
      <p><b>{{ $appProduct->mainproduct->name }}</b></p>
      <p class="font-thin">{{ $appProduct->subproduct->name }}</p>
    </div>
    <div class="col-md-3  col-xs-3"> 
        <span class="label  pull-left label-step-card">{{ $appProduct->displayOption($index + 1) }}</span> 
    </div>
    <div class="col-md-2 pull-right"> 
        @if($index != 0 && Auth::check() && $application->isApplicant())
        <form action="{{URL::to('/application/product/remove')}}">
            @if(isset($appCode))
            <input name="appCode" type="hidden" value="{{$appCode}}" />
            @endif
            <input name="AppProductId" type="hidden" value="{{$appProduct->id}}" />
        <a href="#" data-id="{{$appProduct->id}}" class="btn-remove-product" ><i class="fa fa-times pull-right btn-exit-step-cart" aria-hidden="true"></i> </a>
        </form>
        @endif
    </div>
  </div>
</div>

@endforeach


<script>
        $(".btn-remove-product").click(function(e){
        
            e.preventDefault();
            
            var form = $(this).closest("form");

            var url = form.attr('action');
            $.post(url,form.serialize()).done(function( data ) {
			
                if(!data.error){
                    $(document).trigger("populateProductList");
                    $(document).trigger("populateRecommendList");
                }
			}); 
        });
</script>