
<div class="col-md-12 bg-white step-title pd10">
<div class="row step-head">
<h4><i class="fa fa-check-circle check-color-step" aria-hidden="true"></i> Basic information</h4>
</div>
</div>
<div class="col-md-12  step-form  bg-white pt20">
<div class="row">
{{ Form::open(array('id' => 'formPersonal','name' => 'formPersonal','url' => URL::to('/application/customer') )) }}
    <div class="col-md-12 form-group">
        <label for="Name">Name</label>
        <input id="Name" name="Name" type="text" class="form-control" placeholder="" value="{{$applicant->name }}" required>
    </div>
    <div class=" col-md-6  col-xs-6 form-group">
        <label for="PhoneNumber">Phone</label>
        <input id="PhoneNumber" name="PhoneNumber" type="text" class="form-control" placeholder="" value="{{$applicant->phone_number }}" required>
    </div>
    <div class=" col-md-6  col-xs-6  form-group">
        <label for="Email">Email</label>
        <input id="Email" name="Email" type="text" class="form-control" placeholder="" value="{{$applicant->email }}" required>
    </div>
    <div class=" col-md-12 col-xs-12 form-group">
      <label for="text">Location</label>
        
   {{ Form::select('Location', ['' => 'Select'] + $locations , ( $applicant->hasLocation() ? $applicant->location->id :  null),['class' => 'form-control']) }}

    </div>
    <div class=" col-md-12 form-group">
        <label for="Occupation">Occupations</label>
        <input id="Occupation" name="Occupation" type="text" class="form-control" placeholder="" value="{{ $applicant->occupations }}" required>
    </div>
    <div class=" col-md-12 form-group">
        <label for="ICNumber">IC Number</label>
        <input id="ICNumber" name="ICNumber" type="text" class="form-control" placeholder="" value="{{ $applicant->ic_number }}" required>

    </div>
   {{ Form::close() }}
</div>
<div class="col-md-12 p0">
    <button id="btnSave" type="button" class="btn btn-block btn-success save-app-btn"><b class="font-white-color">Save</b></button>
</div>
 
</div>


@section('footerscript')

<script type="text/javascript">
	var products = {};
	
    $('#btnSave').unbind('click').bind('click', function() {
			var url = $("#formPersonal").attr("action");
            $.post(url,$("#formPersonal").serialize()).done(function( data ) {		    
                    alert(data.message);
                

			});
    });
	
//		$("#btnSave").click(function(event){
//            
//            console.log('clicked');
//            event.stopPropagation();
//            
//			var url = $("#formPersonal").attr("action");
//
//			$.post(url,$("#formPersonal").serialize()).done(function( data ) {		    
//
//				console.log(data);
//
//			});
//            
//             return false;
//		});

   

</script>
@append
