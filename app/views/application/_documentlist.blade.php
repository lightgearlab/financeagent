
<ol>
    @foreach ($appDocuments as $appDocument)
        <li><a href="{{ $appDocument->document->getDownloadUrl() }}" download>{{ $appDocument->document->filename }}</a></li>
    @endforeach
</ol>