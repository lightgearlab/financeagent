  <div class="col-md-12 bg-white step-title pd10">
        <div class="row step-head">
          <h4><i class="fa fa-check-circle check-color-step" aria-hidden="true"></i> Uploads Documents</h4>
        </div>
      </div>
 <div class="col-md-12  step-form bg-white pt20">
        <div class="row">
          <div class="col-md-12">
            <h4>Upload Details</h4>
          </div>
        </div>
        
        
        
        <div class="row">
  <div class="col-xs-6 col-md-3">
    <a href="#" class="thumbnail">
      <img src="http://placehold.it/63x63." alt="...">
    </a>
  </div>
 <div class="col-xs-6 col-md-3">
    <a href="#" class="thumbnail">
      <img src="http://placehold.it/63x63." alt="...">
    </a>
  </div>
   <div class="col-xs-6 col-md-3">
    <a href="#" class="thumbnail">
      <img src="http://placehold.it/63x63." alt="...">
    </a>
  </div>
<div class="col-xs-6 col-md-3">
    <a href="#" class="thumbnail">
      <img src="http://placehold.it/63x63." alt="...">
    </a>
  </div>

</div>
        
        <div class="row">
          <div class="col-md-12">
        
        <form id="formUpload" action="{{ URL::to('/application/document/upload') }}" method="post" enctype="multipart/form-data">
  
          <div class="form-group">
            {{ Form::token() }}
            @if(isset($appCode))
                <input type="hidden" name="appCode" value="{{$appCode}}" />
            @endif
            <input type="file" id="uploadDocument" name="UserDocument">
          </div>
          <button id="btnUpload" name="btnUpload">Upload</button>

        </form>


          </div>
        </div>
        
        
        
        
        <div class="row mt20">
          <div class="col-md-12">
            <p>Your Upload Documents</p>
          </div>
          <div id="divDocuments" data-url="{{ URL::to('/application/document?code=') . $appCode }}" 
               class="col-md-12 upload-file-step"></div>
        </div>
        <div id="divProducts" data-url="{{ URL::to('/application/product?code=') . $appCode }}" >
        </div>
        <div class="row    bg-white btn-step">
        <form action="{{ URL::to('/application/submit') }}">
          <div class="col-md-12 pd10 grey-top-line">
            <label class="checkbox-inline">
             {{ Form::checkbox('IsExist', 1, $application->isExistingCustomer) }} 
             
              <b>Existing Bank Rakyat Customer</b></label>
          </div>
          <div class="col-md-12 p0">
         
            @if(isset($appCode))
                <input type="hidden" name="appCode" value="{{$appCode}}" />
            @endif
            <a id="btnApply" href="#" class="btn btn-block btn-success save-app-btn">Apply</a>
            <br>
            <p class="text-muted">By clicking Apply you are hereby agreed to Bank Rakyat <a href="">Terms & Condition</a></p>
           
          </div>
            </form>
        </div>
      </div>

@section('footerscript')

<script type="text/javascript">
	
    $(document).on( "populateDocumentList", function(event,result) {
        
            var url = $("#divDocuments").data("url");

            $.get(url).done(function( data ) {

                $("#divDocuments").empty();

                if(!data.error) $("#divDocuments").html(data.result);
            });
    });
    
    
     $(document).on( "populateProductList", function(event,result) {
     
            var url = $("#divProducts").data("url");

            $.get(url).done(function( data ) {

                $("#divProducts").empty();

                if(!data.error) $("#divProducts").html(data.result);
            });
    });
    
	$(function(){
	   
        $(document).trigger("populateDocumentList");
        $(document).trigger("populateProductList");
        
        $("#btnUpload").click(function(e){
            
            e.preventDefault();
            
            var formData =  new FormData(document.getElementById("formUpload"));
            var url = $("#formUpload").attr('action');
            
             $.ajax({
                url: url,
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                type: 'POST',
                success: function (data) {
                    if(!data.error){
                        
                        $("#formUpload").trigger('reset');
                        $(document).trigger("populateDocumentList");
                        
                    }
                }
            });
         
            
            return;
        });
        
        $("#btnApply").click(function(e){
            
            e.preventDefault();
            var form = $(this).closest("form");
            var url = form.attr('action');
            $.post(url,form.serialize()).done(function( data ) {
			console.log(data);
                if(!data.error){
                    window.location = data.result.redirect;
                }
			}); 
        });
	});
</script>
@append
