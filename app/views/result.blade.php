
@foreach ($results as $i => $result)

<div class="col-sm-4 agent-list {{  ($i + 1) % 3 != 0 ? 'marginright' : '' }}">
      <div class="col-sm-3 paddingzero">

         @if($result->photointernal == 1)
            <img src="{{ asset($result->photo_url) }}" class="img-rounded" width="100">
         @else
            <img src="{{ $result->photo_url }}" class="img-rounded" width="100">
         @endif
         
      </div>
      <div class="col-sm-9 cardinfo text-left">
         <h6>{{ link_to_route('profile.show', $result->AgentName ,array($result->ProfileId), array('class' => '')) }}</h6>
         <p class="agent-rating">9/10</p>
         <p>{{ $result->biography }}<br>
            <strong>{{ $result->BankName }}</strong>
            {{ $result->address }}
         </p>
      </div>
      <div class="col-sm-9 text-left">
         <a href="<?=URL::to('/agent/message')?>" class="btn btn-info contact-me">Message Me</a>
      </div>
      
</div>
@endforeach

<br/>
{{  $results->appends(array('q' => $search))->links(); }}