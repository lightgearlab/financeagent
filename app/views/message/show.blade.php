@extends('layouts.default')



@section('contenthead')



<script type="text/javascript">
    var convId = '<?= $conversation->id ?>';

    $(document).ready(function(){


         $.get('<?=URL::to('/conversations') ?>', function( data ) {

      
            $('#divConversations').html(data.result);   

             GetParticipant(convId);
             GetMessages(convId); 

          });


    });


    function GetParticipant(id){
       $.get('<?=URL::to('/conversation/participant') . '/' ?>' + id, function( data ) {

      
            $('#divParticipant').html(data.result);    

          });
    }


    function GetMessages(id){

       $.get('<?=URL::to('/conversation') . '/' ?>' + id, function( data ) {

      
            $('#divMessages').html(data.result);    

          });

    }
 </script>

@stop





@section('content')


<section class="profile-page">
  <div class="container">
  <div class="row">
    <div class="col-md-12">
       <h4>Messages</h4>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 application-form">
       
       

      <div class="container">
        <div class="row">
          <div class="col-lg-3">
            <div class="btn-panel btn-panel-conversation">
              <a href="" class="btn  col-lg-6 send-message-btn " role="button"><i class="fa fa-search"></i> 
                Search
              </a>
              <a id="btnNew" href="#" class="btn  col-lg-6  send-message-btn pull-right" role="button"><i class="fa fa-plus"></i> 
                New Message
              </a>
            </div>
            <div>
            <div class="input-group">
                  <input type="text" class="form-control" placeholder="Search for...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                  </span>
                </div>
            </div>
          </div>
          <div class="col-lg-3">
            <div id="divParticipant" class=""></div>
          </div>
        </div>

        <div class="row">

          <div class="conversation-wrap col-lg-3">
              <div id="divConversations"></div>
          </div>
          <div class="message-wrap col-lg-8">       
              <form id="formMessage" name="formMessage" action="<?=URL::to('/conversation/message')?>" method="POST">
                <div id="divMessages" class="msg-wrap"></div>
                <div class="send-wrap ">
                <textarea id="content" name="content" class="form-control send-message" rows="3" placeholder="Write a reply..."></textarea>
                </div>
                <div class="btn-panel">

                <a id="btnSend" href="#" class="col-lg-4 text-right btn send-message-btn pull-right" role="button"><i class="fa fa-plus"></i> Send Message</a>
                </div>
            </form>
          </div> 
        </div>
      </div>
    </div>

  </div>
  </div>
</section>


@stop


@section('footerscript')

<script type="text/javascript">

  $(document).ready(function(){

   $("#btnNew").click(function(e){
        
        e.preventDefault();


        window.location.href = '<?=URL::to("/messages/new")?>';
    });

    $("#btnSend").click(function(e){

        e.preventDefault();

        var convId = $("#listConv li.active").data("value");

         $.ajax({
             type: "POST",
             url : '<?=URL::to('/conversation/message')?>' + '/' + convId,
             data: $("#formMessage").serialize(),
             success : function(data){
                                
               if(!data.error){

                  $("#divMessages").append(data.result);
                  
               }else{
   
               }
   
              
             },
             error: function(xhr, textStatus, errorThrown){
                console.log(xhr.responseText);
             }
         });

    });

  });
</script>

@stop