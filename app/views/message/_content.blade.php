<div class="media msg ">
    <a class="pull-left" href="#">
<img class="media-object" src="{{ $message->user->profile->getAvatar() }}" width="32">
    </a>
    <div class="media-body">
        <small class="pull-right time"><i class="fa fa-clock-o"></i> {{ $message->created_at }}</small>
        <h5 class="media-heading">{{ $message->user->profile->getDisplayName()}}</h5>
        <small class="col-lg-10">{{ $message->content }}</small>
    </div>
</div>