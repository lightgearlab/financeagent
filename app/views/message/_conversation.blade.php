<li class="list-group-item active" data-value="{{ $conversation->id }}">
<div class="media conversation">
	<a class="pull-left" href="#">
		<img class="media-object" src="{{ $conversation->getImageUrl() }}" width="50">
	</a>
	<div class="media-body">
	    <h5 class="media-heading">{{ $conversation->getParticipantName() }}</h5>
	    <small>{{ $conversation->latestMessage()->content }}</small>
	</div>
</div>
</li>