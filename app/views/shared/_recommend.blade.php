<div class="col-md-12 col-xs-12 ">
                <div class="mt20">
                    <div class="col-md-12 col-xs-12 tab-section-product pull-right">
                        <div class="col-md-2 col-xs-6 pull-right  col-xs-12 col-mini-5  tab-login ">
                            <a href="#4a" data-toggle="tab" class="pull-right" class="active"><h4>Recomendations</h4></a>
                        </div>
                        <div class="col-md-2 col-xs-6  pull-right   col-xs-12 col-mini-4  tab-login">
                            <a href="#5a" data-toggle="tab" class="pull-right"><h4>Product you can afford</h4></a>
                        </div>
                    </div>
                    <div class="tab-content clearfix ">
                        <div class="tab-pane col-md-12 pd0 col-xs-12 active" id="4a">
                            <div class="container-fluid pd0 font-thin tab-box-bottom">
                                 @foreach ($products as $ap)

                                <div class="col-md-3 col-xs-12 col-mini-6 mt20 tab-product">
                                   <div class="blue-hover">
                                        <div class="container-fluid pd0 bg-white step-border-grey " style="padding:10px 0 10px 0;">
                                        <div class="col-md-6  col-xs-3 mt20" style="text-align:left;">
                                            <h4>{{ $ap->product->name }}</h4>
                                            <p>{{ $ap->agent->user->profile->getDisplayName() }}</p>
                                        </div>
                                        <div class="col-md-6 col-xs-6 pull-right  " style="text-align:left;">
                                            <img src="{{ $ap->agent->user->profile->getAvatar()}}" class="img-responsive">
                                        </div>
                                        </div>
                                        <div class="container-fluid" style="padding:0px;">
                                        <div class="col-md-12  bg-white pd10 non-text-decoration" >

                                         <h5>Kuala Lumpur<span class="pull-right">
                                             <form action="{{ URL::to('/application/product/add')}}">
                                                @if(isset($appCode))
                                                    <input type="hidden" name="appCode" value="{{$appCode}}" />
                                                @endif
                                                <input name="SubProductId" type="hidden" value="{{ $ap->product->uid }}" />
                                                <input name="MainProductId" type="hidden" value="{{ $ap->productcategory->uid }}" />
                                                 
                                                 <input name="AgentId" type="hidden" value="{{ $ap->agent->uid}}" />
                                             
                                             <a href="#" class="add-product" >Add on</a>
                                            </form>
                                                 </span></h5>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                               
                            </div>
                        </div>
                        <div class="tab-pane col-md-12 col-xs-12" id="5a">
                            <div class="container-fluid  font-thin" style="min-height:250px;">
                                <!-- no data page-->
                                <div class="col-md-2 col-xs-3 col-md-offset-5 col-xs-offset-4  mt20 text-center">
                                    <img src="img/no_data.png" class="img-responsive col-md-offset-2 col-xs-offset-1">
                                    <p class="mt20">ups there is no data </p>
                                </div>
                                <!--end of no data-->
                            </div>
                        </div>
                    </div>
                </div>
    </div>


<script>
        $(".add-product").click(function(e){
        
            e.preventDefault();
            
            var form = $(this).closest("form");

            var url = form.attr('action');
            $.post(url,form.serialize()).done(function( data ) {
                if(!data.error){
                    $(document).trigger("populateProductList");
                    $(document).trigger("populateRecommendList");
                }
			}); 
        });
</script>


