   
      <div class="col-md-12 bg-white step-title pd10">
        <div class="row step-head">
          <h4><i class="fa fa-check-circle check-color-step" aria-hidden="true"></i> Pre Scoring</h4>
        </div>
      </div>
      <div class="col-md-12  step-form  bg-white pt20">
        <div class="row">

{{ Form::open(array('id' => 'formCalculator','name' => 'formCalculator','url' => URL::to('/score/calculate') )) }}

        @if(isset($appCode))
            <input type="hidden" name="appCode" value="{{$appCode}}" />
        @endif
            
        
		<div class="col-md-12 form-group">
		<label for="text">Product</label>

		{{ Form::select('productCategories', ['' => 'Select'] + $productCategories, ( $score->hasMainProduct() ? $score->mainproduct->id :  null),['class' => 'form-control user-input','required' => 'required']) }}
		</div>
		<div class=" col-md-12 col-xs-12 form-group">
		<label for="text">Monthly Income (RM)</label>
		<input type="text" class="form-control currency user-input" id="MonthlyIncome" name="MonthlyIncome" value="{{$score->monthlyincome}}">
		</div>
		<div class=" col-md-6  col-xs-6 form-group">
		<label for="text">Loan tenure (Years)</label>
          
		<select class="form-control user-input" name="LoanTenure">
			<option value="1"  {{ $score->loantenure == 1 ? 'selected="selected"' : '' }} >1 year</option>
			<option value="2"  {{ $score->loantenure == 2 ? 'selected="selected"' : '' }} >2 years</option>
			<option value="3"  {{ $score->loantenure == 3 ? 'selected="selected"' : '' }} >3 years</option>
			<option value="4"  {{ $score->loantenure == 4 ? 'selected="selected"' : '' }} >4 years</option>
			<option value="5"  {{ $score->loantenure == 5 ? 'selected="selected"' : '' }} >5 years</option>
			<option value="6"  {{ $score->loantenure == 6 ? 'selected="selected"' : '' }} >6 years</option>
			<option value="7"  {{ $score->loantenure == 7 ? 'selected="selected"' : '' }} >7 years</option>
			<option value="8"  {{ $score->loantenure == 8 ? 'selected="selected"' : '' }} >8 years</option>
			<option value="9"  {{ $score->loantenure == 9 ? 'selected="selected"' : '' }} >9 years</option>

		</select>
		</div>
		<div class=" col-md-6 col-xs-6  form-group">
		<label for="text">Interest Rate (% pa)</label>
            
		{{ Form::select('InterestRate', ['' => 'Select'] + $interestRates, ( $score->hasInterestRate() ? $score->interestrate->id :  null),['class' => 'form-control user-input']) }}
	
		</div>

		<div class=" col-md-6 col-xs-6  form-group">
			<label for="text">Total house financing if any</label>
			<input type="text" class="form-control currency user-input" id="text" name="TotalHouseFinance" 
                   value='{{$score->totalhousefinance}}' >
		</div>

		<div class=" col-md-6 col-xs-6  form-group">
			<label for="text">Total vehicle financing if any</label>
			<input type="text" class="form-control currency user-input" id="text" name="TotalVehicleFinance" 
                   value='{{$score->totalvehiclefinance}}' >
		</div>
		<div class=" col-md-6 col-xs-6  form-group">
			<label for="text">Total personal financing if any</label>
			<input type="text" class="form-control currency user-input" id="text" name="TotalPersonalFinance" 
                   value='{{$score->totalpersonalfinance}}' >
		</div>

		<div class=" col-md-6 col-xs-6  form-group">
			<label for="text">Total credit card minimum payment if any</label>
			<input type="text" class="form-control currency user-input" id="text" name="TotalCreditCard" 
                   value='{{$score->totalcreditcardfinance}}' >
		</div>


		<div class="col-md-12 text-center pd5">
			<button type="button" id="btnCalculate" data-url="{{URL::to('/score/calculate')}}" class="btn btn-default btn-block pd10"><b>Calculate</b></button>
		</div>


		<div class="col-md-12">
		<h5>Monthly Repayment (RM):	</h5> 
            <input id="MonthlyRepayment" name="MonthlyRepayment" type="text" value="{{ $score->monthlyrepayment }}" class="form-control currency user-result readonly" readonly></input>
       

		<h5>Maximum Loan (RM):	</h5> 
             <input id="MaximumLoan" name="MaximumLoan" type="text" value="{{ $score->maximumloan }}" class="form-control currency user-result readonly" readonly></input>

		<h5>Maximum Monthly Repayment (RM):	</h5> 
           <input id="MaximumMonthlyRepayment" name="MaximumMonthlyRepayment" type="text" value="{{ $score->maximummonthlyrepayment }}" class="form-control currency user-result readonly" readonly></input>

		</div>
        
        

{{ Form::close() }}
        <div class="col-md-12 p0 step-one-btn">
            <button id="btnSaveCalculation" name="btnSaveCalculation" type="button" data-url="{{URL::to('/score/calculate/save')}}" class="btn btn-block btn-success save-app-btn"><b class="font-white-color">Save</b></button>
          </div>
        
      </div>
    </div>


@section('footerscript')

<script type="text/javascript">
	var products = {};
	
	$(function(){

		$('.currency').trigger('change');
        
        
        $('.user-input').keypress(function(e){
            $('.user-result').val('');
            $("#btnSaveCalculation").prop("disabled",true);
        });
        
		$("#btnAddCommitment").click(function(e){


			var element = '<li>Commitment <input id="Commitment" name="Commitment[]" type="text" class="form-control" placeholder="" value="" required>Total <input id="Total" name="Total[]" type="text" class="form-control" placeholder="" value="" required></li>';

			 $("#CommitmentList").append(element);
		});
        
        
        $("#btnCalculate").click(function(e){

            e.preventDefault();
			var url = $(this).data("url");
            
            
            $('.user-result').val('');
            $("#btnSaveCalculation").prop("disabled",true);

			$.post(url,$("#formCalculator").serialize()).done(function( data ) {
            
			    if(!data.error){
                    
                    if(data.result.score != null){
                      
                    $("#MonthlyRepayment").val(data.result.score.monthlyrepayment).trigger('change');
                    $("#MaximumLoan").val(data.result.score.maximumloan).trigger('change');
                    $("#MaximumMonthlyRepayment").val(data.result.score.maximummonthlyrepayment).trigger('change');
                    }
                    
                    $("#btnSaveCalculation").prop("disabled",false);
                    
			    	$( document ).trigger("populateRecommendList", data.result);
			    }


			});
            
            return;
		});

		$("#btnSaveCalculation").click(function(e){


			var url = $(this).data("url");

			$.post(url,$("#formCalculator").serialize()).done(function( data ) {
               
			    if(!data.error){
			   
			    	alert(data.message);
			    }


			});
		});


	});
</script>
@append
