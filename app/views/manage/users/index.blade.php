@extends('layouts.default')

@section('contenthead')

   
@stop


@section('content')
	
	
	 <section class="result-page">
            <div class="container">
               <div class="row" style="text-align:right;">
               		{{ link_to('manage/', 'Back', $attributes = array(), $secure = null) }}
               </div>
               <div class="row">

               	
               	@if ($users->count())
               		<table class="table table-striped table-bordered">
			        <thead>
			            <tr>
			            	<th>Image</th>
			                <th>Name</th>
					        <th>Email</th>
					        <th>Roles</th>
					        <th>&nbsp;</th>
					    </tr>
			        </thead>
             		<tbody>
             	  	@foreach ($users as $i => $user)

             	  	@if(!$user->isAdmin())
             	  	  <tr>
             	  	  	  <td><img src="{{ $user->profile->getAvatar() }}" id="profile-avatar" alt="Image for Profile" width="100px" height="100px" /></td>
		                  <td>{{ $user->profile->name }}</td>
				          <td>{{ $user->email }}</td>
				          <td>

				          	<ol>
				          	@foreach($user->roles as $role)

				          		<li>{{ $role->name }}</li>

				          	@endforeach
				          	</ol>
				          	
				          </td>
				          <td>
				          	
							{{ link_to_route('profile.show', 'View', $user->profile->id , array('class' => 'btn btn-info')) }}
				          	<button name="btnDelete" data-value="{{ $user->id }}" type="button" class="btn btn-danger delete" >Remove</button>
				          </td>
				      </tr>
				    @endif
               		@endforeach

               		
               		  <tr>          	
		              	<td colspan="5" style="text-align:center;">
		              		
		              		{{  $users->links(); }}
		              	</td>
		              </tr>
		            
               		</tbody>
      
    				</table>
    				@else
					    There are no users
					@endif
               </div>

            </div>
     </section>

<div id="dialog-confirm" title="Are you sure?" style="display:none;">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This item will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>
@stop


@section('footerscript')

	<script type="text/javascript">

	$(function() {
    
		$('.delete').click(function(e){
			e.preventDefault();
			var btn = $(this);
			var id = $(this).data("value");
			var url = '<?=URL::to('/manage/user/remove')?>' + '/' + id;



			$( "#dialog-confirm" ).dialog({
		      resizable: false,
		      height: 200,
		      width: 500,
		      modal: true,
		      buttons: {
		        "Confirm": function() {
		          
		          deleteItem(url,btn);

		          $( this ).dialog( "close" );

		        },
		        Cancel: function() {
		          $( this ).dialog( "close" );
		        }
		      }
		    });
		

			
		});

	});

	function deleteItem(url,btn){
		$.ajax({
				type: 'POST',
			    url: url,
			    data: { "_token": "{{ csrf_token() }}" },
			    success: function(result) {
			       	$(btn).closest("tr").remove();
		          	
			    },
			    error: function(xhr, textStatus, errorThrown){
                           console.log(xhr.responseText);
                           
                }
			});

		
	}

	</script>

@stop