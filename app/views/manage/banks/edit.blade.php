@extends('layouts.default')

@section('contenthead')

   
@stop


@section('content')

	<section class="profile-page">
	<div class="container">
		<h1> Bank </h1>
			{{ Form::model($bank, array('method' => 'PATCH', 'route' => array('manage.bank.update', $bank->id))) }}
			<div class="form-group">
			    <label for="txtName">Name</label>
			    <input type="text" class="form-control" id="txtName" name="name" placeholder="Bank Name" value="{{ $bank->name }}">
			  </div>
			
		
			  <button type="submit" class="btn btn-info">Submit</button>
			 {{ link_to_route('manage.bank.index', 'Cancel', null, array('class' => 'btn btn-default')) }}
			{{ Form::close() }}
		
	</div>
   </section>

@stop


@section('footerscript')

@stop