@extends('layouts.default')

@section('contenthead')

   
@stop


@section('content')

	<section class="profile-page">
	<div class="container">
		<h1> Bank </h1>
			{{ Form::open(array('route' => 'manage.bank.store')) }}
			<div class="form-group">
			    <label for="txtName">Name</label>
			    <input type="text" class="form-control" id="txtName" name="name" placeholder="Bank Name" value="">
			  </div>
			
			  <button type="submit" class="btn btn-info">Submit</button>
			 {{ link_to_route('manage.bank.index', 'Cancel', null, array('class' => 'btn btn-default')) }}
			{{ Form::close() }}
		
	</div>
   </section>

@stop


@section('footerscript')

@stop