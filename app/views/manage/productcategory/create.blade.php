@extends('layouts.default')

@section('contenthead')

   
@stop


@section('content')

	<section class="profile-page">
	<div class="container">
		<h1> Product Category </h1>
			{{ Form::open(array('route' => 'manage.category.store')) }}
			<div class="form-group">
			    <label for="txtName">Name</label>
			    <input type="text" class="form-control" id="txtName" name="name" placeholder="Product Name" value="">
			  </div>
			
			  <div class="checkbox">
			    <label>
			     {{ Form::checkbox('active', 1, true) }} Active
			    </label>
			  </div>
			  <button type="submit" class="btn btn-info">Submit</button>
			 {{ link_to_route('manage.category.index', 'Cancel', null, array('class' => 'btn btn-default')) }}
			{{ Form::close() }}
		
	</div>
   </section>

@stop


@section('footerscript')

@stop