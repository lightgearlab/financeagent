@extends('layouts.default')

@section('contenthead')

   
@stop


@section('content')

	<section class="profile-page">
	<div class="container">
		<h1> Product Category </h1>
			{{ Form::model($category, array('method' => 'PATCH', 'route' => array('manage.category.update', $category->id))) }}
			<div class="form-group">
			    <label for="txtName">Name</label>
			    <input type="text" class="form-control" id="txtName" name="name" placeholder="Product Name" value="{{ $category->name }}">
			  </div>
			
			  <div class="checkbox">
			    <label>
			     {{ Form::checkbox('active', 1, $category->active) }} Active
			    </label>
			  </div>
			  <button type="submit" class="btn btn-info">Submit</button>
			 {{ link_to_route('manage.category.index', 'Cancel', null, array('class' => 'btn btn-default')) }}
			{{ Form::close() }}
		
	</div>
   </section>

@stop


@section('footerscript')

@stop