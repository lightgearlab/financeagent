@extends('layouts.default')

@section('contenthead')

   
@stop


@section('content')

	<section class="profile-page">
	<div class="container">
		<h1> Product Category </h1>
	<div class="row">

		<div class="form-group">
		    <label for="exampleInputEmail1">Name</label>
		    {{ $category->name }}
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Password</label>
		    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputFile">File input</label>
		    <input type="file" id="exampleInputFile">
		    <p class="help-block">Example block-level help text here.</p>
		  </div>
		  <div class="checkbox">
		    <label>
		      <input type="checkbox"> Check me out
		    </label>
		  </div>
		  <button type="submit" class="btn btn-default">Submit</button>
		</form>

		<ul>
        <li>{{ $category->name }}</li>

    	</ul>
		
	</div>

	<div class="row">
		<div class="col-md-4"><a class="btn btn-info" href="{{ URL::previous() }}">Back</a></div>
		
	</div>
		
	</div>
   </section>

@stop


@section('footerscript')

@stop