@extends('layouts.default')

@section('contenthead')

   
@stop


@section('content')
	
	
	 <section class="result-page">
            <div class="container">
            	<div class="row" style="text-align:right;">
               		{{ link_to('manage/', 'Back', $attributes = array(), $secure = null) }}
               </div>
               <div class="row">

               	<p>{{ link_to_route('manage.category.create', 'Add new category', null, array('class' => 'btn btn-primary')) }}</p>
               	@if ($categories->count())
               		<table class="table table-striped table-bordered">
			        <thead>
			            <tr>
			                <th>Name</th>
					        <th>Status</th>
					        <th>&nbsp;</th>
					    </tr>
			        </thead>
             		<tbody>
             	  	@foreach ($categories as $i => $category)
             	  	  <tr>
		                  <td>{{ 
							link_to('manage/category/' . $category->id ,  $category->name, $attributes = array(), $secure = null) }}</td>
				          <td>{{ $category->getStatus() }}</td>
				          <td>
				          	{{ link_to('manage/category/' . $category->id . '/edit',  "Edit", $attributes = array('data-value' => $category->id ,'class' => 'btn btn-info .edit'), $secure = null) }}

				          	<button name="btnDelete" data-value="{{ $category->id }}" type="button" class="btn btn-danger delete" >Remove</button>
				          </td>
				      </tr>
               		@endforeach

               		@if(count($categories) >= $pagesize)
               		  <tr>          	
		              	<td colspan="3" style="text-align:center;">
		              		
		              		{{  $categories->links(); }}
		              	</td>
		              </tr>
		             @endif
               		</tbody>
      
    				</table>
    				@else
					    There are no product category
					@endif
               </div>

            </div>
     </section>

<div id="dialog-confirm" title="Are you sure?" style="display:none;">
	<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This item will be permanently deleted and cannot be recovered. Are you sure?</p>
</div>
@stop


@section('footerscript')

	<script type="text/javascript">

	$(function() {
    
		$('.delete').click(function(e){
			e.preventDefault();
			var btn = $(this);
			var id = $(this).data("value");
			var url = '<?=URL::to('/manage/category/remove')?>' + '/' + id;



			$( "#dialog-confirm" ).dialog({
		      resizable: false,
		      height: 200,
		      width: 500,
		      modal: true,
		      buttons: {
		        "Confirm": function() {
		          
		            deleteItem(url,btn);

		          	$( this ).dialog( "close" );
		        },
		        Cancel: function() {
		          $( this ).dialog( "close" );
		        }
		      }
		    });
		

			
		});

	});

	function deleteItem(url,btn){
		$.ajax({
				type: 'POST',
			    url: url,
			    data: { "_token": "{{ csrf_token() }}" },
			    success: function(result) {
			       	$(btn).closest("tr").remove();
		          	
			    },
			    error: function(xhr, textStatus, errorThrown){
                           console.log(xhr.responseText);
                           
                }
			});

		
	}

	</script>

@stop