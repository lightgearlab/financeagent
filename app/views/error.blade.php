@extends('layouts.default')

@section('contenthead')

   
@stop


@section('content')
	
	<section class="profile-page">
		<div class="container">
			<h1>Error</h1>

			@if(Session::has("message"))
			<div class="alert alert-danger" style="font-size:20px;">

					{{ Session::get("message") }} 
			</div>
			@endif
			<br/>
			<a class="btn btn-info" href="{{ URL::previous() }}">back</a>
		</div>
	</section>

@stop


@section('footerscript')



@stop