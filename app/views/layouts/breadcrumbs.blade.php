@if ($breadcrumbs)

    <!-- Page Content -->
    <div class="header-top ">
        <div class="container-fluid bg-white">

            <div class="col-md-3 pd10">
                <ol class="breadcrumb bread-custom" style="margin-bottom:0px;background-color:#fff;">

			        @foreach ($breadcrumbs as $breadcrumb)
			            @if (!$breadcrumb->last)
			                <li><a href="{{{ $breadcrumb->url }}}">{{{ $breadcrumb->title }}}</a></li>
			            @else
			                <li class="active">{{{ $breadcrumb->title }}}</li>
			            @endif
			        @endforeach
                </ol>
            </div>
        </div>
        <!-- /.container -->
    </div>
   
@endif