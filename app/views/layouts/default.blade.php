 <!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Discover Finance Agents in Malaysia</title>

          <!-- Bootstrap Core CSS -->
      <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

      <!-- Custom CSS -->
      <link href="{{ asset('css/landing-page.css') }}" rel="stylesheet">
      <link href="{{ asset('css/style_custom/stylesheets/master_custom.css') }}" rel="stylesheet">

      <!-- Custom Fonts -->
      <link href="{{ asset('font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
      <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
       
      <link href="{{ asset('css/finance-custom.css') }}" rel="stylesheet">
      <script src="{{ asset('js/jquery.js') }}"></script>


      @yield('contenthead')
      
      <body class="@yield('body_class')">
        
      @include('layouts.topnavigation')

     

      @yield('content')

      <!-- jQuery -->

      <script src="{{ asset('js/custom.js') }}"></script>
      <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
      <script src="{{ asset('js/jquery.shorten.1.0.js') }}"></script>

      <!-- Bootstrap Core JavaScript -->
      <script src="{{ asset('js/bootstrap.min.js') }}"></script>
      <script src="{{ asset('js/numeral.min.js') }}"></script>
      
      <script src="{{ asset('js/finance.js') }}"></script>
    
      @yield('footerscript')

      <script type="text/javascript">

          $.ajaxSetup({ 
              headers: { 'csrftoken' : '{{ csrf_token() }}' },
              statusCode: {
                  401 : function(){
                       location.href = "{{ URL::to('/login') }}";
                  }
              }
          });
      </script>

            <div id="dialog-edit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
           <div class="modal-dialog">

             <!-- Modal content-->
             <div class="modal-content" id="dialog-edit-body">
                
                 
             </div>

          </div>
         </div>



   </body>
</html>
