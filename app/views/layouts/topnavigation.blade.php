
<!-- Navigation -->
<nav id="header" class="navbar navbar-default topnav set-menu nav_bar_color" role="navigation">
        <div id="header-container" class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header col-md-6  ">
                <button type="button" class="navbar-toggle mini-menu " data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
              <a class="navbar-brand topnav go-right " id="brand" style="padding-top:5px;" href="{{ URL::to('/') }}"><h2 class="brand-header"><img src="{{ asset('img/gof_logo.png') }}" width="150"></h2></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                 @if (Auth::check())

                 <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="{{URL::to('/')}}">Discover</a>
                    </li>
                    @if(Auth::user()->isAdmin())

                    <li>{{ link_to('manage/', 'Manage' , $attributes = array(), $secure = null) }}</li>

                    @endif
                    <li role="">

                        <a href="#" class="dropdown-toggle drop-down-dtl-set" data-toggle="dropdown"  role="button" aria-haspopup="true" aria-expanded="false">Saved<span class="badge badge-red">3</span> <span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-filter">
                            <li><a href="{{URL::to('/saved/agent')}}">Agents</a></li>
                            <li><a href="{{URL::to('/saved/product')}}">Products</a></li>
                           
                        </ul>
                    </li>  
                    <li role=""><a href="#">Messages <span class="badge badge-red">3</span></a></li>  
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle drop-down-dtl-set" data-toggle="dropdown"  role="button" aria-haspopup="true" aria-expanded="false">{{Auth::user()->profile->getDisplayName()}}<span class="caret"></span></a>
                        <ul class="dropdown-menu dropdown-filter">
                            <li>{{ link_to_route('profile.show', 'Accounts' ,array(Auth::user()->profile->uid), array('class' => '')) }}</li>
                            <li><a href="#">Dashboard</a></li>
                            <li><a href="#">Support</a></li>
                            <li><a href="{{URL::to('/logout')}}">Log Out</a></li>
                        </ul>
                    </li>
                </ul>
                @else
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="#">How it Works</a>
                    </li>
                    <li>
                        <a href="{{URL::to('/agent/register')}}">Signup Agent</a>
                    </li>
                    <li>
                        <a href="{{URL::to('/register')}}">Signup</a>
                    </li>
                    <li>
                        <a href="{{URL::to('/login')}}">Login</a>
                    </li>
                </ul>

                 @endif
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>