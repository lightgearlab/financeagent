<!-- <div class="container">

   <div class="row v-center">

      <div class="col-sm-3">

         <a class="brand" href="<?=URL::to('/')?>">FinanceAgent</a>

      </div>

      <div class="col-sm-2">

         <ul class="footerlist">

            <li><a class="" href="#">About Us</a></li>

            <li><a class="" href="#">Articles</a></li>

            <li><a class="" href="#">Jobs</a></li>

            <li><a class="" href="mailto:mac@dext.com.my">Contact Us</a></li>

            <li><a class="" href="mailto:mac@dext.com.my">Support</a></li>

         </ul>

      </div>

      <div class="col-sm-2">

         <ul class="footerlist">

            <li><a href="<?=URL::to('/search?service=Credit+Card')?>" class="#">Credit Card</a></li>

            <li><a href="<?=URL::to('/search?service=Insurance')?>" class="#">Insurance</a></li>

            <li><a href="<?=URL::to('/search?service=Hire+Purchase')?>" class="#">Car Loans</a></li>

            <li><a href="<?=URL::to('/search?service=Mortgage')?>" class="#">House Loans</a></li>

         </ul>

      </div>

      <div class="col-sm-2">

         <ul class="footerlist">

            <li><a class="" href="<?=URL::to('/search?service=')?>">Kuala Lumpur</a></li>

            <li><a class="" href="<?=URL::to('/search?service=')?>">Selangor</a></li>

            <li><a class="" href="<?=URL::to('/search?service=')?>">Cyberjaya</a></li>

            <li><a class="" href="<?=URL::to('/search?service=')?>">Putrajaya</a></li>

         </ul>

      </div>

      <div class="col-sm-3">

         <p>&copy; Financeagent 2016</p>

      </div>

   </div>

</div> -->


   <!-- Footer -->

    
    <footer class=" bg-white ">
        <div class="container">
            <div class="row foot-bottom"> 
                <div class="col-md-3 list-foot-menu">
                  <p><b>GoFinance</b></p>
                    <ul style="list-style:none;padding-lefT:0px;" class="font-thin">
                        <li><a href="">About Us</a></li>
                         <li><a href="">Career</a></li>
                         <li><a href="">Refferals</a></li>
                         <li><a href="">Support</a></li>
                         <li><a href="">Our Stores</a></li>
                    </ul>
                </div>
                <div class="col-md-3 list-foot-menu">
                  <p><b>Credit Card</b></p>
                    <ul style="list-style:none;padding-lefT:0px;" class="font-thin">
                        <li><a href="">Personal Loan</a></li>
                         <li><a href="">Car Loan</a></li>
                         <li><a href="">Car Insurance</a></li>
                         <li><a href="">health Insurance</a></li>
                         <li><a href="">Fixed Deposit</a></li>
                    </ul>
                </div> 
                <div class="col-md-3  list-foot-menu">
                     <p><b>Products</b></p>
                    <ul style="list-style:none;padding-lefT:0px;" class="font-thin">
                        <li><a href="">Bank Rakyat Classic Credit Card-i</a></li>
                         <li><a href="">Bank Rakyat Gold Credit Card-i</a></li>
                         <li><a href="">Bank Rakyat Platinum Credit Card-i</a></li>
                         <li><a href="">Bank Rakyat TISB Credit Card-i</a></li>
                    </ul>
                </div>
                <div class="col-md-3 foot-address">
                   <p><b>Contact Us</b></p>
                   
                    <p class="font-thin">
                    support@gofinance.co
                    <br>
Malaysian Global Innovation & Creativity Centre, Block 3730, Persiaran APEC, 63000 Cyberjaya, Malaysia.</p>
                
                </div>
                 <!--<div class="col-md-3  foot-social">
                   <p><b>SOCIAL</b></p>
                    <ul class="list-inline" >
                        <li><i class="fa fa-twitter" aria-hidden="true" ></i></li>
                        <li><i class="fa fa-google-plus" aria-hidden="true" ></i></li>
                        <li><i class="fa fa-facebook" aria-hidden="true" ></i></li>
                        <li> <i class="fa fa-instagram" aria-hidden="true" ></i></li>
                     </ul>
                </div>-->
            </div>
        </div>
        <div class="container border-footer">
            <div class="col-md-12 col-xs-12 text-copyright">
            <p class="font-thin pull-left"> &copy; 2016 GoFinance.co</p>
            </div>
        </div>
    </footer>