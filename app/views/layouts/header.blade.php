<div class="container">

               <div class="navbar col-sm-12" role="navigation">

                  <div class="navbar-header">

                     <button type="button" class="navbar-toggle"></button>

                     <a class="brand" href="<?=URL::to('/')?>">FinanceAgent</a>

                  </div>

                  <div class="collapse navbar-collapse pull-right">



                     @if (Auth::check())

                     <ul class="nav pull-left">
<!--<li><a href="<?=URL::to('/search?service=')?>">Discover</a></li>-->
                      

                        <li><a href="<?=URL::to('/saved')?>"> <span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span></a> <span class="badge icn-notification reserved">{{ Auth::user()->getReservedCount() }}</span></li>

						
                        <!--<li><a href="#"><span class="glyphicon glyphicon-envelope notready" aria-hidden="true"></span></a></li>

                        <li><a href="#"><span class="glyphicon glyphicon-bell notready" aria-hidden="true"></span></a></li>-->

                        <li class="dropdown user-nav-dropdown">

                           <a href="#" class="dropdown-toggle user-name-img" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">

                           {{ Auth::user()->profile->getDisplayName()  }}

                           <img src=" {{ Auth::user()->profile->getAvatar() }}" width="30" class=" img-circle img-profile">

                           </a>

                           <ul class="dropdown-menu user-dropdown-list">

                              @if(Auth::user()->isAdmin())

                              <li>{{ link_to('manage/', 'Manage' , $attributes = array(), $secure = null) }}</li>

                              @endif

                             

                              <li>{{ link_to_route('profile.show', 'Profile' ,array(Auth::user()->profile->uid), array('class' => '')) }}</li>

                               <!-- <li><a href="#">History</a></li>-->

                              <li><a class="" href="<?=URL::to('/logout')?>">Log Out</a></li>

                           </ul>

                        </li>

                     </ul>

                     @else

                     <div class="navbar-form pull-left">

                        <a class="btn btn-info" href="<?=URL::to('/agent/register')?>">Signup as agent</a>

                        <a class="btn btn-info" href="<?=URL::to('/login')?>">Log In</a>

                        <a class="btn btn-info" href="<?=URL::to('/register')?>">Sign Up</a>

                     </div>

                     @endif

                  </div>

               </div>

            </div>

