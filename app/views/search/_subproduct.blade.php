
@if($products != null && count($products) > 0)
@foreach ($products as $i => $product)
<div class="col-lg-12 col-mini-6 col-md-6 col-xs-12  list-profile-member  agent-list  ">

    <div class="col-lg-2 col-xs-12 col-md-10 col-md-offset-1  col-md-10 col-mini-8 col-mini-offset-1  col-lg-offset-0 mb10">
        <img src="{{ $product->getImageUrl() }}" width="117" height="117">
    </div>
    <div class="col-lg-5  col-sm-8 col-xs-12 col-md-10 col-md-offset-1  col-sm-offset-2 col-lg-offset-0  agent-list-profile">
        

        <a href="{{ URL::to('/product?sub='. $product->uid . '&primary=' . $MainProductId ) }}">
            <h4>{{ $product->name }}</h4>
        </a>
        <a href="{{ URL::to('search/agent?primary=' . $MainProductId . '&sub=' . $product->uid) }}">
            <p>All Finance Agents</p>
         </a>
         <a href="profile.html">
            <p>{{ $product->city }}</p>
        </a>
    </div>
    
      @if(Auth::check())
          <div class="col-lg-1 col-xs-2 col-mini-4   mt20 heart-profile   text-center"> 
            <span><i class="{{ $product->hasSaved() ? "fa fa-heart" : "fa fa-heart-o" }}" 
                     data-value="{{ $product->uid }}" 
                     url =  "{{ $product->hasSaved() ? URL::to('/product/saved/remove/'.$product->uid): URL::to('/product/saved/add?primary=' . $MainProductId .'&sub=' .$product->uid) }}"
                     data-status = "{{ $product->hasSaved() }}"
                     style="font-size:20px;" aria-hidden="true" 
                     onclick="saveProduct(this);"></i></span> 
          </div>
        @endif

    <div class="col-lg-2  col-xs-4 col-mini-4  col-lg-offset-0  mt25  ml15 agent-list-message">
        
    </div>
    <div class="col-lg-1  col-xs-4 col-mini-4  col-lg-offset-0 mt20   ">
         <a href="{{ URL::to('/product?sub='. $product->uid . '&primary=' . $MainProductId ) }}"><button type="button" class="btn btn-primary btn-md  apply-profile-btn" style="align-item:ceter;">Apply Now</button></a>
    </div>
</div>
<!--end of listt-->
@endforeach
@else
No Result
<br/>
<a class="btn btn-info" href="{{ URL::previous() }}">Back</a>
@endif

   <div class="row">
    <div class="col-md-6 col-md-offset-3">
       {{  $products->appends($q)->links(); }}
    </div>
 </div>



    <script type="text/javascript">

        function saveProduct(e){
            
            var link = $(e);
            var url = link.attr('url');
           
            $.post(url).done(function(data) {
              
                if(!data.error){
                   link.attr('url',data.result.url);
                    
                    if(data.result.status == 'removed'){                      
                        link.removeClass('fa-heart');
                        link.addClass('fa-heart-o');
                    }else{
                        link.removeClass('fa-heart-o');
                        link.addClass('fa-heart');
                    }
                }
            });
            
        }
        
        $(function(){



        });
    </script>


