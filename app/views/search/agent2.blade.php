@extends('layouts.default')
@section('contenthead')
@stop
@section('content')

<div class="header-top set-header-profile">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-3 mt10 breadcrumb-profile">
        <ol class="breadcrumb bread-custom" >
          <li><a href="#">Home</a></li>
          <li><a href="#">Credit Card</a></li>
          <li class="active">All Agents</li>
        </ol>
      </div>
      <div class="col-lg-5 col-xs-12  search-sub" >
        <div class="input-group ">
          <input type="text" class="form-control search-tab" placeholder="Search ">
          <span class="input-group-btn">
          <button class="btn btn-default  icon-search" type="button">&nbsp; <i class="fa fa-search search-size" aria-hidden="true"></i>&nbsp; </button>
          </span> </div>
      </div>
      <div class="col-lg-2 col-xs-6  text-center section-dropdown-menu" >
        <div>
          <ul class="nav navbar-nav set-dropdown-menu sub-menu-dropdown">
            <li class="dropdown"> <a href="#" class="dropdown-toggle drop-down-dtl-set" data-toggle="dropdown"  role="button" aria-haspopup="true" aria-expanded="false">Product<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Credit Card</a></li>
                <li><a href="#">Insurance</a></li>
                <li><a href="#">Fixed Deposit</a></li>
                <li><a href="#">Micro Finance</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-lg-2 col-xs-6 text-center section-dropdown-menu" >
        <div>
          <ul class="nav navbar-nav set-dropdown-menu sub-menu-dropdown">
            <li class="dropdown"> <a href="#" class="dropdown-toggle drop-down-dtl-set" data-toggle="dropdown"  role="button" aria-haspopup="true" aria-expanded="false">Location<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="#">Kuala Lumpur</a></li>
                <li><a href="#">Cyberjaya</a></li>
                <li><a href="#">Selangor</a></li>
                <li><a href="#">Klang</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
      
      <!--wdwd--> 
      
    </div>
  </div>
  
  <!-- /.container --> 
  
</div>

<!--new section-->

<section class="content-section-a-no-bottom">
  <div class="container-fluid">
    <div class="col-lg-8 ">
      <div class="col-lg-12  col-xs-12    agent-list  ">
        <div class="col-lg-6">
          <h4>Pick an Agent to be in touch</h4>
        </div>
        <div class="col-lg-6">
          <a href="application_step.html" class="pull-right"><p>Auto assign agent for me</p></a>
        </div>
      </div>
    </div>
    <div id="divSearchList"  class="col-lg-8 col-xs-12  mb10 ">
      @include('search._agent')
        
        <div class="col-lg-12 col-mini-6 col-md-6 col-xs-12  list-profile-member  agent-list  ">
            <a id="lnkChoose" href="#" type="button" class="btn btn-primary agent-pick-skip pull-right">Choose for Me</a>
      </div>
    </div>
    
    <!--end of templte-->
    
    <div class="col-lg-4 col-xs-12 right-side-profile  ">
      <div class="col-lg-12">
        <div class="row "> <img src="{{ asset('img/assets/ads_profile.jpg') }}" class="img-responsive"> </div>
        <div class="row  bg-white">
          <div class="col-lg-8 col-md-8  col-md-offset-2  col-lg-offset-2 mt50 pb20">
            <button type="button" class=" btn-block round-outer-btn">
            <p>Discover</p>
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@stop
@section('footerscript')
    <script type="text/javascript">
        
        $(function(){
            
            
            $("#lnkChoose").click(function(e){
                
                e.preventDefault();
                
                
                
            });
        });
    </script>
@append

