@extends('layouts.default')
@section('contenthead')
@stop
@section('content')


 <div class="header-top set-header-profile">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 mt10 breadcrumb-profile">
                    <ol class="breadcrumb bread-custom" >
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Credit Card</a></li>
                        <li class="active">All Cards</li>
                    </ol>
                </div>
                <div class="col-lg-5 col-xs-12  search-sub" >
                    <div class="input-group ">
                        <input type="text" class="form-control search-tab" placeholder="Search ">
                        <span class="input-group-btn">
                      <button class="btn btn-default  icon-search" type="button">&nbsp;
                          <i class="fa fa-search search-size" aria-hidden="true"></i>&nbsp;
                     </button>
                </span>
                    </div>
                </div>

                <div class="col-lg-2 col-xs-6  text-center section-dropdown-menu" >
                    <div>
                        <ul class="nav navbar-nav set-dropdown-menu sub-menu-dropdown">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle drop-down-dtl-set" data-toggle="dropdown"  role="button" aria-haspopup="true" aria-expanded="false">Filter By<span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-filter">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                   
                                    <li class="dropdown-header">Nav header</li>
                                    <li><a href="#">Separated link</a></li>
                                   
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                 <div class="col-lg-2 col-xs-6 text-center section-dropdown-menu" >
                    <div>
                        <ul class="nav navbar-nav set-dropdown-menu sub-menu-dropdown">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle drop-down-dtl-set" data-toggle="dropdown"  role="button" aria-haspopup="true" aria-expanded="false">Location<span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-filter">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                   
                                    <li class="dropdown-header">Nav header</li>
                                    <li><a href="#">Separated link</a></li>
                                   
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--wdwd-->
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!--new section-->
    <section class="content-section-a-no-bottom">

        <div class="container-fluid ">

            <div class="col-lg-8 ">
                <div class="col-lg-12  col-xs-12    agent-list  ">
                    <div class="col-lg-12">
                        <h4>Sub Products</h4> </div>

                </div>
            </div>
            <div class="col-lg-8 col-xs-12  mb10 ">
                
                @if($products != null && count($products) > 0)
                @foreach ($products as $i => $product)
                <div class="col-lg-12 col-mini-6 col-md-6 col-xs-12  list-profile-member  agent-list  ">

                    <div class="col-lg-2 col-xs-12 col-md-10 col-md-offset-1  col-md-10 col-mini-8 col-mini-offset-1  col-lg-offset-0 mb10">
                        <img src="{{ $product->getImageUrl() }}" width="117" height="117">
                    </div>
                    <div class="col-lg-5  col-sm-8 col-xs-12 col-md-10 col-md-offset-1  col-sm-offset-2 col-lg-offset-0  agent-list-profile">
                        

                        <a href="{{ $product->getProductDetailUrl() }}">
                            <h4>{{ $product->name }}</h4>
                        </a>
                        <a href="profile.html">
                             <a href=""><p>All Finance Agents</p></a>
                         </a>
                         <a href="profile.html">
                            <p>{{ $product->city }}</p>
                        </a>
                    </div>
                    <div class="col-lg-1 col-xs-2 col-mini-4   mt20 heart-profile   text-center">
                        <span><i class="fa fa-heart-o" style="font-size:20px;" aria-hidden="true"></i></span>

                    </div>
                    <div class="col-lg-2  col-xs-4 col-mini-4  col-lg-offset-0  mt25  ml15 agent-list-message">
                        
                    </div>
                    <div class="col-lg-1  col-xs-4 col-mini-4  col-lg-offset-0 mt20   ">
                         <a href="product_page.html"><button type="button" class="btn btn-primary btn-md  apply-profile-btn" style="align-item:ceter;">Apply Now</button></a>
                    </div>
                </div>
                <!--end of listt-->
                @endforeach
                @else
                No Result
                <br/>
                <a class="btn btn-info" href="{{ URL::previous() }}">Back</a>
                @endif

                   <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                       {{  $products->appends($q)->links(); }}
                    </div>
                 </div>
                
            </div>
            <!--end of templte-->


            <div class="col-lg-4 col-xs-12 right-side-profile  ">
                <div class="col-lg-12">
                    <div class="row ">
                        <img src="img/assets/ads-profile2.jpg" class="img-responsive">
                    </div>
                    <div class="row  bg-white">
                        <div class="col-lg-8 col-md-8  col-md-offset-2  col-lg-offset-2 mt50 pb20">
                            <button type="button" class=" btn-block round-outer-btn">
                                <p>Discover</p>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
 
<!-- <section class="result-page">
   <div class="container">
   <div class="row">
      <form id="formSearch" name="formSearch" class="form-inline" action="<?=URL::to('/search/product')?>" method="GET">
         <div class="col-sm-12 resultpage-filter paddingzero">
            <div class="col-sm-6 paddingzero">
               <div class="form-group">
                  <input id="txtKeyword" name="keyword" type="text" class="form-control searchfilter"  placeholder="Search" value="{{ (array_key_exists('keyword',$q) ? $q['keyword'] : '' ) }}">
               </div>
               <button type="submit" class="btn btn-default btn-search">Go</button>
            </div>
            <div class="col-sm-2 paddingzero">
               <div class="btn-group" role="group">
                  <button id="btnService" name="btnService" type="button" class="btn btn-default dropdown-toggle btn-filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {{ (array_key_exists('service',$q) ? $q['service'] : 'Services' ) }} 
                  <span class="caret"></span>
                  </button>
                  <ul id="ddlService" name="ddlService" class="dropdown-menu">
                     @foreach ($categories as $i => $category)
                     <li data-value="{{ $category->name }}">
                        <a href="#">  {{ $category->name }} </a>
                     </li>
                     @endforeach
                  </ul>
                  <input id="hidService" name="service" type="hidden" value="{{ (array_key_exists('service',$q) ? $q['service'] : '' ) }}" />
               </div>
            </div>
            <div class="col-sm-2 paddingzero">
               <div class="btn-group" role="group">
                  <button id="btnBank" name="btnBank" type="button" class="btn btn-default dropdown-toggle btn-filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {{ (array_key_exists('bank',$q) ? $q['bank'] : 'Banks' ) }} 
                  <span class="caret"></span>
                  </button>
                  <ul id="ddlBank" name="ddlBank" class="dropdown-menu">
                     @foreach ($banks as $i => $oBank)
                     <li data-value="{{ $oBank->name }}">
                        <a href="#">{{ $oBank->name }}</a>
                     </li>
                     @endforeach
                  </ul>
                  <input id="hidBank" name="bank" type="hidden" value="{{ (array_key_exists('bank',$q) ? $q['bank'] : '' ) }}" />
               </div>
            </div>
            <div class="col-sm-2 paddingzero">
               <div class="btn-group" role="group">
                  <button  id="btnAgent" name="btnAgent" type="button" class="btn btn-default dropdown-toggle btn-filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {{ (array_key_exists('rating',$q) ? $q['rating'] : 'Ratings' ) }} 
                  <span class="caret"></span>
                  </button>
                  <ul id="ddlRating" name="ddlRating" class="dropdown-menu">
                     <li data-value="highest"><a href="#">Highest</a></li>
                     <li data-value="lowest"><a href="#">Lowest</a></li>
                  </ul>
                  <input id="hidRating" name="rating" type="hidden" value="{{ (array_key_exists('rating',$q) ? $q['rating'] : '' ) }}" />
               </div>
            </div>
         </div>
      </form>
   </div>
   <div id="divResult" class="row">
      @if($products != null && count($products) > 0)
      @foreach ($products as $i => $product)
      <div class="col-sm-4 agent-list {{  ($i + 1) % 3 != 0 ? 'marginright' : '' }}">
         <div class="col-sm-3 paddingzero">
            <img src="{{ $product->getImageUrl() }}" class="img-rounded" width="100">
         </div>
         <div class="col-sm-9 cardinfo text-left">
            <h6>
               {{ link_to_route('product.show', $product->name ,array($product->id), array('class' => '')) }}
            </h6>
       
            @if($product->agent != null)
            <p>
              By {{ $product->agent->user->profile->name }}
            </p>
            @endif
         
            @if($product->agent->user->location != null)
            <p>
               {{ $product->agent->user->location->city }}
            </p>
            @endif
            


         </div>
         <div class="col-sm-3 text-left">
&nbsp;
</div>
                  
         
          <div class="col-sm-5 text-left">
            @if(Auth::check() && !$product->agent->user->isLoginUser() && !$product->hasSaved())
            <a class="btn btn-info btn-save-agent pull-right saveagent" href="#" data-value="{{ $product->id }}" ><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span></a>
            @endif
         </div>
         
      </div>
      @endforeach
      @else
      No Result
      <br/>
      <a class="btn btn-info" href="{{ URL::previous() }}">Back</a>
      @endif
   </div>
   <div class="row">
      <div class="col-md-6 col-md-offset-3">
         {{  $products->appends($q)->links(); }}
      </div>
   </div>
</section> -->
@stop
@section('footerscript')
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript">
   $(document).ready(function(){
   
      var serviceOptions =  $("#ddlService").children('li');
   
      $("#ddlService").on("click", "li", function() {
          serviceOptions.removeClass('selected');
          $(this).addClass('selected');
          $("#btnService").html($(this).html() + "<span class='caret'></span>");
          $("#hidService").val($("#ddlService").find(".selected").data("value"));
   
          $("#formSearch").submit();
      });
   
   
      var bankOptions =  $("#ddlBank").children('li');
   
      $("#ddlBank").on("click", "li", function() {
          bankOptions.removeClass('selected');
          $(this).addClass('selected');
          $("#btnBank").html($(this).html() + "<span class='caret'></span>");
          $("#hidBank").val($("#ddlBank").find(".selected").data("value"));
   
          $("#formSearch").submit();
      });
   
       var agentOptions =  $("#ddlRating").children('li');
   
      $("#ddlRating").on("click", "li", function() {
          agentOptions.removeClass('selected');
          $(this).addClass('selected');
          $("#btnRating").html($(this).html() + "<span class='caret'></span>");
          $("#hidRating").val($("#ddlRating").find(".selected").data("value"));
   
          $("#formSearch").submit();
      });
   
   
     $(".saveagent").click(function(e){
       
       e.preventDefault();
        var btnSave = $(this);
   
        var itemId = btnSave.data("value");  
   
        var dataString = 'itemId='+ itemId + "&type=2";           
   
         $.ajax({
             type: "POST",
             url : '<?=URL::to('/product/save')?>',
             data: dataString,
             success : function(data){
                 
               
               if(!data.error){
   
                 $(btnSave).hide();
                 $('.reserved').text(data.result);
   
               }else{
   
               }
   
              
             },
             error: function(xhr, textStatus, errorThrown){
                console.log(xhr.responseText);
             }
         });
     });
   
   
   });
   
   
   
   
     $(".messageme").click(function(e){
        e.preventDefault();
   
        var isLogin = '<?=Auth::check()?>';
   
        if(isLogin){
           $.ajax({
               type: "GET",
               url : $(this).attr('href'),
               cache: false,
               success : function(data){
                   
                  $("#dialog-edit-body").html(data.result);
   
                  $("#dialog-edit").modal({ show: true });
               },
               error: function(xhr, textStatus, errorThrown){
                  console.log(xhr.responseText);
               }
           });
   
           }else{
              window.location.href = '<?=URL::to('/login')?>'; 
           }
      });
    
   
</script>
@append