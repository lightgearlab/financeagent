@extends('layouts.default')
@section('contenthead')
@stop
@section('content')
<section class="result-page">
   <div class="container">
   <div class="row">
      <form id="formSearch" name="formSearch" class="form-inline" action="<?=URL::to('/search/agent')?>" method="GET">
         <div class="col-sm-12 resultpage-filter paddingzero">
            <div class="col-sm-6 paddingzero">
               <div class="form-group">
                  <input id="txtKeyword" name="keyword" type="text" class="form-control searchfilter"  placeholder="Find location, company, service" value="{{ (array_key_exists('keyword',$q) ? $q['keyword'] : '' ) }}">
               </div>
               <button type="submit" class="btn btn-default btn-search">Go</button>
            </div>
            <div class="col-sm-2 paddingzero">
               <div class="btn-group" role="group">
                  <button id="btnService" name="btnService" type="button" class="btn btn-default dropdown-toggle btn-filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {{ (array_key_exists('service',$q) ? $q['service'] : 'Services' ) }} 
                  <span class="caret"></span>
                  </button>
                  <ul id="ddlService" name="ddlService" class="dropdown-menu">
                     @foreach ($products as $i => $product)
                     <li data-value="{{ $product->name }}">
                        <a href="#">  {{ $product->name }} </a>
                     </li>
                     @endforeach
                  </ul>
                  <input id="hidService" name="service" type="hidden" value="{{ (array_key_exists('service',$q) ? $q['service'] : '' ) }}" />
               </div>
            </div>
            <div class="col-sm-2 paddingzero">
               <div class="btn-group" role="group">
                  <button id="btnBank" name="btnBank" type="button" class="btn btn-default dropdown-toggle btn-filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {{ (array_key_exists('bank',$q) ? $q['bank'] : 'Banks' ) }} 
                  <span class="caret"></span>
                  </button>
                  <ul id="ddlBank" name="ddlBank" class="dropdown-menu">
                     @foreach ($banks as $i => $oBank)
                     <li data-value="{{ $oBank->name }}">
                        <a href="#">{{ $oBank->name }}</a>
                     </li>
                     @endforeach
                  </ul>
                  <input id="hidBank" name="bank" type="hidden" value="{{ (array_key_exists('bank',$q) ? $q['bank'] : '' ) }}" />
               </div>
            </div>
            <div class="col-sm-2 paddingzero">
               <div class="btn-group" role="group">
                  <button  id="btnAgent" name="btnAgent" type="button" class="btn btn-default dropdown-toggle btn-filter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  {{ (array_key_exists('rating',$q) ? $q['rating'] : 'Ratings' ) }} 
                  <span class="caret"></span>
                  </button>
                  <ul id="ddlRating" name="ddlRating" class="dropdown-menu">
                     <li data-value="highest"><a href="#">Highest</a></li>
                     <li data-value="lowest"><a href="#">Lowest</a></li>
                  </ul>
                  <input id="hidRating" name="rating" type="hidden" value="{{ (array_key_exists('rating',$q) ? $q['rating'] : '' ) }}" />
               </div>
            </div>
         </div>
      </form>
   </div>
   <div id="divResult" class="row">
      @if($agents != null && count($agents) > 0)
      @foreach ($agents as $i => $agent)
      <div class="col-sm-4 agent-list {{  ($i + 1) % 3 != 0 ? 'marginright' : '' }}">
         <div class="col-sm-3 paddingzero">
            <img src="{{ $agent->user->profile->getAvatar() }}" class="img-rounded" width="100">
         </div>
         <div class="col-sm-9 cardinfo text-left">
            <h6>
               {{ link_to_route('profile.show', $agent->user->profile->name ,array($agent->user->profile->uid), array('class' => '')) }}
            </h6>
            <p class="agent-rating"> 
               {{ $agent->user->profile->getRating() }} / 5  
            </p>
            <p>{{ $agent->user->profile->biography }}</p>
            @if($agent->bank != null)
            <p>
               <strong>{{ $agent->bank->name }}</strong>                        
            </p>
            @endif
            @if($agent->hasServices())
            <p>
               {{ implode(",",$agent->getServices()) }}
            </p>
            @endif
            @if($agent->user->location != null)
            <p>
               {{ $agent->user->location->address }}
            </p>
            @endif
            
         </div>
         <div class="col-sm-3 text-left">
&nbsp;
</div>

         <div class="col-sm-4 text-left">
            @if(!$agent->user->isLoginUser())
            <a id="lnkMessage" href="<?=URL::to('/profile/message') . '/'. $agent->user->profile->id ?> " class="btn btn-contact-me messageme">Message Me</a>
            @endif
         </div>
         <div class="col-sm-5 text-left">
            @if(Auth::check() && !$agent->user->isLoginUser() && !$agent->hasSaved())
            <a class="btn btn-info btn-save-agent pull-right saveagent" href="#" data-value="{{ $agent->id }}" ><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span></a>
            @endif
         </div>
         
         
         
         
      </div>
      @endforeach
      @else
      No Result
      <br/>
      <a class="btn btn-info" href="{{ URL::previous() }}">Back</a>
      @endif
   </div>
   <div class="row">
      <div class="col-md-6 col-md-offset-3">
         {{  $agents->appends($q)->links(); }}
      </div>
   </div>
</section>
@stop
@section('footerscript')
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script type="text/javascript">
   $(document).ready(function(){
   
      var serviceOptions =  $("#ddlService").children('li');
   
      $("#ddlService").on("click", "li", function() {
          serviceOptions.removeClass('selected');
          $(this).addClass('selected');
          $("#btnService").html($(this).html() + "<span class='caret'></span>");
          $("#hidService").val($("#ddlService").find(".selected").data("value"));
   
          $("#formSearch").submit();
      });
   
   
      var bankOptions =  $("#ddlBank").children('li');
   
      $("#ddlBank").on("click", "li", function() {
          bankOptions.removeClass('selected');
          $(this).addClass('selected');
          $("#btnBank").html($(this).html() + "<span class='caret'></span>");
          $("#hidBank").val($("#ddlBank").find(".selected").data("value"));
   
          $("#formSearch").submit();
      });
   
       var agentOptions =  $("#ddlRating").children('li');
   
      $("#ddlRating").on("click", "li", function() {
          agentOptions.removeClass('selected');
          $(this).addClass('selected');
          $("#btnRating").html($(this).html() + "<span class='caret'></span>");
          $("#hidRating").val($("#ddlRating").find(".selected").data("value"));
   
          $("#formSearch").submit();
      });
   
   
     $(".saveagent").click(function(e){
       
       e.preventDefault();
        var btnSave = $(this);
   
        var itemId = btnSave.data("value");  
   
        var dataString = 'itemId='+ itemId + "&type=1";           
   
         $.ajax({
             type: "POST",
             url : '<?=URL::to('/agent/save')?>',
             data: dataString,
             success : function(data){
                 
               
               if(!data.error){
   
                 $(btnSave).hide();
                 $('.reserved').text(data.result);
   
               }else{
   
               }
   
              
             },
             error: function(xhr, textStatus, errorThrown){
                console.log(xhr.responseText);
             }
         });
     });
   
   
   });
   
   
   
   
     $(".messageme").click(function(e){
        e.preventDefault();
   
        var isLogin = '<?=Auth::check()?>';
   
        if(isLogin){
           $.ajax({
               type: "GET",
               url : $(this).attr('href'),
               cache: false,
               success : function(data){
                   
                  $("#dialog-edit-body").html(data.result);
   
                  $("#dialog-edit").modal({ show: true });
               },
               error: function(xhr, textStatus, errorThrown){
                  console.log(xhr.responseText);
               }
           });
   
           }else{
              window.location.href = '<?=URL::to('/login')?>'; 
           }
      });
   
   
   /*function initialize() {
   
   var input = document.getElementById('location');
   var autocomplete = new google.maps.places.Autocomplete(input);
   }
   
   google.maps.event.addDomListener(window, 'load', initialize);
   
   
   function GetLatlong()
   {
        var geocoder = new google.maps.Geocoder();
        var address = document.getElementById('location').value;
   
        geocoder.geocode({ 'address': address }, function (results, status) {
   
            if (status == google.maps.GeocoderStatus.OK) {
                var latitude = results[0].geometry.location.lat();
                var longitude = results[0].geometry.location.lng();
   
            }
        });
   }*/
   
   
   
   
   
</script>
@stop