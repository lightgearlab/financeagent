 @if($agents != null && count($agents) > 0)
      @foreach ($agents as $i => $agent)

      <div class="col-lg-12 col-mini-6 col-md-6 col-xs-12  list-profile-member  agent-list  ">
        <div class="col-lg-2 col-xs-12 col-md-10 col-md-offset-1  col-md-10 col-mini-8 col-mini-offset-1  col-lg-offset-0 mb10"> 
        <a href="{{ $agent->user->profile->getProfileUrl() }}"> 
          <img src="{{ $agent->user->profile->getAvatar() }}" class="img-responsive"></a> 
        </div>
        <div class="col-lg-5  col-sm-8 col-xs-12 col-md-10 col-md-offset-1  col-sm-offset-2 col-lg-offset-0  ">
          <h4>{{ link_to_route('profile.show', $agent->user->profile->name ,array($agent->user->profile->uid), array('class' => '')) }}</h4>
          <p>

            @foreach($lists = $agent->getPrimaryProduct() as $i => $primary)

                @if($i != count($lists)-1)
                  <a href="{{ URL::to('/search/product?scope=subproduct&primary=' . $primary->uid ) }}">{{ $primary->name }}</a>,
                @else
                  <a href="{{ URL::to('/search/product?scope=subproduct&primary=' . $primary->uid ) }}">{{ $primary->name }}</a>
                @endif

            @endforeach
           

          </p>
            @if($agent->user->location != null)
            <p>
               {{ $agent->user->location->city }}
            </p>
            @endif
        </div>
          
       @if(Auth::check() && !$agent->user->isLoginUser())
          <div class="col-lg-1 col-xs-2 col-mini-4   mt20   text-center"> 
            <span><i class="{{ $agent->hasSaved() ? "fa fa-heart" : "fa fa-heart-o" }}" 
                     data-value="{{ $agent->id }}" 
                     url =  "{{ $agent->hasSaved() ? URL::to('/agent/saved/remove/'.$agent->uid): URL::to('/agent/saved/add/'.$agent->uid) }}"
                     data-status = "{{ $agent->hasSaved() }}"
                     style="font-size:30px;" aria-hidden="true" 
                     onclick="saveAgent(this);"></i></span> 
          </div>
        @endif
        
        <div class="col-lg-2  col-xs-4 col-mini-4   col-lg-offset-0  mt25  ml15"> <span class="message-profile-btn">Message</span> </div>
          
        @if($MainProductId != '' && $SubProductId != '')
        <div class="col-lg-1  col-xs-4 col-mini-4     col-lg-offset-0 mt20   "> 
            <form action="{{ URL::to('/application/apply') }}">
                <input type="hidden" name="MainProductId" value="{{$MainProductId}}" />
                <input type="hidden" name="SubProductId" value="{{$SubProductId}}" />
                <input type="hidden" name="AgentId" value="{{ $agent->uid }}" />
                <a href="#" 
                     type="button" class="btn btn-primary btn-md  apply-profile-btn btn-apply" style="align-item:ceter;">Apply</a>
            </form>
        </div>
        @elseif($MainProductId != '' && $SubProductId == '')
          <div class="col-lg-1  col-xs-4 col-mini-4     col-lg-offset-0 mt20   "> 
            <a href="{{ URL::to('/search/product?scope=subproduct&primary='.$MainProductId) }}" 
                     type="button" class="btn btn-primary btn-md  apply-profile-btn" style="align-item:ceter;">Apply</a>
          </div>
        @endif
      </div>


        @endforeach
      @else
      No Result
      <br/>
      <a class="btn btn-info" href="{{ URL::previous() }}">Back</a>
      @endif

         <div class="row">
      <div class="col-md-6 col-md-offset-3">
         {{  $agents->appends($q)->links(); }}
      </div>
   </div>


    <script type="text/javascript">

            function saveAgent(e){

                var link = $(e);
                var url = link.attr('url');
                var status = link.data('status');

                $.post(url).done(function(data) {

                    if(!data.error){
                       link.attr('url',data.result.url);

                        if(data.result.status == 'removed'){                      
                            link.removeClass('fa-heart');
                            link.addClass('fa-heart-o');
                        }else{
                            link.removeClass('fa-heart-o');
                            link.addClass('fa-heart');
                        }
                    }
                });

            }
        

        $(document).on("click",".btn-apply",function(e){

            var button = $(this);
            var form = $(button).closest("form");
            e.preventDefault();
            $.post(form.attr('action'),$(form).serialize()).done(function(data) {    
            if(!data.error)
                window.location = data.result.redirect_url;
            });
        });
    

    </script>