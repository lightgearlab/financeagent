@extends('layouts.default')
@section('contenthead')
@stop
@section('content')

<div class="header-top set-header-profile">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 mt10 breadcrumb-profile">
                <ol class="breadcrumb bread-custom" >
                    <li><a href="#">Home</a></li>
                    <li class="active">Products</li>
                </ol>
            </div>
            <div class="col-lg-5 col-xs-12  search-sub" >
                <div class="input-group ">
                    <input type="text" class="form-control search-tab" placeholder="Search ">
                    <span class="input-group-btn">
                  <button class="btn btn-default  icon-search" type="button">&nbsp;
                      <i class="fa fa-search search-size" aria-hidden="true"></i>&nbsp;
                 </button>
            </span>
                </div>
            </div>

            <div class="col-lg-2 col-xs-6  text-center section-dropdown-menu" >
                <div>
                    <ul class="nav navbar-nav set-dropdown-menu sub-menu-dropdown">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle drop-down-dtl-set" data-toggle="dropdown"  role="button" aria-haspopup="true" aria-expanded="false">Filter By<span class="caret"></span></a>
                            <ul class="dropdown-menu dropdown-filter">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                               
                                <li class="dropdown-header">Nav header</li>
                                <li><a href="#">Separated link</a></li>
                               
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
             <div class="col-lg-2 col-xs-6 text-center section-dropdown-menu" >
                <div>
                    <ul class="nav navbar-nav set-dropdown-menu sub-menu-dropdown">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle drop-down-dtl-set" data-toggle="dropdown"  role="button" aria-haspopup="true" aria-expanded="false">Location<span class="caret"></span></a>
                            <ul class="dropdown-menu dropdown-filter">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                               
                                <li class="dropdown-header">Nav header</li>
                                <li><a href="#">Separated link</a></li>
                               
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <!--wdwd-->
        </div>
    </div>
    <!-- /.container -->
</div>

<section class="content-section-a-no-bottom">

    <div class="container-fluid ">

        <div class="col-lg-8 ">
            <div class="col-lg-12  col-xs-12    agent-list  ">
                <div class="col-lg-12">
                    <h4>{{ $PageTitle }}</h4> </div>

            </div>
        </div>
        <div id="divSearchList" data-url="{{ $SearchURL }}" class="col-lg-8 col-xs-12  mb10 "></div>
        <div class="col-lg-4 col-xs-12 right-side-profile  ">
            <div class="col-lg-12">
                <div class="row ">
                    <img src="{{ asset('img/assets/ads_profile.jpg') }}" class="img-responsive">
                </div>
                <div class="row  bg-white">
                    <div class="col-lg-8 col-md-8  col-md-offset-2  col-lg-offset-2 mt50 pb20">
                        <button type="button" class=" btn-block round-outer-btn">
                            <p>Discover</p>
                        </button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

@stop
@section('footerscript')

    <script type="text/javascript">

            function getProducts(){
                var url = $("#divSearchList").data("url");

                $.get(url).done(function( data ) {

                    $("#divSearchList").empty();

                    if(!data.error) $("#divSearchList").html(data.result);
                });
            }

            $(function(){


                getProducts();


                $('#divSearchList').on('click','.pagination a', function (event) {

                    event.preventDefault();

                    var url = $(this).attr("href");


                    $.get(url).done(function( data ) {

                    $("#divSearchList").empty();

                    if(!data.error) $("#divSearchList").html(data.result);
                    });

                    //getProducts();
                });

            });
    </script>
@append

