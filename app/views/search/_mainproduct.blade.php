@if($productCategories != null && count($productCategories) > 0)
  @foreach ($productCategories as $i => $product)

    <div class="col-lg-12 col-mini-6 col-md-6 col-xs-12  list-profile-member  agent-list  ">

        <div class="col-lg-2 col-xs-12 col-md-10 col-md-offset-1  col-md-10 col-mini-8 col-mini-offset-1  col-lg-offset-0 mb10">
            <img src="{{ $product->getImageUrl() }}" class="img-responsive">
        </div>
        <div class="col-lg-5  col-sm-8 col-xs-12 col-md-10 col-md-offset-1  col-sm-offset-2 col-lg-offset-0  agent-list-profile">
            
            <a href="#">
                <h4>{{ $product->name }}</h4>
            </a>
            <a href="{{ URL::to('/search/agent?primary=' . $product->uid) }}">
                 <p>Discover agents</p>
             </a>
             <a href="profile.html">
                <p>{{ $product->city }}</p>
            </a>
        </div>

        
        <div class="col-md-2 col-lg-2  col-xs-4 col-mini-4 mt20 pull-right">
             <a href="{{ URL::to('/search/product?scope=subproduct&primary='.$product->uid) }}"><button type="button" class="btn btn-primary btn-md  apply-profile-btn" style="align-item:ceter;">View More</button></a>
        </div>
    </div>

    @endforeach
  @else
        No Result
        <br/>
        <a class="btn btn-info" href="{{ URL::previous() }}">Back</a>
  @endif 