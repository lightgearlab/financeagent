@extends('layouts.default')
@section('contenthead')
@stop
@section('content')

<div class="content-section-a">
<div class="container-fluid">
    
    
    <div class="col-lg-8 col-xs-12  mb10 ">
     @if($agentSaves != null && count($agentSaves) > 0)
      @foreach ($agentSaves as $i => $agentSave) 
                <div class="col-lg-12 col-mini-6 col-md-6 col-xs-12  list-profile-member  agent-list  ">

                    <div class="col-lg-2 col-xs-12 col-md-10 col-md-offset-1  col-md-10 col-mini-8 col-mini-offset-1  col-lg-offset-0 mb10">
                        <img src="{{ $agentSave->agent->user->profile->getAvatar() }}" width="117" height="117">
                    </div>
                    <div class="col-lg-5  col-sm-8 col-xs-12 col-md-10 col-md-offset-1  col-sm-offset-2 col-lg-offset-0  agent-list-profile">
                        
                        <a href="{{ $agentSave->agent->user->profile->getProfileUrl() }}">
                            <h4>{{$agentSave->agent->user->profile->name}}</h4>
                            
                        </a>
                         <p>

                            @foreach($lists = $agentSave->agent->getPrimaryProduct() as $i => $primary)

                                @if($i != count($lists)-1)
                                  <a href="{{ URL::to('/search/product?scope=subproduct&primary=' . $primary->id ) }}">{{ $primary->name }}</a>,
                                @else
                                  <a href="{{ URL::to('/search/product?scope=subproduct&primary=' . $primary->id ) }}">{{ $primary->name }}</a>
                                @endif

                            @endforeach
                            </p>
                        
                         <a href="{{ $agentSave->agent->user->profile->getProfileUrl() }}">
                            <p> {{ $agentSave->agent->user->getUserLocation() }}</p>
                        </a>
                    </div>
                    <div class="col-lg-1 col-xs-2 col-mini-4   mt20 heart-profile   text-center">
                        <span>
                            <i class="fa fa-heart" 
                                 data-value="{{ $agentSave->agent->id }}" 
                                 url =  "{{ URL::to('/agent/saved/remove/'.$agentSave->agent->uid) }}"
                                 style="font-size:30px;" aria-hidden="true" 
                                 onclick="removeAgent(this);"></i>
                        </span>

                    </div>
                     <div class="col-lg-2  col-xs-4 col-mini-4   col-lg-offset-0  mt25  ml15"> <span class="message-profile-btn">Message</span> </div>
                    <div class="col-lg-1  col-xs-4 col-mini-4     col-lg-offset-0 mt20   "> 
                      <a href="{{ $agentSave->agent->user->profile->getProfileUrl() }}" 
                         type="button" class="btn btn-primary btn-md  apply-profile-btn" style="align-item:ceter;">View Profile</a>
                    </div>
                </div>
                <!--end of listt-->
        @endforeach
      @else
      No Result
      <br/>
      <a class="btn btn-info" href="{{ URL::previous() }}">Back</a>
      @endif

        <div class="row">
          <div class="col-md-6 col-md-offset-3">
             {{  $agentSaves->appends($q)->links(); }}
          </div>
        </div>
</div>
    
    
</div>
</div>

@stop
@section('footerscript')

    <script type="text/javascript">

        function removeAgent(e){
            
            var link = $(e);
            var url = link.attr('url');

            $.post(url).done(function(data) { 
                if(!data.error){
                    link.closest(".agent-list").remove();
                }
            });
            
        }
            $(function(){



            });
    </script>
@append

