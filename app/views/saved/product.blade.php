@extends('layouts.default')
@section('contenthead')
@stop
@section('content')

<div class="content-section-a">
<div class="container-fluid">
    
    
    <div class="col-lg-8 col-xs-12  mb10 ">
     @if($productSaves != null && count($productSaves) > 0)
      @foreach ($productSaves as $i => $productSave) 
                <div class="col-lg-12 col-mini-6 col-md-6 col-xs-12  list-profile-member  product-list  ">

                    <div class="col-lg-2 col-xs-12 col-md-10 col-md-offset-1  col-md-10 col-mini-8 col-mini-offset-1  col-lg-offset-0 mb10">
                        <img src="{{ $productSave->product->getImageUrl() }}" width="117" height="117">
                    </div>
                    <div class="col-lg-5  col-sm-8 col-xs-12 col-md-10 col-md-offset-1  col-sm-offset-2 col-lg-offset-0  agent-list-profile">
                        
                        <a href="#">
                            <h4>{{ $productSave->product->name }}</h4>
                        </a>
                        <a href="profile.html">
                             <a href=""><p>All Finance Agents</p></a>
                         </a>
                         <a href="profile.html">
                            <p></p>
                        </a>
                        
                    </div>
                    <div class="col-lg-1 col-xs-2 col-mini-4   mt20 heart-profile   text-center">
                        <span>
                            <i class="fa fa-heart" 
                                 data-value="{{ $productSave->product->uid }}" 
                                 url =  "{{ URL::to('/product/saved/remove/'.$productSave->product->uid) }}"
                                 style="font-size:30px;" aria-hidden="true" 
                                 onclick="removeProduct(this);"></i>
                        </span>

                    </div>
                    <div class="col-lg-2  col-xs-4 col-mini-4  col-lg-offset-0  mt25  ml15 agent-list-message">
        
                    </div>
                    <div class="col-lg-1  col-xs-4 col-mini-4  col-lg-offset-0 mt20   ">
    
                         <a href="{{ URL::to('/product?sub='. $productSave->product->uid . '&primary=' . $productSave->productcategory->uid ) }}"><button type="button" class="btn btn-primary btn-md  apply-profile-btn" style="align-item:ceter;">Apply Now</button></a>
                    </div>
                </div>
                <!--end of listt-->
        @endforeach
      @else
      No Result
      <br/>
      <a class="btn btn-info" href="{{ URL::previous() }}">Back</a>
      @endif

        <div class="row">
          <div class="col-md-6 col-md-offset-3">
             {{  $productSaves->appends($q)->links(); }}
          </div>
        </div>
</div>
    
    
</div>
</div>

@stop
@section('footerscript')

    <script type="text/javascript">

        function removeProduct(e){
            
            var link = $(e);
            var url = link.attr('url');

            $.post(url).done(function(data) { 
                if(!data.error){
                    link.closest(".product-list").remove();
                }
            });
            
        }
            $(function(){



            });
    </script>
@append

