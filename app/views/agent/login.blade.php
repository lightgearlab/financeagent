@extends('layouts.default')

@section('contenthead')


    
@stop


@section('content')

<h1> Agent login </h1>

 {{ Form::open(array('role' => 'form' , 'url' => '/login' , 'method' => 'post')) }}
	  <div class="form-group">
	    <label for="txtEmail">Email</label>
	    <input id="txtEmail" name="txtEmail"  type="email" class="form-control" placeholder="mail@abc.com">
	  </div>
	  <div class="form-group">
	    <label for="txtPassword">Password</label>
	    <input id="txtPassword" name="txtPassword" type="password" class="form-control"  placeholder="Password">
	  </div>
	  
	  <button type="submit" class="btn btn-default">Sign In</button>
  {{Form::close()}}

@stop
