@extends('layouts.default')

@section('contenthead')

@stop

@section('content')

 <div class="header-top set-header-profile">
  @if(count($errors) > 0)
      <div class="alert alert-danger" style="text-align:left;">
        <strong>Error:</strong>
          <ul>
           @foreach ($errors->all() as $message)
          
            <li>{{ $message }}</li>
           @endforeach
          </ul>
      </div>
  @endif
 </div>

<div class="content-section-a-no-bottom">
    <div class="container">
        <div class="col-lg-5 col-xs-12 col-mini-8 col-mini-offset-1 col-extra-offset-3 login-themes">


            <div class="tab-content clearfix">
                <div class="mt20">
                    <p class="p-thin-medium ml10">Welcome, join us for more benefits!</p>
                </div>

                    <div class="container-fluid mt10">
                       {{ Form::open(array('id' => 'formSignUp','role' => 'form' , 'url' => '/agent/register' , 'method' => 'post')) }}

                        <div class="col-lg-12">
                         <div class="row">
                                    <div class="form-group">
                                        <label class="control-label p-thin-medium" for="name">Name</label>
                                        <div class="input-group">
                                             <input id="txtName" name="txtName"  type="text" class="form-control email-input" placeholder="Your name" required>
                                            <span class="input-group-addon"><i class="fa fa-check form-check" style="font-size:20px;" aria-hidden="true"></i></span>
                                        </div>

                             </div>
                                    <div class="form-group">
                                        <label class="control-label p-thin-medium" for="name">Email</label>
                                        <div class="input-group">
                                             <input id="txtEmail" name="txtEmail"  type="email" class="form-control email-input" placeholder="Your Email" required>
                                            <span class="input-group-addon"><i class="fa fa-check form-check" style="font-size:20px;" aria-hidden="true"></i></span>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="control-label p-thin-medium" for="name">Password</label>
                                        <div class="input-group">
                                              <input id="txtPassword" name="txtPassword" type="password" class="form-control email-input password-input"  placeholder="Password" required>
                                            <span class="input-group-addon show-password"><i class="fa fa-eye form-check-pass" style="font-size:20px;" aria-hidden="true"></i></span>
                                        </div>
                                    </div>
                             <!--  <div class="form-group">

   <label for="ddlBank">Bank</label>

   {{ Form::select('ddlBank', ['' => 'Select'] + $banks , null,['class' => 'form-control'])}}

   </div>

   <div class="form-group">

   <label for="product">Products</label>   

    @foreach ($products as $i => $product)

   <label class="form-control" >

   {{ Form::checkbox( 'products[]', 

   

                $product->id,

   

                in_array($products[$i]->id, array()),

   

                ['class' => '', 'id' => $product->id] 

   

                ) 

   

          }}

   

   {{ $product->name }}

   </label>

   @endforeach

   </div>

   

   -->

                           

                            </div>
                           <div class="row login-button" >

                                    <button type="submit" class="btn btn-block btn-login">
                                        <h3>Sign Up</h3>
                                    </button>

                            </div>
                             {{Form::close()}}
                            </div>
                        </div>

                </div>

            </div>
        </div>
    </div>

@stop



@section('footerscript')



<script type="text/javascript">

  
     $('#formSignUp').validate({ // initialize the plugin

        rules: {
            txtName: {

                required: true

            },
            txtEmail: {

                required: true

            },

            txtPassword: {

                required: true

            }

        },

        messages: {
            txtName: {

                required: '* Name is required.'

            },
            txtEmail: {

               required: '* Email is required.'

            },

            txtPassword: {

               required: '* Password is required.'

            }

         }

    });



</script>

   

@append

