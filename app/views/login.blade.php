@extends('layouts.default')

@section('contenthead')

@stop

@section('content')

 <div class="header-top set-header-profile">
  @if(count($errors) > 0)
      <div class="alert alert-danger" style="text-align:left;">
        <strong>Error:</strong>
          <ul>
           @foreach ($errors->all() as $message)
          
            <li>{{ $message }}</li>
           @endforeach
          </ul>
      </div>
  @endif
 </div>

        <div class="content-section-a-no-bottom">
            <div class="container">
                <div class="col-lg-5 col-xs-12 col-mini-8 col-mini-offset-1 col-extra-offset-3 login-themes">
                    
                    <div class="tab-content clearfix">
                        <div class="mt20">
                            <p class="p-thin-medium ml10">Welcome, join us for more benefits!</p>
                        </div>
                        <div class="tab-pane active" id="1a">
                            <div class="container-fluid mt10">
                                <div class="col-lg-12">
                                {{ Form::open(array('id' => 'formLogin' ,'role' => 'form' , 'url' => '/login' , 'method' => 'post')) }}

                                    <div class="row">
                                
                                        <div class="form-group">
                                                <label class="control-label p-thin-medium" for="email">Email</label>
                                                <div class="input-group">
                                                    <input id="txtEmail" name="txtEmail"  type="email" class="form-control email-input" placeholder="Your Email" required>
                                                    <span class="input-group-addon"><i class="fa fa-check form-check" style="font-size:20px;" aria-hidden="true"></i></span>
                                                </div>

                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label p-thin-medium" for="name">Password</label>
                                                <div class="input-group">
                                                     <input id="txtPassword" name="txtPassword" type="password" class="form-control email-input password-input"  placeholder="Password" required>
                                                     <span class="input-group-addon show-password" data-value="show"><i class="fa fa-eye form-check-pass" style="font-size:20px;" aria-hidden="true"></i></span>
                                                </div>
                                            </div>

                                            <div class="checkbox mt30">
                                                <label class="p-thin-medium">
                                                    <input id="remember" name="remember" type="checkbox" class="mt40" value="true">Remember me</label>
                                                <p class="pull-right p-thin-medium">
                                                    <a href="#">Forget Password</a></p>
                                            </div>
                          
                                    </div>
                                   <div class="row login-button" >
                                            <button type="submit" class="btn btn-block btn-login">
                                                <h3>Login</h3>
                                            </button>
        
                                    </div>

                                    {{Form::close()}}
                                </div>
                            </div>

                        </div>

                        
                        </div>

                    </div>
                </div>
            </div>

@stop



@section('footerscript')



<script type="text/javascript">

  

   $('#formLogin').validate({ // initialize the plugin

        rules: {

            txtEmail: {

                required: true

            },

            txtPassword: {

                required: true

            }

        },

        messages: {

            txtEmail: {

               required: '* Email is required.'

            },

            txtPassword: {

               required: '* Password is required.'

            }

         }

    });
    
    
     



</script>

   

@append

