@extends('layouts.default')



@section('contenthead')



   

        

@stop





@section('content')

 <section class="result-page">

            <div class="container">

				 <div id="divResult" class="row">



				@if($reserves != null && count($reserves) > 0)

				  @foreach ($reserves as $i => $reserve)



				  <div class="reserve col-sm-4 agent-list {{  ($i + 1) % 3 != 0 ? 'marginright' : '' }}">

				        <div class="col-sm-3">

				           <img src="{{ $reserve->agent->user->profile->getAvatar() }}" width="100">

				        </div>

				        <div class="col-sm-9 text-left">

				           <h6>

				            {{ link_to_route('profile.show', $reserve->agent->user->profile->name ,array($reserve->agent->user->profile->id), array('class' => '')) }}</h6>

				           @if($reserve->agent->user->profile->rating_cache != null)

				           <p class="agent-rating"> 

				              {{ $reserve->agent->user->profile->rating_cache }} / 5  

				           </p>

				             @endif


				           	<p>{{ $reserve->agent->user->profile->biography }}<br>


				            <strong>{{ $reserve->agent->bank->name }}</strong>

				              {{ $reserve->agent->user->getUserLocation() }}

				           </p>

							@if(Auth::check())

							<a class="btn btn-info pull-right removeagent" href="#" data-value="{{ $reserve->id }}" ><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span></a>



							@endif



				        </div>

				  </div>

				  @endforeach

				@else

				  No Result

				  <br/>

				  <a class="btn btn-info" href="{{ URL::previous() }}">Back</a>

				@endif

				</div>

				<div class="row">

				 <div class="col-md-6 col-md-offset-3">

				      {{  $reserves->links(); }}

				</div>

				  

				</div>

          </div>

         </section>

@stop





@section('footerscript')

	<script type="text/javascript">



		 $(".removeagent").click(function(e){

          

          e.preventDefault();

           var btnRemove = $(this);



           var agentId = btnRemove.data("value");       



            $.ajax({

                type: "POST",

                url : '<?=URL::to('/agent/remove')?>' + '/' + agentId,

                success : function(data){

                    

                  

                  if(!data.error){



                    $(btnRemove).hide();

                    $(btnRemove).closest(".reserve").remove();

                    $('.reserved').text(data.result);



                  }else{



                  }



                 

                },

                error: function(xhr, textStatus, errorThrown){

                   console.log(xhr.responseText);

                }

            });

        });



	</script>

@stop