@extends('layouts.default')



@section('contenthead')



@stop





@section('content')

	<section class="profile-page">

		<div class="container">


			@include('shared._calculator')

		</div>

		<div class="container">
			<div id="divRecommendList" data-url="<?=URL::to('/product/recommend')?>"></div>
		</div>
	</section>


@stop





@section('footerscript')

	<script type="text/javascript">

		function getRecommendProduct(products){
			var url = $("#divRecommendList").data("url");

			$.post(url,products).done(function( data ) {
			   	$("#divRecommendList").empty();
			   	$("#divRecommendList").html(data.result);
			});
		}

		$(document).on( "populateRecommendList", function(event,data) {

			getRecommendProduct(data);
		});

		$(function(){

			getRecommendProduct(null);
		});
	</script>

@append