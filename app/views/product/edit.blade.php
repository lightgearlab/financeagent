{{ Form::model($product, array('id' => 'formProduct','method' => 'PATCH',  'route' => array('product.update', $product->id), 'files' => true)) }}

@include("product._edit",array('title' => 'Edit Product','product' => $product));

{{Form::close()}}