

@if(count($products) > 0)

<div class="col-md-12">

<h5>

{{ link_to_route('product.show', $products[0]->name  ,array($products[0]->id), array('class' => '')) }}</h5>

<div class="row">

   <div class="col-md-3 paddingzero">



      <a href="{{ URL::route('product.show', array('id' => $products[0]->id)) }}" class="">

      <img src="{{ $products[0]->getImageUrl() }}" class="img-rounded" alt="...">

      </a>

   </div>

   <div class="col-md-9">

      <p class="description">{{ $products[0]->description }}</p>

   </div>

</div>

</div>

@endif





@if(count($products) > 1)

<div class="col-md-12">

   <h5>Other Products</h5>

   <div class="row">



      @for($i = 1; $i < count($products); $i++) 

         <div class="col-xs-6 col-md-3">

            <a href="{{ URL::route('product.show', array('id' => $products[$i]->id)) }}" class="thumbnail">

            <img src="{{ $products[$i]->getImageUrl() }}" alt="...">

            </a>

         </div>

      @endfor

   </div>

</div>

@endif





@if(Auth::check() && Auth::user()->isAgent() && Auth::user()->isLoginUser())

<div class="col-md-12">

<div class="row">

         @include('product.create',array('agent' => $agent, 'productcategory' => $productcategory))

</div>

</div>   

 @endif 



