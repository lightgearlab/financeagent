

   <div class="panel panel-default">

      <div class="panel-heading">

         <h3 class="panel-title">{{ $title }}</h3>

      </div>

      <div class="panel-body">

         <div class="col-xs-6 col-md-12 product1">

            <div class="col-md-4 paddingzero">

               <a href="#" class="thumbnail">

               <img  id="productImage" src="{{ $product->getImageUrl() }} " alt="...">

               </a>

            </div>

            <div class="col-md-8">

               <span class="file-input btn btn-upload btn-file">

                  

                  Upload Photo <input id="fileProductImage"  name="productimage" type="file" multiple>

               </span>

            </div>

            <input id="txtProductName" name="productname" type="text" class="form-control" placeholder="Product Name" value="{{ $product->name }}" required>

            <br>

            {{ Form::select('categories', ['' => 'Select'] + $productcategory , ( $product->productcategory != null ? $product->productcategory->id : ''),['class' => 'form-control','required' => 'required']) }}

            

            <br>

            <textarea name="description" class="form-control" rows="3" placeholder="Product Descriptions" rows="4" cols="20" style="text-align:left;" required>{{ $product->description }}</textarea>

            <br>

             <div class="checkbox">

             <label>

              {{ Form::checkbox('primary', 1, $product->primary) }} Is Primary

             </label>

           </div>

            <div class="col-xs-6 col-md-12 paddingzero crud-buttons">

              @if($product->id != null)

               <button id="btnRemove" data-value="{{ $product->id }}" type="button" class="btn btn-remove">Remove</button> 

              @endif

               <button type="submit" class="btn btn-success pull-right">Save</button>

            </div>

         </div>

      </div>

   </div>



 <div id="dialog-confirm" title="Are you sure?" style="display:none;">

  <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This item will be permanently deleted and cannot be recovered. Are you sure?</p>

</div>



<script type="text/javascript">

// IIFE to prevent globals

(function() {



  var s;

  var Avatar = {



    settings: {

      bod: $("body"),

      img: $("#productImage"),

      fileInput: $("#fileProductImage")

    },



    init: function() {

      s = Avatar.settings;

      Avatar.bindUIActions();

    },



    bindUIActions: function() {



      var timer;



      s.bod.on("dragover", function(event) {

        clearTimeout(timer);

        if (event.currentTarget == s.bod[0]) {

          Avatar.showDroppableArea();

        }



        // Required for drop to work

        return false;

      });



      s.bod.on('dragleave', function(event) {

        if (event.currentTarget == s.bod[0]) {

          // Flicker protection

          timer = setTimeout(function() {

            Avatar.hideDroppableArea();

          }, 200);

        }

      });



      s.bod.on('drop', function(event) {

        // Or else the browser will open the file

        event.preventDefault();



        Avatar.handleDrop(event.dataTransfer.files);

      });



      s.fileInput.on('change', function(event) {

        Avatar.handleDrop(event.target.files);

      });

    },



    showDroppableArea: function() {

      s.bod.addClass("droppable");

    },



    hideDroppableArea: function() {

      s.bod.removeClass("droppable");

    },



    handleDrop: function(files) {



      Avatar.hideDroppableArea();



      // Multiple files can be dropped. Lets only deal with the "first" one.

      var file = files[0];



      if (typeof file !== 'undefined' && file.type.match('image.*')) {



        Avatar.resizeImage(file, 256, function(data) {

          Avatar.placeImage(data);

        });



      } else {



        alert("That file wasn't an image.");



      }



    },



    resizeImage: function(file, size, callback) {



      var fileTracker = new FileReader;

      fileTracker.onload = function() {

        Resample(

         this.result,

         size,

         size,

         callback

       );

      }

      fileTracker.readAsDataURL(file);



      fileTracker.onabort = function() {

        alert("The upload was aborted.");

      }

      fileTracker.onerror = function() {

        alert("An error occured while reading the file.");

      }



    },



    placeImage: function(data) {

      s.img.attr("src", data);

    }



  }



  Avatar.init();



})();







    $('#btnRemove').click(function(e){

      e.preventDefault();

      var btn = $(this);

      var id = $(this).data("value");

      var url = '<?=URL::to('/product')?>' + '/' + id;







      $( "#dialog-confirm" ).dialog({

          resizable: false,

          height: 200,

          width: 500,

          modal: true,

          buttons: {

            "Confirm": function() {

              

              $('input[name=_method]').val("DELETE");

              $('#formProduct').attr('action', url).submit();

            },

            Cancel: function() {

              $( this ).dialog( "close" );

            }

          }

        });

   

    });







</script>