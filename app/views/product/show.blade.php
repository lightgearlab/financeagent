@extends('layouts.default')



@section('contenthead')



@stop





@section('content')
 

 		{{ Breadcrumbs::render() }}


        <div class="content-section-a">
        <div class="container-fluid">

        	<!-- product image summary start -->
            <div class="col-md-3 col-xs-12  col-mini-5 mt10 ">
                <div class="container-fluid" style="background-image: url({{ $product->getImageUrl() }});background-size:cover;display:block;  background-repeat: no-repeat;min-height:250px;">
                
                </div>

            </div>
			<div class="col-md-4 col-xs-12 col-md-4 col-mini-7 mt10">
				<div class="container-fluid bg-white" style="min-height:250px;">
					<h4 class="mt30">{{ $product->name }}</h4>

					<p class="font-thin">Free group Takaful insurance coverange includes guaranty repayment of outstanding debts accured through credit card-i usage</p>
					<ul class="font-thin">
					<li>13.5 % p.a profit rate</li>
					<li>RM 0.00 annual fee</li>
					<li>20 days penalty-free period</li>
					<li>Earn up to 1x reward points</li>
					</ul>
				</div>
			</div>
<!-- product image summary end -->
<!-- pre scoring start -->
<div class="col-md-4 col-mini-12 col-xs-12 pull-right mt10">
                <div class="col-md-12 bg-white step-title pd10">
                    <div class="container-fluid pd0 step-head">
                        <h4 >Pre Scoring</h4>
                    </div>
                </div>

                <div class="col-md-12  col-xs-12 col-mini-12    bg-white pt20">
                    <div class="row">
                        
                        
                        
                        <form>
            <div class="col-md-12 form-group">
              <label for="text">Product</label>
              <select class="form-control">
  <option>Credit Card</option>
  <option>Health Insurance</option>
  <option>Personal Financing</option>
  <option>Micro Financing</option>
  <option>Fixed Deposit</option>
  <option>Wealth Management</option>
</select>
            </div>
            <div class=" col-md-12 col-xs-12 form-group">
              <label for="text">Monthly Income (RM)</label>
              <input type="text" class="form-control" id="text">
            </div>
            <div class=" col-md-6  col-xs-6 form-group">
              <label for="text">Loan tenure (Years)</label>
              <select class="form-control">
  <option>1 year</option>
  <option>2 years</option>
  <option>3 years</option>
  <option>4 years</option>
  <option>5 years</option>
  <option>6 years</option>
  <option>7 years</option>

  <option>8 years</option>
  <option>9 years</option>

</select>
            </div>
            
            
            <div class="col-md-12 daus-slider">
                              
    <div class="budget">
        <div class="header">
            <div class="title clearfix">Loan Period<span class="pull-right"></span></div>
        </div>
        <div class="content">
            <input type="range" min="1" max="100" value="20" data-rangeslider>
        </div>
        
    </div>

                            </div>
            
            <div class=" col-md-6 col-xs-6  form-group">
              <label for="text">Interest Rate (% pa)</label>
              <select class="form-control">
  <option>Credit Card</option>
  <option>Health Insurance</option>
  <option>Personal Financing</option>
  <option>Micro Financing</option>
  <option>Fixed Deposit</option>
  <option>Wealth Management</option>
</select>
            </div>
            
            <div class=" col-md-6 col-xs-6  form-group">
              <label for="text">house financing if any</label>
              <input type="text" class="form-control" id="text">
            </div>
            
            <div class=" col-md-6 col-xs-6  form-group">
              <label for="text">vehicle financing if any</label>
              <input type="text" class="form-control" id="text">
            </div>
            <div class=" col-md-6 col-xs-6  form-group">
              <label for="text">personal financing if any</label>
              <input type="text" class="form-control" id="text">
            </div>
            
            <div class=" col-md-6 col-xs-6  form-group">
              <label for="text">credit card minimum payment if any</label>
              <input type="text" class="form-control" id="text">
            </div>
            
            <div class=" col-md-6 col-xs-6  form-group">
              <label for="text">Other commitment</label>
              <input type="text" class="form-control" id="text">
            </div>
            

          <div class="col-md-12 text-center pd5">
            <button type="button" class="btn btn-default btn-block pd10"><b>Calculate & Save</b></button>
          </div>

        
        <div class="col-md-12 calculator-result">
        <h5><strong>Monthly Repayment (RM):</strong>	</h5> <h5>999.00 / Month</h5>
        
        <h5><strong>Maximum Loan (RM):</strong>	</h5> <h5>999.00 / Month</h5>
        
            
            </div>
        
        

          </form>
                        
                    </div>
                    
                    
                </div>
            </div>

            <!-- pre-scoring end -->

			<!-- product detail start -->
            <div class="col-md-7  col-mini-12 col-xs-12 mt20 ">
                <div class="container-fluid bg-white">
                    <div class="login-themes">
                        <div class="row tab-section-product">
                            <div class="col-md-3 col-xs-12 col-mini-5  tab-login ">
                                <a href="#1a" data-toggle="tab" class="active"><h4>Fee & charges</h4></a>
                            </div>
                            <div class="col-md-3  col-xs-12 col-mini-4  tab-login">
                                <a href="#2a" data-toggle="tab" class=""><h4>Requirements</h4></a>
                            </div>
                            <div class="col-md-3  col-xs-12 col-mini-4  tab-login">
                                <a href="#3a" data-toggle="tab" class=""><h4>Reviews</h4></a>
                            </div>
                        </div>
                        <div class="tab-content clearfix">
                            <div class="tab-pane active" id="1a">
                                <div class="container-fluid mt40 font-thin">
                                    <div class="row  ">
                                        <div class="col-md-5 col-mini-5">
                                            <p> <b>Annual</b></p>
                                        </div>
                                        <div class="col-md-7">
                                            <p>RM 0 Free for life</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5 col-mini-5">
                                            <p><b>Annual Fee for Supplementary Card</b></p>
                                        </div>
                                        <div class="col-md-7">
                                            <p>RM 0</p>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-md-5 col-mini-5">
                                            <p><b>late Payment Fee</b> </p>
                                        </div>
                                        <div class="col-md-7">
                                            <p>RM 10 or 1% of outstanding amount, up to maximun of RM 100</p>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-md-5 col-mini-5">
                                            <p><b>Cash Withdraw fee</b></p>
                                        </div>
                                        <div class="col-md-7">
                                            <p>RM 21.20 or 1% of withdraw amount, whetever is higher</p>
                                        </div>
                                    </div>

                                    <div class="row ">
                                        <div class="col-md-5 col-mini-5">
                                            <p><b>Profit Rate on Cash Withdrawls</b> </p>
                                        </div>
                                        <div class="col-md-7">
                                            <p>18 %</p>
                                        </div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-md-5 col-mini-5">
                                            <p><b>Daily Maximum Cash Withrawl</b></p>
                                        </div>
                                        <div class="col-md-7">
                                            <p>RM 5000</p>
                                        </div>
                                    </div>
                                    <div class="row ">

                                        <div class=" pull-right mt50 col-md-7 col-xs-12 pb20 col-mini-offset-6">
                                            <a href="{{ URL::to('/search/agent?primary=' . $MainProductId . '&sub=' . $SubProductId )}}">
                                              <button type="button" class="btn btn-primary btn-block apply-btn">APPLY</button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="2a" style="min-height:350px;">
                                <div class="container-fluid mt10   ">
                                    <!--if there is no data-->
                                    <div class=" col-md-12 col-xs-12  ">
                                       <!-- <center><img src="img/no_data.png" class="img-responsive"></center>-->
                                          <p class="mt20 font-thin">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap in</p>
                                           <p class="font-thin">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap in</p>
                                            <p class="font-thin">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap in</p>

                                    </div>
                                    <!--end of no data-->
                                </div>
                            </div>

                            <div class="tab-pane" id="3a" style="min-height:350px;">
                                <div class="container-fluid mt10   ">
                                     <div class=" col-md-12 col-xs-12  ">
                                       <!-- <center><img src="img/no_data.png" class="img-responsive"></center>-->
                                          <p class="mt20 font-thin"> <b>dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</b> since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap in</p>
                                           <p class="font-thin">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap in</p>
                                            <p class="font-thin">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap in</p>
                                            
                                    </div>
                                  <!--if there is no data-->
                                    <!--<div class=" col-md-4 col-xs-4 col-xs-offset-4 col-md-offset-4 text-center  mt60">
                                        <center><img src="img/no_data.png" class="img-responsive"></center>
                                          <p class="mt20">oops there is no data in this page</p>
                                    </div> -->
                                    <!--end of no data-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.container -->
            </div>
<!-- 
            product detail end -->
         </div>
		</div>


@stop





@section('footerscript')



@stop