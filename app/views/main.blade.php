@extends("layouts.default")
@section('contenthead')
@stop
@section('content')

<style>

.landing-product-name {background: #000;
    width: 264px;
    height: 263px;
    opacity: 0.5;
    position: absolute;
       top: 0px;
    left: 14px;}
.landing-product-name h4{position: absolute; z-index:99px;
    top: 101px; color:#FFF;
    left: 77px;}
	
	.hero-unit ul.tags-categories { margin-left: -38px;}
	
	.thumbnail {
    padding: 0;
	      box-shadow: 0 2px 0px rgba(0, 0, 0, 0.01),0 0 0 1px #DBDBDC;
    -webkit-box-shadow: 0 2px 0px rgba(0, 0, 0, 0.01),0 0 0 1px #DBDBDC;
    -moz-box-shadow: 0 2px 0px rgba(0, 0, 0, 0.01),0 0 0 1px #DBDBDC;
	border:0;
	}
</style>


         <section class="header-10-sub v-center">
   <div class="background">
      &nbsp;
   </div>
               <div>

               <div class="container">
                  <div class="hero-unit">
                     <h1>Financial Products in One Place </h1>
                    <!-- <p>
                        Discover finance solutions around you, get service in an instant
                     </p>-->



         <form class="form-inline search-form" action="<?=URL::to('/search')?>" type="GET">
            <div class="form-group">
               <input id="txtKeyword" name="keyword" type="text" class="form-control main-search"  placeholder="Search products or locations">
            </div>
            <input type="submit" class="btn btn-search" value="Search"></input>
         </form>
         <ul class="tags-categories">
            <li><a href="<?=URL::to('/search?service=')?>">All Products</a></li>
            <li><a href="<?=URL::to('/search?service=Insurance')?>">Insurance</a></li>
            <li><a href="<?=URL::to('/search?service=Credit+Card')?>">Credit Card</a> </li>
            <li><a href="<?=URL::to('/search?service=Hire+Purchase')?>">Car Loans</a></li>
            <li> <a href="<?=URL::to('/search?service=Mortgage')?>">House Loans</a></li>
         </ul>
      </div>
   </div>
   </div>
</section>
<!-- content-8  -->
<section class="content-8 v-center">
   <div>
      <div class="container">
                  <div class="row">
                     <h3>Top Products</h3>
                     <div class="col-xs-6 col-md-3">
                        <a href="<?=URL::to('/search?service=Mortgage')?>" class="landing-product-name"><h4>Home Loans</h4></a>
                        <div class="product-img-bg"></div>
                        <a href="<?=URL::to('/search?service=')?>" class="thumbnail img-responsive">
                        <img src="images/houseloans.jpg" width="350">
                        </a>
                     </div>
                     <div class="col-xs-6 col-md-3">
                        <a href="<?=URL::to('/search?service=Hire+Purchase')?>" class="landing-product-name"><h4>Car Loans</h4></a>
                        <div class="product-img-bg"></div>
                        <a href="<?=URL::to('/search?service=')?>" class="thumbnail img-responsive">
                        <img src="images/carloans.jpg" width="350">
                        </a>
                     </div>
                     <div class="col-xs-6 col-md-3">
                        <a href="<?=URL::to('/search?service=')?>" class="landing-product-name"><h4>Small<br />
Business Loan</h4></a>
                        <div class="product-img-bg"></div>
                        <a href="<?=URL::to('/search?service=')?>" class="thumbnail img-responsive">
                        <img src="images/businessloans.jpg" width="350">
                        </a>
                     </div>
                     <div class="col-xs-6 col-md-3">
                        <a href="<?=URL::to('/search?service=Insurance')?>" class="landing-product-name"><h4>Medical<br />
Insurance</h4></a>
                        <div class="product-img-bg"></div>
                        <a href="<?=URL::to('/search?service=')?>" class="thumbnail img-responsive">
                        <img src="images/medicalinsurance.jpg" width="350">
                        </a>
                     </div>
                     <div class="col-xs-6 col-md-3">
                        <a href="<?=URL::to('/search?service=Insurance')?>" class="landing-product-name"><h4>Car Insurance</h4></a>
                        <div class="product-img-bg"></div>
                        <a href="<?=URL::to('/search?service=')?>" class="thumbnail img-responsive">
                        <img src="images/carinsurance.jpg" width="350">
                        </a>
                     </div>
                     <div class="col-xs-6 col-md-3">
                        <a href="<?=URL::to('/search?service=')?>" class="landing-product-name"><h4>Fixed Deposits</h4></a>
                        <div class="product-img-bg"></div>
                        <a href="<?=URL::to('/search?service=')?>" class="thumbnail img-responsive">
                        <img src="images/fixeddeposit.jpg" width="350">
                        </a>
                     </div>
                     <div class="col-xs-6 col-md-3">
                        <a href="<?=URL::to('/search?service=Credit+Card')?>" class="landing-product-name"><h4>Credit Card</h4></a>
                        <div class="product-img-bg"></div>
                        <a href="<?=URL::to('/search?service=')?>" class="thumbnail img-responsive">
                        <img src="images/creditcard.jpg" width="350">
                        </a>
                     </div>
                     <div class="col-xs-6 col-md-3">
                        <a href="<?=URL::to('/search?service=')?>" class="landing-product-name"><h4>Share Trading</h4></a>
                        <div class="product-img-bg"></div>
                        <a href="<?=URL::to('/search?service=')?>" class="thumbnail img-responsive">
                        <img src="images/sharetrading.jpg" width="350">
                        </a>
                     </div>
                  </div>
                  <div class="row">
                     <h3>Favourites Locations</h3>
                     <div class="col-xs-6 col-md-3">
                        <a href="<?=URL::to('/search?service=')?>" class="landing-product-name"><h4>Petaling Jaya</h4></a>
                        <div class="product-img-bg"></div>
                        <a href="<?=URL::to('/search?service=')?>" class="thumbnail img-responsive">
                        <img src="images/pj.png" width="350">
                        </a>
                     </div>
                     <div class="col-xs-6 col-md-3">
                        <a href="<?=URL::to('/search?service=')?>" class="landing-product-name"><h4>Kuala Lumpur</h4></a>
                        <div class="product-img-bg"></div>
                        <a href="<?=URL::to('/search?service=')?>" class="thumbnail img-responsive">
                        <img src="images/kl.png" width="350">
                        </a>
                     </div>
                     <div class="col-xs-6 col-md-3">
                        <a href="<?=URL::to('/search?service=')?>" class="landing-product-name"><h4>Cyberjaya</h4></a>
                        <div class="product-img-bg"></div>
                        <a href="<?=URL::to('/search?service=')?>" class="thumbnail img-responsive">
                        <img src="images/cyberjaya.png" width="350">
                        </a>
                     </div>
                     <div class="col-xs-6 col-md-3">
                        <a href="<?=URL::to('/search?service=')?>" class="landing-product-name"><h4>Selangor</h4></a>
                        <div class="product-img-bg"></div>
                        <a href="<?=URL::to('/search?service=')?>" class="thumbnail img-responsive">
                        <img src="images/selangor.png" width="350">
                        </a>
                     </div>
                  </div>
               </div>
   </div>
</section>
@stop