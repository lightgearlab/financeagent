@extends("layouts.default")
@section('contenthead')
@stop
@section('content')


    <!-- Header -->
    <div class="intro-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="intro-message">
                    <h1>Financial Product Made Easy</h1>
                </div>
            </div>
            <div class="col-md-6  col-md-6 col-xs-10 col-sm-6  col-xs-offset-1 col-md-offset-3 col-sm-offset-3 form-search">
                <form action="profile.html">
                    <div class="input-group">

                        <input type="text" class="form-control main-landing-search" placeholder="Search ">
                        <span class="input-group-btn">
                          <a href="{{ URL::to('/search/product') }}" class="btn btn-default" >&nbsp<i class="fa fa-search search-size" aria-hidden="true"></i> </a>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <!-- Page Content -->
    <div class="content-section-a-no-bottom">
        <div class="container-fluid">
            <div class="row">
                
                @foreach ($mainProducts as $product)

                <div class="col-md-3 col-sm-6 col-mini-6  mt20">
                    <!--expereimnet-->
                    <div class="blue-hover">
                        <div class="carding">
                            <div class="image">
                                <a href="#"><img src="{{ $product->getImageUrl() }}" class="img-responsive" alt="Cerulean"></a>
                            </div>
                            <div class="subject-country">
                                <h4>{{ $product->name }}</h4>
                                <p class="sub-subject">
                                    <a href="{{ URL::to('/search/product?scope=subproduct&primary='.$product->uid) }}">
                                        Discover More
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach           
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.content-section-a -->

    <div class="content-section-c">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-mini-6 text-center mt20 discover-agent-home">
                   <a href="product-result.html">
                        <div class="blue-hover">
                            <div class="city-list ">
                                <h4>SELANGOR</h3>
                                <p class="sub-subject">Discover Agents</p>
                            </div>
                             <div class="all-prod">
                                <h4>Discover Products</h4>
                            </div>
                        </div>
                    </a>  
                </div>
                <!--end of country-list-->
                <div class="col-md-3 col-sm-6 col-mini-6 text-center mt20 discover-agent-home">
                   <a href="product-result.html">
                        <div class="blue-hover">
                            <div class="city-list ">
                                <h4>SELANGOR</h3>
                                <p class="sub-subject">Discover Agents</p>
                            </div>
                             <div class="all-prod">
                                <h4>Discover Products</h4>
                            </div>
                        </div>
                    </a>  
                </div>
                <!--end of country-list-->
                <div class="col-md-3 col-sm-6 col-mini-6 text-center mt20 discover-agent-home ">
                    <a href="product-result.html">
                        <div class="blue-hover">
                            <div class="city-list ">
                                <h4>SELANGOR</h3>
                                <p class="sub-subject">Discover Agents</p>
                            </div>
                             <div class="all-prod">
                                <h4>Discover Products</h4>
                            </div>
                        </div>
                    </a>  
                </div>
                <!--end of country-list-->
                <div class="col-md-3 col-sm-6 col-mini-6 text-center mt20 discover-agent-home">
                   <a href="product-result.html">
                        <div class="blue-hover">
                            <div class="city-list ">
                                <h4>SELANGOR</h3>
                                <p class="sub-subject">Discover Agents</p>
                            </div>
                             <div class="all-prod">
                                <h4>Discover Products</h4>
                            </div>
                        </div>
                    </a>  
                </div>
                <!--end of country-list-->
            </div>
        </div>
        <!-- /.container -->
    </div>
    <!-- /.content-section-c -->


    
    @include('layouts.footer')

    @stop