<?php


class General {

	public static function distance($lat1, $lon1, $lat2, $lon2, $unit) {

	  $theta = $lon1 - $lon2;
	  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	  $dist = acos($dist);
	  $dist = rad2deg($dist);
	  $miles = $dist * 60 * 1.1515;
	  $unit = strtoupper($unit);

	  if ($unit == "K") {
	    return ($miles * 1.609344);
	  } else if ($unit == "N") {
	      return ($miles * 0.8684);
	    } else {
	        return $miles;
	      }
	}

	public static function GetDrivingDistance($lat1, $long1,$lat2, $long2)
	{
			try{

		    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving";
		    $ch = curl_init();
		    curl_setopt($ch, CURLOPT_URL, $url);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		    $response = curl_exec($ch);
		    curl_close($ch);
		    $response_a = json_decode($response, true);

		   // dd($response_a);

		    $status = $response_a['rows'][0]['elements'][0]['status'];

		    if($status == 'ZERO_RESULTS' || $status == 'NOT_FOUND')
		    {
		    	return '';
		    }
		    else{
		    	$dist = $response_a['rows'][0]['elements'][0]['distance']['text'];
			    $time = $response_a['rows'][0]['elements'][0]['duration']['text'];

			    //return array('distance' => $dist, 'time' => $time);
		    	return $dist;
		    }
		}catch(Exception $e){

				Log::info("[Get Driving Distance Error] : " . $e->getMessage());

				return '';
		}
	    
	}


	public static function SendWelcomeEmail($user,$name){
		
		try
		{
			$email = $user->email;

			Mail::send('emails.welcome', array('name' => $name,'email' => $email), function($message) use ($email,$name){
	        	
	        	$message->to($email, $name)->subject('Welcome to the Financeagent.co !');
	    	
	    	});

	    	if(count(Mail::failures()) > 0 ) {

				Log::info("[Send Email Error] : Failed");
			}else {

			}

    	}catch(Exception $e){

			Log::info("[Send Email Error] : " . $e->getMessage());
		}

	}
	public static function SendEmail($templateName,$userMessage){
	
		try{

			Mail::send($templateName, array('userMessage' => $userMessage), function($message) use ($userMessage){

				$message->from($userMessage->sender->email, $userMessage->sender->profile->getDisplayName());
				$message->to($userMessage->receiver->email, $userMessage->receiver->profile->getDisplayName())->subject($userMessage->subject);

				
			});

			if(count(Mail::failures()) > 0 ) {

				Log::info("[Send Email Error] : Failed");
			}else {

				$userMessage->send = true;

				$userMessage->save();
			}
		}catch(Exception $e){

			Log::info("[Send Email Error] : " . $e->getMessage());
		}	


	}

}	

?>