<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {


	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public static $rules = array();
		/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the token value for the "remember me" session.
	 *
	 * @return string
	 */
	public function getRememberToken()
	{
		return $this->remember_token;
	}

	/**
	 * Set the token value for the "remember me" session.
	 *
	 * @param  string  $value
	 * @return void
	 */
	public function setRememberToken($value)
	{
		$this->remember_token = $value;
	}

	/**
	 * Get the column name for the "remember me" token.
	 *
	 * @return string
	 */
	public function getRememberTokenName()
	{
		return 'remember_token';
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	// many to many
	public function roles() {
        return $this->belongsToMany('Role');
    }


  	// one to many
	public function accounts()
    {
        return $this->hasMany('Account');
    }

    public function profile()
    {
        return $this->hasOne('Profile');
    }

    public function agent()
    {
        return $this->hasOne('Agent');
    }
    
    // one to many
	public function applicant()
    {
        return $this->hasMany('Applicant');
    }


//  	// one to one
//	public function location()
//    {
//        return $this->hasOne('Location');
//    }
    
 
    

    // one to many
	public function reviews()
    {
        return $this->hasMany('Review');
    }

    // one to many
    public function reserves()
    {
        return $this->hasMany('Reserve');
    }

      // one to many
    public function agentsaved()
    {
        return $this->hasMany('AgentSaved');
    }

      // one to many
    public function productsaved()
    {
        return $this->hasMany('ProductSaved');
    }


    public function isAdmin()
	 {
		// add your logic here to determine if user is an admin

	 	$blnAdmin = false;

		foreach($this->roles as $role){

			if($role->name === 'Administrator'){
				$blnAdmin = true;
				break;
			}
				
		}

		return $blnAdmin;
	 }


	 public function isAgent()
	 {
		// add your logic here to determine if user is an admin

	 	$blnAgent = false;

		foreach($this->roles as $role){

			if($role->name === 'Agent'){
				$blnAgent = true;
				break;
			}
				
		}

		return $blnAgent;
	 }

	 public function isLoginUser(){

    	
    	if(Auth::check() && $this->id == Auth::user()->id)
    		return true;
    	else
    		return false;
    }

    public function hasEmail(){
    	if($this->email != null && $this->email != ''){
    		return true;
    	}
    	else
    		return false;
    }

    public function hasReview(){

    	if($this->reviews() != null && count($this->reviews()) > 0)
    		return true;
    	else
    		return false;
    }

    public function hasLocation(){

//    	if($this->location != null &&  $this->location->lng != null && $this->location->lat != null && $this->location->lat != 0 && $this->location->lng != 0 )
//    		return true;
//    	else
//    		return false;
        
        return $this->profile->location != null;

    }

    public function getAgentSavedCount(){

    	if($this->agentsaved != null)
    		return count($this->agentsaved);
    	else 
    		return 0;
    }

    public function getProductSavedCount(){

    	if($this->productsaved != null)
    		return count($this->productsaved);
    	else 
    		return 0;
    }

    public function getReservedCount(){

    	if($this->reserves != null)
    		return count($this->reserves);
    	else 
    		return 0;
    }


    public function getUserLocation(){

    	if($this->profile->location != null){
    		return $this->profile->location->city;
    	}
    	else{
    		return '';
    	}
    }

 

	public function scopeDistance($query, $lat, $lng, $radius = 100, $unit = "km")
	{

	    $unit = ($unit === "km") ? 6378.10 : 3963.17;
	    $lat = (float) $lat;
	    $lng = (float) $lng;
	    $radius = (double) $radius;

	    return $query->join('locations','users.id','=','locations.user_id')
	    			->having('distance','<=',$radius)
	                ->select(DB::raw("users.*,locations.lat,locations.lng,
	                            ($unit * ACOS(COS(RADIANS($lat))
	                                * COS(RADIANS(lat))
	                                * COS(RADIANS($lng) - RADIANS(lng))
	                                + SIN(RADIANS($lat))
	                                * SIN(RADIANS(lat)))) AS distance")
	                )->where('current',1);


	                
	}


	public function storeUser($email,$password = null){

        if($this->id == null){
            $this->uid = Str::random(40);
        }
        if($email != null)
		  $this->email = $email;

		if($password != null)
			$this->password = $password;
        
          if($this->isDirty())
		  $this->save();
	}


	public function updateLoginInfo($token){
		$this->logincounter = $this->logincounter + 1;
		$this->logindate = new DateTime();
		//$this->setRememberToken($token);
		$this->save();
	}

	public function updateLogoutInfo($token){
	
		$this->logoutdate = new DateTime();
		$this->save();
	}


	 public function conversations()
    {
        return $this->belongsToMany('Conversation', 'conversation_user')->withTimestamps()->orderBy('updated_at', 'desc');
    }
    
	public function messageStates()
    {
        return $this->hasMany('MessageState')->orderBy('updated_at', 'desc');
    }
    
    public function unreadMessageStates(){
    	return $this->messageStates()->where('state', '=', 0)->orderBy('updated_at', 'desc')->get();
    }
    public function unreadMessagesCount(){
        return count($this->unreadMessageStates());
    }
    public function hasUnreadMessages(){
        return $this->unreadMessagesCount() > 0;
    }
    
    public function unreadConversations(){
    	//TODO: Do this using Eloquent
    	$unreadConversations = array();
    	foreach($this->unreadMessages() as $unreadMessage){
    		$unreadConversation = $unreadMessage->conversation;
    		$conversationId = $unreadConversation->id;
    		 
    		if(isset($unreadConversations[$conversationId])) continue;
    		 
    		$unreadConversations[$conversationId] = $unreadConversation;
    	}
      	return $unreadConversations;    	 
    }
    public function unreadMessages(){ 	
    	return $this->findMessages('unread');
    }
    
    public function findMessages($state = false){
    	$user_id = $this->id;
    	 
    	$unreadMessages = Message::whereHas('messageStates', function($q) use( &$user_id, &$state)
    	{
    		$q->where('user_id', '=', $user_id);
    		
    		if($state)
    		$q->where('state', '=', MessageState::indexOf($state));
    		 
    	})->with('conversation')->get();
    	 
    	return $unreadMessages;
    }

}
