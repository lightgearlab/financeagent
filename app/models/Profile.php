<?php

class Profile extends Eloquent {
	
	protected $table = 'profiles';
	protected $guarded = array();

	public static $rules = array();


	public function user()
    {
        return $this->belongsTo('User');
    }

    // one to many
	public function reviews()
    {
        return $this->hasMany('Review')->orderBy('created_at','desc');
    }
    
       
    public function location()
    {
        return $this->belongsTo('Location','location_id','id');
    }

    public function isUserProfile($user = null){
        
        if($user != null)
            return $user->id == $this->user->id;
        else
            return Auth::user()->id == $this->user->id;
    }
    
    public function hasReview(){

    	if($this->reviews != null && count($this->reviews))
    		return true;
    	else
    		return false;
    }

    public function recalculateRating()
	{
	    $reviews = $this->reviews();
	    $avgRating = $reviews->avg('rating');
	    $this->rating_cache = round($avgRating,1);
	    $this->rating_count = $reviews->count();
	    $this->save();
	
	}

	public function getRating(){

		if($this->rating_cache != null || $this->rating_cache != 0){
			return $this->rating_cache;
		}
		else
		{
			return 3.5;
		}
	}

    public function getAvatar()
	 {
		// add your logic here to determine if user is an admin

	 	$photoUrl = asset("images/users/default.png");

		if(isset($this->photo_url)){
			if($this->photointernal == 1)
				$photoUrl = asset($this->photo_url);
			else
				$photoUrl = $this->photo_url;
		}

		return $photoUrl;
	 }

	public function getDisplayName(){

	 	 if($this->first_name != null 
	 	 	&& $this->first_name != ''
	 	 	&& $this->last_name != null
	 	 	&& $this->last_name != ''
	 	 	)
	 	 	return $this->first_name . " " . $this->last_name;
	 	 else{
	 	 	if($this->name != null && $this->name != '')
	 	 		return $this->name;
	 	 	else
	 	 		return 'Anonymous';
	 	 }
	 }


	 public function getDistance(){

      if(Auth::check() && Auth::user()->hasLocation()){

      	$strDistance = General::GetDrivingDistance($this->user->location->lat, $this->user->location->lng,Auth::user()->location->lat, Auth::user()->location->lng);
		
		if($strDistance != '')
			return  $strDistance .' from you';
		else
			return '';
		//return round(General::distance( $this->user->location->lat, $this->user->location->lng,Auth::user()->location->lat, Auth::user()->location->lng, "K"),2)   ;
      }else{
         return '';
      }
    }

    public function getProfileUrl(){

		$url = route('profile.show', $this->uid);

		return $url;
	}


	public function storeProfile($userId,
								 $name = null,
								 $first_name = null,
								 $last_name = null,
								 $phonenumber = null,
								 $age = null,
								 $gender = null,
								 $photo_url = "images/users/default.png",
								 $biography = null,
								 $photointernal = 1,
								 $rating_cache = 3.5,
								 $rating_count = null,
                                 $locationId = null)
	{
		$user = User::find($userId);

		$this->user_id = $userId;
        
        if($this->id == null){
            $this->uid = Str::random(20);
        }

		if($name != null)
			$this->name = $name;

		if($first_name != null)
			$this->first_name = $first_name;

		if($last_name != null)
			$this->last_name = $last_name;

		if($phonenumber != null)
			$this->phonenumber = $phonenumber;

		if($age != null)
			$this->age = $age;

		if($gender != null)
			$this->gender = $gender;

		if($photo_url != null)
			$this->photo_url = $photo_url;

		if($biography != null)
			$this->biography = $biography;

		if($photointernal != null)
			$this->photointernal = $photointernal;

		if($rating_cache != null)
			$this->rating_cache = $rating_cache;
		
		if($rating_count != null)
			$this->rating_count = $rating_count;
		
		if($this->id == null)
			$this->user()->associate($user);
        
        if($locationId != null)
        {
            $location = Location::find($locationId);
            $this->location()->associate($location);
        }
        
        if($this->isDirty())
		  $this->save();
	}


}
