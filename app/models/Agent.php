<?php

class Agent extends Eloquent {
	protected $table = 'agents';
	protected $guarded = array();

	public static $rules = array();


	public function user()
    {
        return $this->belongsTo('User');
    }

    // 1 to many
	public function products() {
        return $this->hasMany('Product');
    }

    public function bank()
    {
        return $this->belongsTo('Bank','bank_id','id');
    }

    // one to many
    public function agentsaved()
    {
        return $this->hasMany('AgentSaved');
    }

     // one to many
    public function agentproduct()
    {
        return $this->hasMany('AgentProduct');
    }
    
    // one to many
    public function applicationproduct()
    {
        return $this->hasMany('ApplicationProduct');
    }
    
    public function isAgent($user){
        
        return $this->user->id == $user->id;
    }

    public function hasBank(){

        if($this->bank !=null)
            return true;
        else
            return false;
    }

    public function hasSaved(){

      if(Auth::check()){
        $saved = $this->agentsaved()->where('user_id','=',Auth::user()->id)->get();

        if($saved != null && count($saved) > 0)
            return true;
        else
            return false;
      }else{

        return false;
      }

    }


    public function hasServices(){

        if($this->products != null)
          return true;
        else
          return false;
    }



    public function hasMainProduct(){
      return Product::MainProduct($this->id)->first() != null;
    }

    public function hasOtherProduct(){
      return Product::OtherProduct($this->id)->first() != null;
    }

    public function getMainProduct(){

        return Product::MainProduct($this->id)->first();
    }

    public function getOtherProduct(){

        return Product::OtherProduct($this->id)->get();
    }

     public function getServices(){

        $listService = array();

        
        foreach($this->products as $product)
        {
             if(!in_array($product->ProductCategory->name,  $listService, true)){
                array_push( $listService, $product->ProductCategory->name);
            }   
        }

        return $listService;
    }


     public function getPrimaryProduct(){

         return $this->AgentPrimaryProduct()->get();
    }

    

    public function getReservedCount(){
      
        if($this->reserves != null)
          return count($this->reserves);
        else
          return 0;
    }

    public function storeAgent($userId,$bankId = null){

        $user = User::find($userId);

        if($this->id == null)
        {
            $this->user_id = $userId;

            $this->user()->associate($user);
        }

        if($bankId != null)
          $this->bank_id = $bankId;

        $this->save();

    }


    public function scopeServiceName($query,$name){

        return $query->whereIn('agents.id',function($query) use($name){

            $query->select(DB::raw('products.agent_id'))
                  ->from('products')
                  ->join('productcategory','productcategory.id','=','products.productcategory_id')
                  ->where('productcategory.name','=',$name);

        });
    }

    public function scopeBankName($query,$name){

         return $query->join('banks','banks.id','=','agents.bank_id')
               ->select(DB::raw('agents.*'))
               ->where('banks.name','=',$name);  
    

    }


    public function scopeLocationName($query,$name){
        return $query->join('locations','locations.user_id','=','agents.user_id')
                     ->select(DB::raw('agents.*'))
                     ->where('locations.address','=','$name');
    }

    public function scopeUserRating($query,$rating){

        if($rating == 'highest')
        {
         return $query
                ->join('users','users.id','=','agents.user_id')
                ->join('profiles','profiles.user_id','=','users.id')
                ->select(DB::raw('agents.*'))
                ->where('profiles.rating_cache','>=',3.5);
        }
        else{

         return $query
                ->join('users','users.id','=','agents.user_id')
                ->join('profiles','profiles.user_id','=','users.id')
                ->select(DB::raw('agents.*'))
                ->where('profiles.rating_cache','<',3.5);
        }
    

    }

    public function scopeUserKeyword($query,$keyword){

        return $query
               ->join('locations','locations.user_id','=','agents.user_id')
               ->join('banks','banks.id','=','agents.bank_id')
               ->select(DB::raw('agents.*'))
               ->where('banks.name','like','%' . $keyword. '%')
               ->orWhere('locations.address','like','%' . $keyword. '%')
               ->orwhereIn('agents.id',function($query) use($keyword){

                    $query->select(DB::raw('products.agent_id'))
                          ->from('products')
                          ->join('productcategory','productcategory.id','=','products.productcategory_id')
                          ->where('productcategory.name','like','%' . $keyword. '%');

                });

    }


    public function scopeAgentPrimaryProduct($query){

        return $query->select(DB::table('pc1'))
            ->join('agent_product as ap1', 'ap1.agent_id', '=', 'agents.id')
            ->join('productcategory as pc1', 'ap1.productcategory_id', '=', 'pc1.id')
            ->join('products as p1','p1.id','=','ap1.product_id')
            ->distinct()->select('pc1.*')
           // ->select('pc1.*')
            ->where('ap1.agent_id','=',$this->id);
            
  }

    public function scopePrimaryProduct($query,$primaryProductUId){

        return $query->select(DB::table('agents'))
            ->join('agent_product as ap1', 'ap1.agent_id', '=', 'agents.id')
            ->join('productcategory as pc1', 'ap1.productcategory_id', '=', 'pc1.id')
            ->join('products as p1','p1.id','=','ap1.product_id')
            ->distinct()
            ->select('agents.*')
            ->where('pc1.uid','=',$primaryProductUId);
            
  }


    public function scopeSubProduct($query,$subProductUId){

        return $query->select(DB::table('agents'))
            ->join('agent_product', 'agent_product.agent_id', '=', 'agents.id')
            ->join('productcategory', 'agent_product.productcategory_id', '=', 'productcategory.id')
            ->join('products','products.id','=','agent_product.product_id')
            ->distinct()
            ->select('agents.*')
            ->where('products.uid','=',$subProductUId);
            
  }
}
