<?php

class UserScore extends Eloquent {
	protected $table = 'userscore';
	protected $guarded = array();

	public static $rules = array();


	public function saveUserScore($args){
        
        if($args['productCategoryId'] != null)
		  $this->productcategory_id = $args['productCategoryId'];
        
        if($args['monthlyincome'] != null)
		  $this->monthlyincome = $args['monthlyincome'];
        
        if($args['loantenure'] != null)
		  $this->loantenure = $args['loantenure'];
        
		if($args['interestrateId'] != null)
		  $this->interestrate_id = $args['interestrateId'];
        
		if($args['totalhousefinance'] != null)
		  $this->totalhousefinance = $args['totalhousefinance'];
             
		if($args['totalvehiclefinance'] != null)
		  $this->totalvehiclefinance = $args['totalvehiclefinance'];
        
        if($args['totalpersonalfinance'] != null)
		  $this->totalpersonalfinance = $args['totalpersonalfinance'];
        
        if($args['totalcreditcardfinance'] != null)
		  $this->totalcreditcardfinance = $args['totalcreditcardfinance'];
        
        if($args['monthlyrepayment'] != null)
		  $this->monthlyrepayment = $args['monthlyrepayment'];
        
        if($args['maximumloan'] != null)
		  $this->maximumloan = $args['maximumloan'];
        
        if($args['maximummonthlyrepayment'] != null)
		  $this->maximummonthlyrepayment = $args['maximummonthlyrepayment'];

        if($args['user'] != null)
		  $this->user = $args['user'];
        
        if($args['status'] != null)
		  $this->status = $args['status'];

		if($this->isDirty())
		  $this->save();	
	}
    
    
    public function application(){
        return $this->hasOne('Application');
    }
    
    public function interestrate(){
        return $this->hasOne('InterestRate','id','interestrate_id');
    }
    
     public function mainproduct(){
        return $this->hasOne('ProductCategory','id','productcategory_id');
    }
    
    
    public function hasMainProduct(){
        return $this->mainproduct != null;
    }
    
    public function hasInterestRate(){
        return $this->interestrate != null;
    }
}
