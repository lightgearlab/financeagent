<?php

class Review extends Eloquent {
	protected $table = 'reviews';
 	protected $fillable = ['profile_id', 'user_id', 'rating','comment'];

    public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    public function profile()
    {
        return $this->belongsTo('Profile', 'profile_id');
    }

	public function storeReviewForProfile($profileId, $comment, $rating)
	{
	  $profile = Profile::find($profileId);
	  
	  $this->user_id = Auth::user()->id;
	  $this->comment = $comment;
	  $this->rating = $rating;
	  $profile->reviews()->save($this);

	  // recalculate ratings for the specified product
	  $profile->recalculateRating();
	}


	public function removeReview($profileId){

		$profile = Profile::find($profileId);

		if($this->profile->id == $profile->id){

				$this->delete();
				$profile->recalculateRating();

				return true;
		}else
		{
			return false;
		}
	}

	
}