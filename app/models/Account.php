<?php

class Account extends Eloquent {
	protected $table = 'accounts';

	protected $guarded = array();

	public static $rules = array();


	//Relationship many to 1
	public function user() {
        return $this->belongsTo('User');
    }

    public function storeAccount($userId,
    							$uid = null,
    							$username = null,
    							$provider = null,
    							$access_token = null,
    							$access_token_secret = null,
    							$connect = null)
    {
    		$user = User::find($userId);

    		$this->user_id = $user->id;

			if($provider != null)
				$this->provider = $provider;

			if($access_token != null)
			$this->access_token = $access_token;
			
			if($connect != null)
				$this->connect = true;

			if($this->id == null)
				$this->user()->associate($user);
	 		
	 		$this->save();
    }
}
