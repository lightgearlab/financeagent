<?php

class Product extends Eloquent {
	protected $table = 'products';
	protected $guarded = array();

	public static $rules = array();

	protected $fillable = ['productcategory_id', 'agent_id', 'name','description','photo_url'];

	//Relationship many to 1
	public function agent() {
        return $this->belongsTo('Agent');
    }

   	public function productcategory(){
			
		return $this->belongsTo('ProductCategory','productcategory_id','id');
	}

    // one to many
    public function agentproduct()
    {
        return $this->hasMany('AgentProduct');
    }
    
    // one to many
    public function applicationproduct()
    {
        return $this->hasMany('ApplicationProduct');
    }

	public function isProductOwner(){

		$user = Auth::user();

		if($user->isAgent()){

			if($user->agent->id == $this->agent->id){
				return true;
			}else{
				return false;
			}
		}else
		{
			return false;
		}
	}

	public function getProductDetailUrl(){

		$url =  URL::to("/product/" . $this->uid ) ;

		return $url;
	}

	public function getImageUrl(){

		if($this->photo_url != null && $this->photo_url != '')
			return asset($this->photo_url);
		else
			return "http://placehold.it/171x180";
	}


	public function productsaved()
    {
        return $this->hasMany('ProductSaved');
    }

	public function hasSaved(){

      if(Auth::check()){

        $saved = $this->productsaved()->where('user_id','=',Auth::user()->id)->get();

        if($saved != null && count($saved) > 0)
            return true;
        else
            return false;
      }else{

        return false;
      }

    }


	public function scopeMainProduct($query,$agentId){

		return $query->select(DB::raw('products.*'))
					 ->where('primary',true)
					 ->where('agent_id',$agentId);
	}

	public function scopeOtherProduct($query,$agentId){

		return $query->select(DB::raw('products.*'))
					 ->where('primary',false)
					 ->where('agent_id',$agentId);
	}


	public function scopeSubProduct($query,$primaryProductId){
		    
		  return $query->select(DB::table('products'))
            ->join('agent_product', 'products.id', '=', 'agent_product.product_id')
            ->join('productcategory', 'agent_product.productcategory_id', '=', 'productcategory.id')
            ->select('products.*')
            ->where('productcategory.uid','=',$primaryProductId)
            ->distinct();
            
	}


	public function updatePrimaryProduct($agentId){

		$product1 = Product::where('primary',1)->where('agent_id',$agentId)->first();

		if($product1 != null){

			$product1->primary = false;
			$product1->save();
		}
	}


	public function storeAgentProduct($agentId, $name = null, $description = null,$categoryId = null,$IsPrimary,$file = null)
	{
	  $agent = Agent::find($agentId);    
	  
	  if($this->id != null)
	  	$this->agent_id = $agent->id;

	  if($name != null)
	  	$this->name = $name;
	  
	  if($description != null)
	  	$this->description = $description;

	  if($categoryId != null)
	  	$this->productcategory_id = $categoryId;

	  if($IsPrimary && !$this->primary){
	  	$this->primary = $IsPrimary;
	  	$this->updatePrimaryProduct($agentId);	
	  }else
	  {
	  	$this->primary = $IsPrimary;
	  }
	  
	  if($file != null){
		  $newName = strval(uniqid()) . "." . $file->getClientOriginalExtension();

	      $file->move('images/products', $newName);

	      $this->photo_url = "images/products/" . $newName;
  	  }

	  $agent->products()->save($this);
	}

	public function scopeProductKeyword($query,$keyword){

        return $query
               ->join('agents','agents.id','=','products.agent_id')
               ->join('locations','locations.user_id','=','agents.user_id')
               ->join('banks','banks.id','=','agents.bank_id')
               ->select(DB::raw('products.*'))
               ->where('banks.name','like','%' . $keyword. '%')
               ->orWhere('locations.address','like','%' . $keyword. '%')
               ->orWhere('products.name','like','%' . $keyword. '%')
               ->orWhere('products.description','like','%' . $keyword. '%');

    }
}
