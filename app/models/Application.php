<?php

class Application extends Eloquent {
	protected $table = 'applications';
	protected $guarded = array();

	public static $rules = array();


	public function saveApplication($applicantId = null,
                                    $statusName = null){
        
        if($this->id == null){
            $this->code =  Str::random(30);
        }
        
        if($applicantId != null){
            
            $applicant = Applicant::find($applicantId);
            $this->applicant()->associate($applicant);
        }
        
        if($statusName != null){
            $appStatus = ApplicationStatus::whereName($statusName)->first();
            $this->applicationstatus()->associate($appStatus);
        }
        
        
        if($this->isDirty())
		  $this->save();
	}
    
    public function submitApplication($IsExist = false){
        
        $appStatus = ApplicationStatus::whereName('Pending')->first();
        
        $this->applicationstatus()->associate($appStatus);
        $this->isExistingCustomer = $IsExist;
        
         if($this->isDirty())
		  $this->save();
    }
    
    public function saveStatus($statusName){
        
        $appStatus = ApplicationStatus::whereName($statusName)->first();
        
        if($appStatus != null)
        {
            $this->applicationstatus()->associate($appStatus)->save();
            return true;
        }else
        {
            return false;
        }
    }
    
    public function scopeAgentAvailableProduct($query){

          return AgentProduct::whereNotIn( DB::raw('(agent_product.productcategory_id,agent_product.product_id)'),function ($query) {
                $query    
                    ->join('applications','application_product.application_id','=','applications.id')
                    //->join('agents','agents.id','=','applications.agent_id')       
                    ->select('application_product.productcategory_id','application_product.product_id')
                    ->from('application_product')
                    ->where('applications.code','=',$this->code);
            });
              
        //dd(DB::getQueryLog());
    

    }
    
    public static function getLastPendingApplication($userId,
                                              $mainProductUId = null,
                                              $subProductUId = null){
        $query = DB::table('applications')->whereApplicationStatusId(5)
        ->join('applicants','applicants.id','=','applications.applicant_id')->whereUserId($userId)
        ->join('application_product','application_product.application_id','=','applications.id')
        ->join('productcategory','application_product.productcategory_id','=','productcategory.id')
        ->join('products','application_product.product_id','=','products.id')        
        ->select('applications.*');
        
        if($subProductUId != null)
            $query = $query->where('products.uid','=',$subProductUId);
        
        if($mainProductUId != null)
            $query = $query->where('productcategory.uid','=',$mainProductUId);
        
        return $query->first();
    }
    
    public function getApplicant(){
        
        $applicant = null;
        
        if($this->applicant == null){
   
            $user = User::find(Auth::user()->id);

            $applicant = new Applicant;

            $applicant->name = $user->profile->getDisplayName();
            $applicant->phone_number = $user->profile->phonenumber;
            $applicant->email = $user->email;		
            $applicant->location_id = $user->profile->location->id;
        }else{
            $applicant = $this->applicant;       
        }
        
        return $applicant;
        
    }
    
    
    public function getUserScore(){
        
        $score = null;
        
        if($this->userscore == null){
   
            $score = new UserScore;
        }else{
            $score = $this->userscore;       
        }
        
        return $score;
        
    }
    
    public function isApplicant($user = null){
        if($this->applicant != null && $this->applicant->user != null)
            return $this->applicant->user->id == ( $user != null ? $user->id : Auth::user()->id) ;
        else
            return false;
    }

    
    
    public function hasUserScore(){
        return $this->userscore != null;
    }
    
    
//     public function agent(){
//        return $this->belongsTo('Agent');
//    }
    
    
     public function applicationstatus(){
        return $this->belongsTo('ApplicationStatus','application_status_id','id');
    }
    
    
    public function applicant(){
        return $this->belongsTo('Applicant');
    }
    
    public function userscore(){
        return $this->belongsTo('UserScore');
    }
    
    // one to many
    public function applicationdocument(){
        return $this->hasMany('ApplicationDocument');
    }
    
      // one to many
    public function applicationproduct()
    {
        return $this->hasMany('ApplicationProduct');
    }
}
