<?php

class ProductCategory extends Eloquent {
	protected $table = 'productcategory';
	protected $guarded = array();

	public static $rules = array();

  	public function product() 
    {
        return $this->hasOne('Product');
    }

   // one to many
    public function agentproduct()
    {
        return $this->hasMany('AgentProduct','productcategory_id');
    }
    
    public function userscore(){
        return $this->belongsTo('UserScore','userscore_id');
    }
    
     // one to many
    public function applicationproduct()
    {
        return $this->hasMany('ApplicationProduct');
    }

    public function productsaved()
    {
        return $this->hasMany('ProductSaved');
    }

    public function getStatus(){

    	if($this->active)
    		return "Active";
    	else
    		return "Not Active";
    }

    public function getImageUrl(){

        if($this->photo_url != null && $this->photo_url != '')
            return asset($this->photo_url);
        else
            return "http://placehold.it/171x180";
    }

  
    public function scopeProductCategoryKeyword($query,$keyword){

        return $query
               ->join('products','products.productcategory_id','=','productcategory.id')
               ->join('agents','agents.id','=','products.agent_id')
               ->join('locations','locations.user_id','=','agents.user_id')
               ->join('banks','banks.id','=','agents.bank_id')
               ->select(DB::raw('productcategory.*'))
               ->where('banks.name','like','%' . $keyword. '%')
               ->orWhere('locations.address','like','%' . $keyword. '%')
               ->orWhere('products.name','like','%' . $keyword. '%')
               ->orWhere('products.description','like','%' . $keyword. '%');

    }
}
