<?php

class Role extends Eloquent {
	protected $table = 'roles';
	protected $guarded = array();

	public static $rules = array();

	public function users() {
        return $this->belongsToMany('User');
    }
}
