<?php

class ApplicationDocument extends Eloquent {
    protected $table = 'application_document';
	protected $guarded = array();

	public static $rules = array();
    
    public function document()
    {
        return $this->belongsTo('Document', 'document_id');
    }
    
    public function application()
    {
        return $this->belongsTo('Application', 'application_id');
    }
    
    public function saveApplicationDocument($applicationCode,$documentId){
        
        $application = Application::whereCode($applicationCode)->first();
        $document = Document::find($documentId);
        
        if($application != null && $document != null){
            
            $this->application()->associate($application);
            $this->document()->associate($document);
            
            $this->save();
        }
    }
}
