<?php

class ApplicationProduct extends Eloquent {
    protected $table = 'application_product';
	protected $guarded = array();

	public static $rules = array();
    
    public function mainproduct()
    {
        return $this->belongsTo('ProductCategory', 'productcategory_id');
    }
    
    public function subproduct()
    {
        return $this->belongsTo('Product', 'product_id');
    }
    
    public function application()
    {
        return $this->belongsTo('Application', 'application_id');
    }
    
    public function agent(){
        return $this->belongsTo('Agent');
    }
    
    public function saveApplicationProduct(
        $applicationId = null,
        $productCategoryUId = null,
        $productUId = null,
        $agentUId = null){

        if($applicationId != null)
        {
            $application = Application::find($applicationId);
            // $this->application->associate($application);
            
            $this->application()->associate($application);
        }   
        
        if($productCategoryUId != null){
            $productCategory = ProductCategory::whereUid($productCategoryUId)->first();
            
            $this->mainproduct()->associate($productCategory);
     
            //$this->productcategory_id = $productCategory->id ;
        }
        
        if($productUId != null){
            
            $product = Product::whereUid($productUId)->first();
            $this->subproduct()->associate($product);    
            //$this->product_id = $product->id ;
        }
        
        if($agentUId != null){
            $agent = Agent::whereUid($agentUId)->first();
            $this->agent()->associate($agent);
        }
                
        if($this->isDirty())
		  $this->save();

    }
    
    public function displayOption($index){
        
        if($index == 1) return '1st Option';
        else if($index == 2) return '2nd Option';
        else if($index == 3) return '3rd Option';
        else 
            return $index . ' th Option';
    }
}
