<?php

class Reserve extends Eloquent {
	protected $table = 'reserve';
	protected $guarded = array();

	public static $rules = array();


	public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    public function agent()
    {
        return $this->belongsTo('Agent', 'agent_id');
    }

    public function storeReserve($agentId,$type){

    	$this->user_id = Auth::user()->id;
    	$this->agent_id = $agentId;
      $this->type = $type;
    	$this->save();
    }

    public function removeReserve(){

      if($this->user->id == Auth::user()->id)
      {
        $this->delete();

        return true;
      }else{

        return false;
      }
    }


    public function scopeUserId($query,$userId){

       return $query
              ->join('agents','agents.id','=','reserve.agent_id')
              ->join('users','users.id','=','reserve.user_id')
              ->select(DB::raw('reserve.*'))
              ->where('reserve.user_id','=',$userId);
    }

}
