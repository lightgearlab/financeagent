<?php

class Document extends Eloquent {
    protected $table = 'documents';
	protected $guarded = array();

	public static $rules = array();
    
    // one to many
    public function applicationdocument()
    {
        return $this->hasMany('ApplicationDocument');
    }
    
    public function getDownloadUrl(){
        
        $url = '';
        
        if($this->download_url != null)
        {
            $url = asset($this->download_url);     
        }
        
        return $url;
    }
    
    public function saveDocument($fileName = null,$fileType = null,$downloadUrl = null){  
        
        if($fileName != null)
            $this->filename = $fileName;
        
        if($fileType != null)
            $this->filetype = $fileType;
        
        if($downloadUrl != null)
            $this->download_url = $downloadUrl;  
        
        if($this->isDirty())
		  $this->save();
    }
}
