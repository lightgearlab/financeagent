<?php

class UserMessage extends Eloquent {

	protected $table = 'usermessages';
	protected $guarded = array();

	public static $rules = array();

	public function sender()
    {
        return $this->belongsTo('User', 'sender_id','id');
    }

    public function receiver()
    {
        return $this->belongsTo('User', 'receiver_id','id');
    }
}
