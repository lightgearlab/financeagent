<?php

class AgentSaved extends Eloquent {
	protected $table = 'agent_saved';
	protected $guarded = array();

	public static $rules = array();


	public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }

    public function agent()
    {
        return $this->belongsTo('Agent', 'agent_id');
    }

    public function storeReserve($agentUid){
        
        $user = User::find(Auth::user()->id);
        $agent = Agent::whereUid($agentUid)->first();
        
        if($agent != null){
            $this->user()->associate($user);
            $this->agent()->associate($agent);
            $this->save();
            return true;
        }else{
            return false;
        }
    }

    public function removeReserve(){

      if($this->user->id == Auth::user()->id)
      {
        $this->delete();

        return true;
      }else{

        return false;
      }
    }

    public function scopeAgentSaved($query,$agentId){
         return $query
              ->join('agents','agents.id','=','agent_saved.agent_id')
              ->join('users','users.id','=','agent_saved.user_id')
              ->select(DB::raw('agent_saved.*'))
              ->where('agent_saved.user_id','=', Auth::user()->id)
              ->where('agent_saved.agent_id','=', $agentId);   
    }
    
    public function scopeUserId($query,$userId){

       return $query
              ->join('agents','agents.id','=','agent_saved.agent_id')
              ->join('users','users.id','=','agent_saved.user_id')
              ->select(DB::raw('agent_saved.*'))
              ->where('agent_saved.user_id','=',$userId);
    }
}
