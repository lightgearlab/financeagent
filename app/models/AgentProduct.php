<?php

class AgentProduct extends Eloquent {
	protected $table = 'agent_product';
	protected $guarded = array();

	public static $rules = array();


	public function productcategory()
    {
        return $this->belongsTo('ProductCategory');
    }

	public function product()
    {
        return $this->belongsTo('Product', 'product_id');
    }

	public function agent()
    {
        return $this->belongsTo('Agent', 'agent_id');
    }
}
