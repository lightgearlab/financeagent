<?php

class ProductSaved extends Eloquent {
	protected $table = 'product_saved';
	protected $guarded = array();

	public static $rules = array();


	public function user()
    {
        return $this->belongsTo('User', 'user_id');
    }
    
    public function productcategory()
    {
        return $this->belongsTo('ProductCategory', 'productcategory_id','id');
    }

    public function product()
    {
        return $this->belongsTo('Product', 'product_id');
    }

    public function storeReserve($productCategoryUid,$productUid){
        
        $user = User::find(Auth::user()->id);
        $product = Product::whereUid($productUid)->first();
        $productCategory = ProductCategory::whereUid($productCategoryUid)->first();
        
        if($product != null){
            $this->user()->associate($user);
            $this->product()->associate($product);
            $this->productcategory()->associate($productCategory);
            $this->save();
            return true;
        }else{
            return false;
        }
    }

    public function removeReserve(){

      if($this->user->id == Auth::user()->id)
      {
        $this->delete();

        return true;
      }else{

        return false;
      }
    }

    public function scopeProductSaved($query,$productId){
         return $query
              ->join('products','products.id','=','product_saved.product_id')
              ->join('users','users.id','=','product_saved.user_id')
              ->select(DB::raw('product_saved.*'))
              ->where('product_saved.user_id','=', Auth::user()->id)
              ->where('product_saved.product_id','=', $productId);   
    }

    public function scopeUserId($query,$userId){

       return $query
              ->join('products','products.id','=','product_saved.product_id')
              ->join('users','users.id','=','product_saved.user_id')
              ->select(DB::raw('product_saved.*'))
              ->where('product_saved.user_id','=',$userId);
    }
}
