<?php

class Applicant extends Eloquent {
	protected $table = 'applicants';
	protected $guarded = array();

	public static $rules = array();


	public function saveApplicant($userId,
                                  $optional = null){

		
       
        if($optional['name'] != null)
		  $this->name = $optional['name'];
		
        if($optional['phoneNumber'] != null)
            $this->phone_number = $optional['phoneNumber'];
		
        if($optional['email'] != null)
            $this->email = $optional['email'];
		
        if($optional['ICNumber'] != null)
            $this->ic_number = $optional['ICNumber'];
		
        if($optional['locationId'] != null){
            $location = Location::find($optional['locationId']);
            $this->location()->associate($location);
        }
		
        if($optional['occupation'] != null)
            $this->occupations = $optional['occupation'];
        
        if($userId != null)
        {
            $user = User::find($userId);
            $this->user()->associate($user);
        }
		
                
        if($this->isDirty())
		  $this->save();
	}
    
    
    public function location()
    {
        return $this->belongsTo('Location','location_id','id');
    }
    
    
     public function hasLocation(){
         return $this->location != null;
    }
    
    
	//Relationship many to 1
	public function user() {
        return $this->belongsTo('User');
    }
    
     public function application(){
        return $this->hasOne('Application');
    }
    
    
    
}
