<?php

class Location extends Eloquent {
	protected $table = 'locations';
	protected $guarded = array();

	public static $rules = array();
    
    public function profile()
    {
        return $this->hasOne('Profile');
    }

       // one to many
    public function applicant()
    {
        return $this->hasMany('Applicant');
    }

//
//
//
//    public function storeUserLocation($userId,$address,$latitude,$longitude){
//
//    		$user = User::find($userId);
//			$this->address = $address;
//			$this->lat = $latitude;
//			$this->lng = $longitude;
//			$this->user()->associate($user);
//			$this->save();
//
//	}
}
