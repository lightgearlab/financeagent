<?php

class BankController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$pagesize = 10;

		$banks = Bank::orderBy('name')->paginate($pagesize);

		return View::make('manage.banks.index',compact('banks','pagesize'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return View::make('manage.banks.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		//
		try{

			$input = Input::all();
			$validation = Validator::make($input, Bank::$rules);

	        if ($validation->passes())
	        {
	        	$bank = new Bank;	
				$bank->name = Input::get('name');
				$bank->save();
				
	            return Redirect::route('manage.bank.index');
	        }

	       return Redirect::route('manage.bank.edit', $id)
	            ->withInput()
	            ->withErrors($validation)
	            ->with('message', 'There were validation errors.');

		}catch(Exception $e){

		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		//
		$bank = Bank::find($id);

		return View::make('manage.banks.show',compact('bank'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$bank = Bank::find($id);

		return View::make('manage.banks.edit',compact('bank'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		//
		try{

			$input = Input::all();


			$validation = Validator::make($input, Bank::$rules);

			if ($validation->passes())
	        {
	        	$bank = Bank::find($id);

				if($bank != null){

					$bank->name = Input::get('name');
					$bank->save();

					return Redirect::route('manage.bank.index');
				}
	        }

	        return Redirect::route('manage.bank.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
			

		}catch(Exception $e){

		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{	
		if(Request::ajax())
	    {
			$bank = Bank::find($id);
			$bank->delete();		
		}
	}


}
