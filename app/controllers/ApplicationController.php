<?php

class ApplicationController extends \BaseController {
    

    public function showSubmission(){
        	return View::make('application.submission');
        
    }


	public function showApplication(){

        $application = new Application;
        
        $applicant = null;
        $score = null;
        
		$appCode = Input::get("code",'');
        
        $productCategories =  ProductCategory::orderBy('name','asc')->lists('name', 'id');
        $locations = Location::orderBy('city','asc')->lists('city', 'id');
        $interestRates =  InterestRate::orderBy('name','asc')->lists('name', 'id');
     
        

		if(!empty($appCode)){
			$application = Application::where('code','=',$appCode)->first();
                
            Session::put('AppCode',$application->code);

		}else{
            
           $application = new Application;
		}
        
        $score = $application->getUserScore();
        
        $applicant = $application->getApplicant();


	    return View::make("application.index",
              compact('appCode','application','score','applicant','locations','productCategories','interestRates'));

	}

	public function updateCustomer(){

		try{

			$name = Input::get("Name","");
			$phoneNumber = Input::get("PhoneNumber","");
			$email = Input::get("Email","");
			$location = Input::get("Location","");
			$occupation = Input::get("Occupation","");
			$ic_number = Input::get("ICNumber","");
			$userId = Auth::user()->id;
            
            $appCode = Session::get('AppCode');
           
            $application = Application::whereCode($appCode)->first();
            $applicant = $application->applicant;
            
            $applicant->saveApplicant($userId, 
                                      array( 'name' => $name,
                                            'phoneNumber' => $phoneNumber,
                                            'email' => $email,
                                            'locationId' => $location,
                                            'occupation' => $occupation,
                                            'ICNumber' => $ic_number));
//			$applicant = new Applicant;
//			$applicant->createApplicant($userId,
//				$name,
//				$phoneNumber,
//				$email,
//				$location,
//				$occupation,
//				$ic_number);         
//            
//            if(Input::has('agent') && Input::has('product')){
//                $agentId = Input::get("agent","");
//                
//                $application = new Application;
//                $application->createApplication($applicant->id,$agentId);
//            }else{
//                
//            }
           
            $message = "Successfully.";

		    return Response::json(array(
								        'error' => false,
								        'result' => array( 'success' => true, 'applicant' => $applicant),
								        'message' => $message),200);

		}
		catch(Exception $e)
		{
            Log::info($e->getMessage());

            $message = $e->getMessage();

            return Response::json(array(
                    'error' => true,
                    'result' => null,
                    'message' => $message),200);
		}
	}
    
    public function doApplyAgent(){

		try{

            $error = true;
            $message = 'Successful';$status = '';
            $redirect = '';
			$agentUId = Input::get('AgentId','');
            $mainProductUId = Input::get('MainProductId','');
			$subProductUId = Input::get('SubProductId','');
              Log::info(Input::all());
			if(Auth::check()){
                $application = Application::getLastPendingApplication(
                    Auth::user()->id,$mainProductUId,$subProductUId);

                if($agentUId != ''&& $mainProductUId != '' && $subProductUId != '')
                {
                    if($application == null){
                        $applicant = new Applicant;
                        $applicant->saveApplicant(Auth::user()->id);

                        $application = new Application;
                        $application->saveApplication($applicant->id,'Incompleted');


                        $appProduct = new ApplicationProduct;

                        $appProduct->saveApplicationProduct(
                            $application->id,
                            $mainProductUId,
                            $subProductUId,
                            $agentUId);

                    }
                    $redirect = URL::to('/application?code=') . $application->code ;
                    $error = false;    
                    $message= "Successful";
                    $status = "Success";

                }
                else{
                    $message= "Missing parameters.";
                    $status = 'failed';
                }
            }else{
                $error = false;
                $status = 'Failed';
                $message = 'Please login.';
                $redirect = URL::to('/login?prevUrl=' . Request::url());
            }
            return Response::json(array(
            'error' => $error,
            'result' => array( 'redirect_url' => $redirect, 'status' => $status),
            'message' => $message),200);

		}catch(Exception $e){

			$message = "Failed.";
            
            Log::info($e->getMessage());
			return Response::json(array(
								'error' => true,
								'result' => '',
								'message' => $e->getMessage()),200);

		}
	}
    
     public function showProducts(){
        
        $appProducts = null;
        
        $appCode = Input::get('code','');
        $application = Application::whereCode($appCode)->first();
        
        if($application != null && $application->applicationproduct != null){
           
            $appProducts = $application->applicationproduct;
        }
     
        $message = "Successfully.";

		return Response::json(array(
		'error' => false,
		'result' => View::make('application._productlist',compact('appCode','appProducts','application'))->render(),
		'message' => $message),200);
    }
    
    
    public function showDocuments(){
        
        $appDocuments = null;
        
        $appCode = Input::get('code','');
        $application = Application::whereCode($appCode)->first();
        
        if($application != null && $application->applicationdocument != null){
           
            $appDocuments = $application->applicationdocument;
        }
     
        $message = "Successfully.";

		return Response::json(array(
		'error' => false,
		'result' => View::make('application._documentlist',compact('appDocuments'))->render(),
		'message' => $message),200);
    }
    
    public function doUploadDocument(){
        
        $document = null;
        
		try{
            
            if (Input::hasFile('UserDocument'))
			{
                $document = new Document;
                
                $appCode = Input::get('appCode','');
			    $file = Input::file('UserDocument');
                
                $orignalName = $file->getClientOriginalName();
			    $newName = strval(uniqid()) . "." . $file->getClientOriginalExtension();

			    $file->move('upload/documents', $newName);

			    $file_url = "upload/documents/" . $newName;

			    $message = "Successful.";
                
                $document->saveDocument($orignalName,$file->getClientOriginalExtension(),$file_url);
                
                $appDocument = new ApplicationDocument;
                
                $appDocument->saveApplicationDocument($appCode,$document->id);

			}else{

				$message = "Failed.";
			}
            
            $message = "Successfully.";

            return Response::json(array(
								        'error' => false,
								        'result' => array( 'success' => true, 'document' => $document),
								        'message' => $message),200);

		}
		catch(Exception $e)
		{
            Log::info($e->getMessage());

            $message = $e->getMessage();

            return Response::json(array(
                    'error' => true,
                    'result' => null,
                    'message' => $message),200);
		}
        
    }
    
    public function doAddApplicationProduct(){
        
		try{
            $message = "Successfully.";
			$appCode = Input::get("appCode");
            $mainProductId = Input::get("MainProductId","");
            $subProductId = Input::get("SubProductId","");
            $agentId = Input::get("AgentId","");
               
            $application = Application::whereCode($appCode)->first();
            
            if($application != null){
                
                if(Request::ajax())
                {
                    $appProduct = new ApplicationProduct;
                    $appProduct->saveApplicationProduct($application->id,$mainProductId,$subProductId,$agentId);
                    
                    $message = "Successfully.";

                    return Response::json(array(
                                            'error' => false,
                                            'result' => array( 'success' => true),
                                            'message' => $message),200);
                }
            }else{
                
                $message = "Failed. Application not exist.";
                
                return Response::json(array(
                                            'error' => true,
                                            'result' => null,
                                            'message' => $message),200);
                
            }                
			
		   
		}
	    catch(Exception $e)
		{
			Log::info($e->getMessage());

			$message = $e->getMessage();

			return Response::json(array(
			        'error' => true,
			        'result' => null,
			        'message' => $message),200);
		}
    }
    
    
    public function doRemoveApplicationProduct(){
        
		try{
            $message = "Successfully.";
			$appCode = Input::get("appCode");
            $appProductId = Input::get("AppProductId","");
                   
            $application = Application::whereCode($appCode)->first();
            
            if($application != null){

                $agentProduct = ApplicationProduct::find($appProductId);
                
                if($application->isApplicant() && $agentProduct != null){
                    $agentProduct->delete();

                    $message = "Successfully.";

                    return Response::json(array(
                                            'error' => false,
                                            'result' => array( 'success' => true),
                                            'message' => $message),200);
                }else{
                    
                      return Response::json(array(
                            'error' => true,
                            'result' => array( 'success' => true),
                            'message' => $message),200);
                }

            }else{
                
                $message = "Failed. Application not exist.";
                
                return Response::json(array(
                                            'error' => true,
                                            'result' => null,
                                            'message' => $message),200);
                
            }                
			
		   
		}
	    catch(Exception $e)
		{
			Log::info($e->getMessage());

			$message = $e->getMessage();

			return Response::json(array(
			        'error' => true,
			        'result' => null,
			        'message' => $message),200);
		}
    }
    
    public function showRecommend(){

		$user = Auth::user();
		
		try{
            
			$appCode = Input::get("code");
            
            $application = Application::whereCode($appCode)->first();
            
            $products = $application->AgentAvailableProduct()->get();
            
			if(Request::ajax())
	    	{
		    	$message = "Successfully.";

		    	return Response::json(array(
				        'error' => false,
				        'result' => View::make('shared._recommend',compact('products','appCode'))->render(),
				        'message' => $message),200);
	    	}
		    else
		    {
		    	return View::make('shared._recommend',compact('products','appCode'));
		    }
		   
		}
	    catch(Exception $e)
		{
			Log::info($e->getMessage());

			$message = $e->getMessage();

			return Response::json(array(
			        'error' => true,
			        'result' => null,
			        'message' => $message),200);
		}

	}
    
    public function doSubmitApplication(){

		$user = Auth::user();
		$message = "Successfully.";
        $redirect = '';
        
		try{
            
            $isExist = Input::get("IsExist",0);
			$appCode = Input::get("appCode","");
            
            $application = Application::whereCode($appCode)->first();
            
            if(Request::ajax())
	    	{
                if($application != null){
                    
                    $application->submitApplication($isExist);
                    
                    $message = "Successfully.";

                    $redirect = URL::to('/application/submission?code=') . $application->code ;
                    return Response::json(array(
                            'error' => false,
                            'result' => array( 'success' => true, 'redirect' => $redirect),
                            'message' => $message),200);
                }
	    	}
		    else
		    {
		    	return View::make('application.submission',compact('appCode'));
		    }
		   
		}catch(Exception $e)
		{
			Log::info($e->getMessage());

			$message = $e->getMessage();

			return Response::json(array(
			        'error' => true,
			        'result' => null,
			        'message' => $message),200);
		}

	}


}
