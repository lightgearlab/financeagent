<?php

class ConversationController extends \BaseController {


	public function showParticipant($id){		
		
		$message = "Successfully.";

		$conversation = Conversation::find($id);

		$participants = $conversation->otherUsers();

		return Response::json(array(
							'error' => false,
							'result' => View::make('message._participant',compact('participants'))->render(),
							'message' => $message),200);

	}

	public function showConversations(){

	  	$conversations = Auth::user()->conversations;
	 
	  	$message = "Successfully.";

		return Response::json(array(
							'error' => false,
							'result' => View::make('message._conversations',compact('conversations'))->render(),
							'message' => $message),200);
	}


	public function doConversation(){

		
		$participants = explode(',',Input::get('participants'));
		$message = Input::get('message');

		array_push($participants,Auth::user()->id);

		$conversation = ConversationManager::startConversation($participants);

		$conversation->addMessage($message);
	}


	public function doSendMessage($id){

		$content = Input::get('content','');

		$conversation = Conversation::find($id);

		$message = $conversation->addMessage($content);

		return Response::json(array(
							'error' => false,
							'result' => View::make('message._content',compact('message'))->render(),
							'message' => "Success"),200);
	}


	public function showMessages($id){		
		
		$message = "Successfully.";

		$conversation = Conversation::find($id);

		$messages = $conversation->messages;
		return Response::json(array(
							'error' => false,
							'result' => View::make('message._contents',compact('messages'))->render(),
							'message' => $message),200);

	}


}
