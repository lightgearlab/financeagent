<?php

class ReviewController extends \BaseController {


	public function showList($id = null){

		$perPage = 5;


		$profile = Profile::find($id);

		$reviews = null;

		if($profile != null)
			$reviews = Review::where('profile_id',$profile->id)->orderBy('created_at','desc')->paginate($perPage);

		if(Request::ajax())
	    {	

	    	if($reviews != null)
	    	{
		    	$message = "Successfully.";

		    	return Response::json(array(
				        'error' => false,
				        'result' => View::make('profile.review-list2',compact('reviews'))->render(),
				        'message' => $message),200);
	    	}else
	    	{
	    		$message = "Failed.";

		    	return Response::json(array(
				        'error' => true,
				        'result' => '',
				        'message' => $message),200);
	    	}
	  
		}else
		{
			return View::make('profile.review-list2',compact('reviews'));
		}

	}

	public function addReview($id){

		$review = new Review;

		if(Input::has("rating") && Input::has("comment"))
			$review->storeReviewForProfile($id,Input::get("comment"),Input::get("rating"));

		$profile = Profile::find($id);

		return Redirect::route('profile.show', array('profile' => $profile->uid)); 
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//

		try{
			$review = Review::find($id);

			if($review->removeReview(Auth::user()->profile->id)){

				$message = 'Successfull.';
			   
			   return Response::json(array(
		        'error' => false,
		        'message' => $message ),
		        200);
			}
		   		
		   	else{

			   $message = 'Not allowed.';
			   
			   return Response::json(array(
		        'error' => true,
		        'message' => $message ),
		        401);
		   	}	

	   	}catch(Exception $e){


				return Redirect::to('error')
	            ->with('message', $e->getMessage());
		}
	}
	


}
