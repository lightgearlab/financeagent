<?php

class ReserveController extends \BaseController {

	public function showAgentReserve(){
        
        $pageSize = 12;
        $q = array();
        $agentSaves = AgentSaved::UserId(Auth::user()->id)->paginate($pageSize);
        
        return View::make('saved.agent',compact('agentSaves','q'));
    }
    
    public function showProductReserve(){
        
        $pageSize = 12;
        $q = array();
        $productSaves = ProductSaved::UserId(Auth::user()->id)->paginate($pageSize);
        
        return View::make('saved.product',compact('productSaves','q'));        
    }
    
	public function showReserve(){

		$reserves = Reserve::UserId(Auth::user()->id)->paginate(12);

		return View::make('reserve',compact('reserves'));
	}

	public function showSavedList(){

		$productsaved = ProductSaved::UserId(Auth::user()->id);
		$agentsaved = AgentSaved::UserId(Auth::user()->id);

		$array = array_merge($productsaved->toArray(), $agentsaved->toArray());
		return Response::json($array);

		// $productsaved = DB::table('product_saved')
		// 					 ->select('product_id','user_id',DB::raw('1 as type'));
							 
		// $agentsaved = DB::table('agent_saved')
  //                           ->select('agent_id','user_id',DB::raw('2 as type'));



		// $results = $productsaved->unionall($agentsaved)->take(30)->get();

		// dd($results);
	}


	public function doAgentSave($uid){


		if(Request::ajax())
		{

			try{
                $agent = Agent::whereUid($uid)->first();
                
                if($agent != null && !$agent->hasSaved())
                {
                    $url = URL::to('/agent/saved/remove/' . $uid ) ;
                    $agentSaved = new AgentSaved;
                    $agentSaved->storeReserve($uid);
                    $status = 'saved';

                    return Response::json(
                                array(
                                'error' => false,
                                'result' => 
                                array('url' => $url, 
                                      'status' => $status, 
                                      'count' => Auth::user()->getAgentSavedCount()),
                                'message' => 'Success')
                                ,200);
                }else{
                    return Response::json(
                                array(
                                'error' => true,
                                'result' => null,
                                'message' => 'Has been saved.')
                                ,200);
                }


			}catch(Exception $e){

				return Response::json(
				    		array(
					        'error' => true,
					        'message' => $e->getMessage())
				    		,500);

			}
			
		}else
		{

		}
	}
    
    public function doAgentSavedRemove($uid){


		if(Request::ajax())
		{

			try{
				
				$agent = Agent::whereUid($uid)->first();
                
                if($agent != null && $agent->hasSaved())
                {
                    $url = URL::to('/agent/saved/add/' . $uid);
                  
                    $agentSaved = AgentSaved::AgentSaved($agent->id)->first();
                    $agentSaved->removeReserve();
                    
                    $status = 'removed';

                    return Response::json(
                                array(
                                'error' => false,
                                'result' => 
                                array('url' => $url, 
                                      'status' => $status, 
                                      'count' => Auth::user()->getAgentSavedCount()),
                                'message' => 'Success')
                                ,200);
                }else{
                    	return Response::json(
				    		array(
					        'error' => true,
					        'message' => 'Error')
				    		,404);
                }

			}catch(Exception $e){

				return Response::json(
				    		array(
					        'error' => true,
					        'message' => $e->getMessage())
				    		,500);

			}
			
		}else
		{

		}
	}

	public function doProductSave(){

        if(Request::ajax())
		{

			try{
                $mainProductUid = Input::get('primary','');
                $subProductUid = Input::get('sub','');
          
                $product = Product::whereUid($subProductUid)->first();
                
                if($product != null && !$product->hasSaved())
                {
                    $url = URL::to('/product/saved/remove/' . $product->uid ) ;
                    $productSaved = new ProductSaved;
                    $productSaved->storeReserve($mainProductUid,$subProductUid);
                    $status = 'saved';

                    return Response::json(
                                array(
                                'error' => false,
                                'result' => 
                                array('url' => $url, 
                                      'status' => $status, 
                                      'count' => Auth::user()->getProductSavedCount()),
                                'message' => 'Success')
                                ,200);
                }else{
                    return Response::json(
                                array(
                                'error' => true,
                                'result' => null,
                                'message' => 'Has been saved.')
                                ,200);
                }


			}catch(Exception $e){
                Log::info($e->getMessage());
				return Response::json(
				    		array(
					        'error' => true,
					        'message' => $e->getMessage())
				    		,500);

			}
			
		}else
		{

		}
	}

	

	public function doProductSavedRemove($uid){

		if(Request::ajax())
		{
			try{
				
				$product = Product::whereUid($uid)->first();
                
                if($product != null && $product->hasSaved())
                {
                    $url = URL::to('/product/saved/add/' . $uid);
                  
                    $productSaved = ProductSaved::ProductSaved($product->id)->first();
                    $productSaved->removeReserve();
                    $status = 'removed';

                    return Response::json(
                                array(
                                'error' => false,
                                'result' => 
                                array('url' => $url, 
                                      'status' => $status, 
                                      'count' => Auth::user()->getProductSavedCount()),
                                'message' => 'Success')
                                ,200);
                }else{
                    	return Response::json(
				    		array(
					        'error' => true,
					        'message' => 'Error')
				    		,404);
                }

			}catch(Exception $e){
                Log::info($e->getMessage());
				return Response::json(
				    		array(
					        'error' => true,
					        'message' => $e->getMessage())
				    		,500);

			}
			
		}else
		{

		}
	}

	public function doRemove($id = null){


		if(Request::ajax())
		{

			try{
				
				$reserve = Reserve::find($id);
				
				if($reserve != null){

				$reserve->removeReserve();

				return Response::json(
				    		array(
					        'error' => false,
					        'result' => Auth::user()->getReservedCount(),
					        'message' => 'Success')
				    		,200);
				}else{

					return Response::json(
				    		array(
					        'error' => true,
					        'message' => 'Error')
				    		,404);
				}


			}catch(Exception $e){

				return Response::json(
				    		array(
					        'error' => true,
					        'message' => $e->getMessage())
				    		,500);

			}
			
		}else
		{

		}
	}

}
