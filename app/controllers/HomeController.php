<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}


	public function showSearchAgent(){
		return View::make('search');
	}

	public function showError(){

		if(Session::has("message"))
			return View::make('error');
		else
			return Redirect::to("/");
	}
    
    public function showMain(){
        $user = null;
        $mainProducts = null;
        
        $mainProducts = ProductCategory::all();
        
        if(Auth::check())
            $user = Auth::user();

        return View::make('main2',compact('user','mainProducts'));
    }
}
