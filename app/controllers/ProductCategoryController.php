<?php

class ProductCategoryController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$pagesize = 10;

		$categories = ProductCategory::orderBy('name')->paginate($pagesize);

		return View::make('manage.productcategory.index',compact('categories','pagesize'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return View::make('manage.productcategory.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		try{

			$input = Input::all();
			$validation = Validator::make($input, ProductCategory::$rules);

	        if ($validation->passes())
	        {
	        	$category = new ProductCategory;	
				$category->name = Input::get('name');
				$category->active = Input::get('active');
				$category->save();
				
	            return Redirect::route('manage.category.index');
	        }

	       return Redirect::route('manage.category.edit', $id)
	            ->withInput()
	            ->withErrors($validation)
	            ->with('message', 'There were validation errors.');

		}catch(Exception $e){

		}
		
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$category = ProductCategory::find($id);

		return View::make('manage.productcategory.show',compact('category'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$category = ProductCategory::find($id);

		return View::make('manage.productcategory.edit',compact('category'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		try{

			$input = Input::all();


			$validation = Validator::make($input, ProductCategory::$rules);

			if ($validation->passes())
	        {
	        	$category = ProductCategory::find($id);

				if($category != null){

					$category->name = Input::get('name');
					$category->active = Input::get('active');
					$category->save();

					return Redirect::route('manage.category.index');
				}
	        }

	        return Redirect::route('manage.category.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
			

		}catch(Exception $e){

		}
		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{	
		if(Request::ajax())
	    {
			$category = ProductCategory::find($id);
			$category->delete();		
		}
	}


}
