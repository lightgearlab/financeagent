<?php

class AuthController extends \BaseController {

	public function showLogin(){
		return View::make('login');
	}

	public function doLogin(){
			// validate the info, create rules for the inputs
		

		$messages = array(
        'txtEmail.required'=>'Email cannot be empty.',
        'txtEmail.email' => 'Invalid email.',
        'txtPassword.required'=>'Password is required.',
        'txtPassword.min' => 'The password must be at least 3 characters.'
	    );

		$rules = array(
			'txtEmail'    => 'required|email', // make sure the email is an actual email
			'txtPassword' => 'required|min:3' // password can only be alphanumeric and has to be greater than 3 characters
		);
Log::info(Input::all());
		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules,$messages);

		
		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			return Redirect::to('login')
				->withErrors($validator) // send back all errors to the login form
				->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
		} else {

			// create our user data for the authentication
			$userdata = array(
				'email' 	=> Input::get('txtEmail'),
				'password' 	=> Input::get('txtPassword')
			);
 
			// attempt to do the login
			if (Auth::attempt($userdata)) {


				$token = Auth::user()->getRememberToken();
				$user = Auth::user();
                $user->updateLoginInfo($token);
                
                $remember = Input::get('remember');
                if(!empty($remember))
                {
                    Auth::login(Auth::user(),true);
                }

				return Redirect::to('/profile/' . $user->profile->uid);

			} else {	 	

			
				return Redirect::to('login')->withErrors(array('login' => 'Email or Password is incorrect.'));

			}

		}
	}


	public function showRegister(){
		return View::make('register');
	}


	public function doRegister(){
		
		$messages = array(
        'txtName.required'=>'Name cannot be empty.',
        'txtEmail.required'=>'Email cannot be empty.',
        'txtEmail.email' => 'Invalid email.',
        'txtEmail.unique' => 'The email has already been taken.',
        'txtPassword.required'=>'Password is required.',
        'txtPassword.alphaNum' => 'The password must be alphanumeric.',
        'txtPassword.min' => 'The password must be at least 3 characters.'
	    );

		
		$rules = array(
            'txtName' => 'required|min:5', // password can only be alphanumeric and has to be greater
			'txtEmail'    => 'required|email|unique :users,email', // make sure the email is an actual email
			'txtPassword' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
		);

		$input = Input::all();
        $validation = Validator::make($input, $rules,$messages);

        if ($validation->passes())
        {
        	$user = new User;
			
			$user->uid = Str::random(40);
			$user->email = Input::get('txtEmail');
			$user->password = Hash::make(Input::get('txtPassword'));
			$user->save();	

			$profile = new Profile;
            $profile->uid = Str::random(20);
			$profile->name = Input::get('txtName');
			$profile->photo_url = "/images/users/default.png";
			$profile->photointernal = true;
			$profile->user()->associate($user);
			$profile->save();
			
			$newId = $user->id;

			$role = Role::find(2);
			$user = $user->roles()->save($role);
			$user->save();
					
			Auth::loginUsingId($newId);

			$token = Auth::user()->getRememberToken();

			$user = Auth::user();
			$user->logincounter += $user->logincounter + 1;
			$user->logindate = new DateTime();
			$user->setRememberToken($token);
			$user->save();

			General::SendWelcomeEmail($user,"Visitor");

            return Redirect::to('/profile/' . $profile->uid);
        }

        return Redirect::to('register')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');

	}


	public function showAgentLogin(){
		return View::make('agent.login');
	}


	public function showAgentRegister(){

		$banks = Bank::orderBy('name','asc')->lists('name', 'id');

		$products = Product::all();

		return View::make('agent.register',compact('products','banks'));
	}


	public function doAgentRegister(){
		
		$messages = array(
        'txtName.required'=>'Name cannot be empty.',
        'txtEmail.required'=>'Email cannot be empty.',
        'txtEmail.email' => 'Invalid email.',
        'txtEmail.unique' => 'The email has already been taken.',
        'txtPassword.required'=>'Password is required.',
        'txtPassword.alphaNum' => 'The password must be alphanumeric.',
        'txtPassword.min' => 'The password must be at least 3 characters.'
	    );

		
		$rules = array(
            'txtName.required'=>'Name cannot be empty.',
			'txtEmail'    => 'required|email|unique :users,email', // make sure the email is an actual email
			'txtPassword' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
		);

		
		$input = Input::all();
        $validation = Validator::make($input, $rules,$messages);

        if ($validation->passes())
        {
        	$user = new User;
			
			$user->storeUser(Input::get('txtEmail'),Hash::make(Input::get('txtPassword')));
			/*$user->email = Input::get('txtEmail');
			$user->password = Hash::make(Input::get('txtPassword'));
			$user->save();*/	

			$profile = new Profile;
			$profile->storeProfile($user->id,
								   Input::get('txtName'),
								   null,
								   null,
								   null,
								   null,
								   null,
								   "images/users/default.png",
								   null,
								   true,
								   3.5,
								   null);

			/*$profile->name = "Agent";
			$profile->photo_url = "/images/users/default.png";
			$profile->photointernal = true;
			$profile->user()->associate($user);
			$profile->save();*/
			
			$newId = $user->id;

			$role = Role::find(3);
			$role = $user->roles()->save($role);

			$agent = new Agent;
			/*$agent->user()->associate($user);
			$agent->save();*/

			$agent->storeAgent($user->id,null);
			
		
			Auth::loginUsingId($newId);

			$token = Auth::user()->getRememberToken();

			$user = Auth::user();
			/*$user->logincounter += $user->logincounter + 1;
			$user->logindate = new DateTime();
			$user->setRememberToken($token);
			$user->save();*/

			$user->updateLoginInfo($token);

			General::SendWelcomeEmail($user,"Agent");

            return Redirect::to('/profile/' . $profile->uid);
        }

        return Redirect::to('agent/register')
            ->withInput()
            ->withErrors($validation)
            ->with('message', $validation->messages());

	}


	public function doLogout(){
		
		if(Auth::check()){
			$user = Auth::user();
			$user->updateLogoutInfo(null);

			Auth::logout();
		}

    	return Redirect::to('/');
	}


	public function showAuthorize(){

		
		return View::make('authorize');
	}

	public function doAuthorize(){

		$status = false;

		//Log::info(Session::get('redirect_url'));
		if(Auth::check())
			$status = true;
		else
			$status = false;

		return Response::json($status,200);
	}


	public function loginFB(){
		
		 /**
	     * Obtain an access token.
	     */

	    $redirect_url = action('HomeController@index');
	    
	    try
	    {
	        $token = Facebook::getTokenFromRedirect();

	        if ( ! $token)
	        {
	            return Redirect::to('/')->with('error', 'Unable to obtain access token.');
	        }
	    }
	    catch (FacebookQueryBuilderException $e)
	    {
	        return Redirect::to('/')->with('error', $e->getPrevious()->getMessage());
	    }

	    if ( ! $token->isLongLived())
	    {
	        /**
	         * Extend the access token.
	         */
	        try
	        {
	            $token = $token->extend();
	        }
	        catch (FacebookQueryBuilderException $e)
	        {
	            return Redirect::to('/')->with('error', $e->getPrevious()->getMessage());
	        }
	    }

	    Facebook::setAccessToken($token);

	    /**
	     * Get basic info on the user from Facebook.
	     */
	    try
	    {
	        $facebook_user = Facebook::object('me')->fields('id','name','first_name','last_name','email','age_range','gender','birthday')->get();
	    	
	    	$redirect_url = $this->CreateOrUpdateUser($facebook_user,$token);
	    }
	    catch (FacebookQueryBuilderException $e)
	    {
	        return Redirect::to('/')->with('error', $e->getPrevious()->getMessage());
	    }

	    // Create the user if not exists or update existing
	    //$user = User::createOrUpdateFacebookObject($facebook_user);



	    // Log the user into Laravel
	    //Facebook::auth()->login($user);

	   	//return Redirect::to('/')->with('message', 'Successfully logged in with Facebook');
	     Session::put('redirect_url',$redirect_url);

	 	 return Redirect::to('/authorize');
	
	}


	public function CreateOrUpdateUser($userData,$token){

		$valid = false;
		$user = null;
		$account =  null;
		$uid = '';
		$url = action('HomeController@index');
		//Log::info($userData);

		if(isset($userData["id"])){

			$uid = $userData['id'];
	    	$account = Account::whereUid($userData['id'])->first();
	    }


	    $blnNew = false;
			
	    if (empty($account)) {
	 

	    	$user = null;

	 		if(!isset($userData["email"])){

	 			//need to request email address.

	 			$user = new User;

	 		}else
	 		{
	 			$user = User::whereEmail($userData['email'])->first();

	 			if(empty($user))
	 				$user = new User;
	 		}


	 		$user->storeUser($userData['email']);

	 		$userAge = null;

	 		if(isset($userData["birthday"])){


				 $time = strtotime($userData["birthday"]);

			 	 $birthdate = date('Y-m-d',$time);
				 $userAge = date_diff(date_create($birthdate), date_create('now'))->y;

			}

	 		$profile->storeProfile($user->id,
							(isset($userData["name"]) ? $userData["name"] : null),
							(isset($userData["first_name"]) ? $userData["first_name"] : null),
							(isset($userData["last_name"]) ? $userData["last_name"] : null),
							null,
							$userAge,
							(isset($userData["gender"]) ? $userData["gender"] : null),
							'https://graph.facebook.com/'. $uid .'/picture?type=large',
							null,
							false,
							null,
							null);


			$account = new Account();
	
			$account->uid = $uid;

			if(isset($userData["name"]))
				$account->username = $userData['name'];

			$account->provider = 'facebook';
			$account = $user->accounts()->save($account);
			$account->access_token = $token;
			$account->connect = true;
	 		$account->save();

			$blnNew = true;

			$valid = true;
			
	    }
		else {
			
			$blnNew = false;

			$valid = true;


			//update user profile
			$user = User::find($account->user->id);

			if(isset($userData["name"]))
				$user->name = $userData['name'];

			if(isset($userData["first_name"]))
				$user->first_name = $userData['first_name'];

			if(isset($userData["last_name"]))
				$user->last_name = $userData['last_name'];

			if(isset($userData["email"]))
				$user->email = $userData['email'];

			if(isset($userData["gender"]))
				$user->gender = $userData['gender'];	


			if(isset($userData["birthday"])){


				 $time = strtotime($userData["birthday"]);

			 	 $birthdate = date('Y-m-d',$time);
				 $user->age = date_diff(date_create($birthdate), date_create('now'))->y;

			}			

			$user->photo_url = 'https://graph.facebook.com/'. $uid .'/picture?type=large';
			$user->photointernal = 0;
			$user->save();



			if(isset($userData["name"]))
				$account->username = $userData['name'];

			$account->connect = true;
			$account->access_token = $token;
		    $account->save();
		 
		 	$user = $account->user;
			
		}
	 

		if($valid){
			
			Auth::login($user);
			
			Session::put('uid',$uid);

			Session::put('connect', 'facebook');



			$user->logincounter = $user->logincounter + 1;

			$user->save();

			$promocode = Session::get('promocode');

			if($user->mailchimp == 0 && $user->email != null && $user->email != ''){
				
				General::RegisterNewsletter($user);
				
			}

			if($promocode != null && $promocode != '')
			{
				$promoCode = PromoCode::where('code',$promocode)->where('active',true)->first();

				$userCode = UserPromoCode::where('user_id',$user->id)
											   ->where('promocode_id',$promoCode->id)
											   ->first();

				if(empty($userCode)){

					$status = General::RedeemPromoCode($user);

					if($status){
						$url = action('HomeController@showThank');

						Session::put('promoredirect',true);//temp
					}
						
					else
						$url = action('HomeController@index');
				}
					
			}else{
				$url = action('HomeController@index');
			}
	
				
		}


		return $url;

	}
	


}
