<?php

class ProfileController extends \BaseController {

	

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		$profile = Profile::whereUid($id)->first();
        $locations = Location::orderBy('city','asc')->lists('city', 'id');

		if($profile != null){
			$user = $profile->user;

			if($user->isAgent()){

				$agent = $user->agent;
				$banks = Bank::orderBy('name','asc')->lists('name', 'id');
                
				$mainproducts = ProductCategory::orderBy('name','asc')->lists('name', 'id');
				$subproducts = Product::orderBy('name','asc')->lists('name', 'id');

				return View::make('profile.show',compact('profile','user','agent','productcategory','banks','mainproducts','subproducts','locations'));
			}
			else
			{
				return View::make('profile.show',compact('profile','user','locations'));
			}
		}else
		{
			return Redirect::to('error')->with('message', 'User not exist.');
		}
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$profile = Profile::find($id);
		$user = $profile->user;

		if($user->isAgent()){
			
			$banks = Bank::orderBy('name','asc')->lists('name', 'id');

			$products = Product::all();
			$agent = $user->agent;



			return View::make('profile.edit',compact('profile','user','products','banks','agent'));
		}
		else
		{
			return View::make('profile.edit',compact('profile','user'));
		}

	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{

		//
		$messages = array(
        'email.required'=>'Email cannot be empty.',
        'name.required'=>'Name is required.',
        'biography.required'=>'Biography is required.',
        'phonenumber.numeric' => 'Phone number must be numeric',
	    );

		$rules = array(
			'email'    => 'required|email|unique:users,email,' . Auth::user()->id . ',id', 
			'name' => 'required',
			'biography' => 'required',
			'phonenumber' => 'numeric',
			
		);

		// run the validation rules on the inputs from the form
		$validator = Validator::make(Input::all(), $rules,$messages);


		$photo_url = null;

		$input = Input::all();


		// if the validator fails, redirect back to the form
		if ($validator->fails()) {
			

			return Response::json(array(
						'error' => true,
						'result' => '',
						'messages' => $validator->messages()),200);
		} else {

				$profile = Profile::whereUid($id)->first();
				$user = $profile->user;

				if (Input::hasFile('uploader'))
				{
					    $file = Input::file('uploader');

					    $newName = strval(uniqid()) . "." . $file->getClientOriginalExtension();

					    $file->move('images/users', $newName);

					    $photo_url = "images/users/" . $newName;
				}

				$profile->storeProfile($profile->user->id,
									Input::get("name",null),
									Input::get("txtFirstName",null),
									Input::get("txtLastName",null),
									Input::get("phonenumber",null),
									Input::get("txtAge",null),
									Input::get("ddlGender",null),
									$photo_url,
									Input::get("biography",null),
									($photo_url != null ? true : false),
									null,
									null);

				if(Input::has("email"))
					$profile->user->storeUser(Input::get("email"));


				if($user->isAgent()){

					$agent = $user->agent;

					if(Input::has("ddlBank")){
						
						/*if(isset($agent->bank))
							$agent->bank_id = Input::get("ddlBank");
						else
						{
							$agent->bank()->save(Bank::find(Input::get("ddlBank")));
						}*/
						$agent->bank()->associate(Bank::find(Input::get("ddlBank")));
						$agent->save();
						
					}

					if(Input::has("products")){
						$agent->products()->getRelatedIds();
						$agent->products()->detach();
						$agent->products()->attach(Input::get('products'));
						$agent->save();
					}

				}


				if(Input::has("location")){

					$location = $user->location;


					if($location != null)
					{	
						$location->storeUserLocation(
													$user->id,
													Input::get("location",''),
													Input::get("hidLat",0.0),
													Input::get("hidLng",0.0));
						/*$location->address = Input::get("txtLocation");
						$location->lat = Input::get("hidLat");
						$location->lng = Input::get("hidLng");
						$location->save();*/
					}else{

						$location = new  Location;

						/*$location->address = Input::get("txtLocation");
						$location->lat = Input::get("hidLat");
						$location->lng = Input::get("hidLng");*/
						$location->storeUserLocation(
													$user->id,
													Input::get("location",''),
													Input::get("hidLat",0.0),
													Input::get("hidLng",0.0));
												
						//$location->user()->associate($user)->save();
					}

				}

				$message = "Successful.";

				return Response::json(array(
								'error' => false,
								'result' => '',
								'message' => $message),200);

		  		//return Redirect::route('profile.show', array('profile' => $profile->id));
  		}

	}


	public function updateProfile($id)
	{

		//
//		$messages = array(
//	        'name.required'=>'Name is required.',
//	        'summary.required'=>'Summary is required.',
//	     	'location.required'=>'Location is required.',
//	    );
//
//		$rules = array(
//			'name' => 'required',
//			'summary' => 'required',
//			'location' => 'required',
//			
//		);

		// run the validation rules on the inputs from the form
		//$validator = Validator::make(Input::all(), $rules,$messages);

        $photo_url = null;

		// if the validator fails, redirect back to the form
//		if ($validator->fails()) {
//			
//
//			return Response::json(array(
//						'error' => true,
//						'result' => '',
//						'messages' => $validator->messages()),200);
//		} else {
        
        try{
            $profile = Profile::whereUid($id)->first();

            if($profile != null && $profile->isUserProfile()){
                $user = $profile->user;
                $profile->storeProfile($profile->user->id,
                                    Input::get("name",null),
                                    Input::get("txtFirstName",null),
                                    Input::get("txtLastName",null),
                                    Input::get("phonenumber",null),
                                    Input::get("txtAge",null),
                                    Input::get("ddlGender",null),
                                    $photo_url,
                                    Input::get("summary",null),
                                    ($photo_url != null ? true : false),
                                    null,
                                    null,
                                    Input::get('location',''));


                $message = "Successful.";
                $url = route('profile.show',   ['id' => $profile->uid] );

                return Response::json(array(
                                'error' => false,
                                'result' => '',
                                'url' => $url,
                                'message' => $message),200);
				
        }else{
            $message = "Failed.";

			return Response::json(array(
								'error' => true,
								'result' => '',
								'message' => $message),200);
            
        }
        
        }catch(Exception $e){

			$message = "Failed.";
            Log::info($e->getMessage());
			return Response::json(array(
								'error' => true,
								'result' => '',
								'message' => $message),200);

		}
		  		
  		

	}


	public function uploadProfileImg(){

		try{



			$user = User::find(Auth::user()->id);
			$profile = $user->profile;
			$photo_url= '';

			if (Input::hasFile('uploader'))
			{
			    $file = Input::file('uploader');

			    $newName = strval(uniqid()) . "." . $file->getClientOriginalExtension();

			    $file->move('images/users', $newName);

			    $photo_url = "images/users/" . $newName;

			    $message = "Successful.";

			    $profile->photo_url = $photo_url;

			    $profile->save();

			}else{

				$message = "Failed.";
			}
			
			return Response::json(array(
								'error' => false,
								'result' => '',
								'message' => $message),200);

		}catch(Exception $e){

			$message = "Failed.";

			return Response::json(array(
								'error' => true,
								'result' => '',
								'message' => $message),200);

		}
	}


	public function addProduct(){

		$error = false;
		try{

			$user = User::find(Auth::user()->id);
			$profile = $user->profile;
			$agent = $user->agent;

			if(Input::has('MainProduct') && Input::has('SubProduct') && Input::has('Description')){
				$mainproduct = Product::find(Input::get('MainProduct'));
				$subproduct = Product::find(Input::get('SubProduct'));
				$description = Input::get('Description');

				$isExist =  AgentProduct::where('product_id','=',Input::get('SubProduct'))
											   ->where('productcategory_id','=',Input::get('MainProduct'))
											   ->where('agent_id', '=', $agent->id)->exists();

				
				if(!$isExist)
				{
					$agentProduct = new AgentProduct;
					$agentProduct->product()->associate($subproduct);
					$agentProduct->agent()->associate($agent);
					$agentProduct->productcategory()->associate($mainproduct);
					$agentProduct->description = $description;
					$agentProduct->save();

					$error = false;
					$message = 'Successful';
				}else{
					$error = true;
					$message = 'Existed.';
				}

			}else{
				$error = true;
				$message = 'Failed';
			}
			
			return Response::json(array(
								'error' => $error,
								'result' => '',
								'message' => $message),200);

		}catch(Exception $e){

			$message = "Failed.";

			return Response::json(array(
								'error' => true,
								'result' => '',
								'message' => $message),200);

		}
	}

	public function getProductList($id){

		try{
			$agent = Agent::find($id);

			$products = $agent->agentproduct;

			$message = "Successfully.";

			return Response::json(array(
				'error' => false,
				'result' => View::make('profile._products',compact('products','agent'))->render(),
				'message' => $message),200);

		}catch(Exception $e){

			$message = "Failed.";

			return Response::json(array(
								'error' => true,
								'result' => '',
								'message' => $message),200);

		}

	}

	public function removeProduct($id){

		try{
			
			if(Auth::check()){
				$user = User::find(Auth::user()->id);
				$agent = $user->agent;

				$agentProduct = AgentProduct::whereId($id)->where('agent_id','=',$agent->id)->first();

				if($agentProduct != null)
				{
					$agentProduct->delete();
					$message = "Successfully.";
				}else
				{
					$message = "Failed.";
				}
			}else{
				$message = "Failed. Not Authorized";
			}

			return Response::json(array(
				'error' => false,
				'result' => '',
				'message' => $message),200);

		}catch(Exception $e){

			$message = "Failed.";

			return Response::json(array(
								'error' => true,
								'result' => '',
								'message' => $message),200);

		}

	}

	

	public function showMessage($id = null){

			$profile = Profile::find($id);

			$message = "Successfully.";

			return Response::json(array(
								'error' => false,
								'result' => View::make('profile.mail',compact('profile'))->render(),
								'message' => $message),200);
	}

	public function doMessage($id = null){

		try{

			$profile = Profile::find($id);

			$userMessage = new UserMessage;

			$userMessage->sender_id = Auth::user()->id;
			$userMessage->receiver_id = $profile->user->id;
			$userMessage->from = Auth::user()->email;
			$userMessage->to = $profile->user->email;
			$userMessage->subject = Input::get("subject",'');
			$userMessage->body = Input::get("message",'');
			$userMessage->send = false;

			$userMessage->save();

			General::SendEmail('emails.message',$userMessage);

			$message = "Successfully.";

			return Response::json(array(
				'error' => false,'message' => $message),200);


		}
		catch(Exception $e){

			Log::info("Custom Error : " . $e->getMessage());

			$message = $e->getMessage();

			return Response::json(array(
				'error' => true,'message' => $message),500);


		}
	}



	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


	


}
