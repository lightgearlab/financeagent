<?php

class SearchController extends \BaseController {


	public function showProductPage(){

		$scope = Input::get("scope",'');
        $MainProductId = Input::get("primary",'');
        
		$SearchURL = '';
		$PageTitle = '';

		if($scope == '' || $scope == 'main'){
			$PageTitle  = 'Products';
			$SearchURL = URL::to('/search/product/main?scope=' . $scope);	
		}else{
			$PageTitle  = 'Sub Products';
			$SearchURL = URL::to('/search/product/sub?scope=' . $scope . '&primary=' . $MainProductId);
		}
		return View::make('search.product',compact('SearchURL','PageTitle','MainProductId'));
	}

	public function showMainProduct(){
		$perPage = 12;
		$filter = false;
		$q = array();

		$keyword = Input::get("keyword",''); 
		$location = Input::get("location",'');


		$productCategories = array();

		if($keyword != '') 
		{	
		 	$filter = true;
		 	if(count($productCategories) > 0)
		 		$productCategories = $productCategories->ProductCategoryKeyword($keyword);
		 	else
		 		$productCategories = ProductCategory::ProductCategoryKeyword($keyword);
			
			$q['keyword'] = $keyword;
		}

		if($filter)
		 	$productCategories = $productCategories->paginate($perPage);
		else
		{
			$productCategories = ProductCategory::paginate($perPage);		
		} 	



		$message = "Successfully.";

		return Response::json(array(
			'error' => false,
			'result' => View::make('search._mainproduct',compact('productCategories'))->render(),
			'message' => $message),200);
	}


    public function showSubProduct(){

		$perPage = 12;
		$filter = false;
		$q = array();

		$keyword = Input::get("keyword",'');

		$service = Input::get("service",'');
		
		$bank = Input::get("bank",'');
		
		$location = Input::get("location",'');
		
		$rating = Input::get('rating','');

		$MainProductId = Input::get('primary','');

		$scope = Input::get('scope','');

		$products = array();

		$q['scope'] = $scope;

		if($MainProductId != ''){

			$filter = true;

		 	if(count($products) > 0)
		 		$products = $products->SubProduct($MainProductId);
		 	else
		 		$products = Product::SubProduct($MainProductId);
			
			$q['primary'] = $MainProductId;
		}
		
		if($keyword != '')
		{	
		 	$filter = true;
		 	if(count($products) > 0)
		 		$products = $products->ProductKeyword($keyword);
		 	else
		 		$products = Product::ProductKeyword($keyword);
			
			$q['keyword'] = $keyword;
		}

		if($service != ''){

		 	$filter = true;
		 	if(count($agents) > 0)
		 		$agents = $agents->ServiceName($service);
		 	else
		 		$agents = Agent::ServiceName($service);
			
			$q['service'] =  $service;
		}

		if($bank != ''){

		 	$filter = true;
		 	if(count($agents) > 0)
		 		$agents = $agents->BankName($bank);
		 	else
		 		$agents = Agent::BankName($bank);

		 	$q['bank'] = $bank;
		}

		if($location != ''){

		 	$filter = true;
		 	if(count($agents) > 0)
		 		$agents = $agents->LocationName($location);
		 	else
		 		$agents = Agent::LocationName($location);
			
			$q['location'] = $location;
		}


		if($rating != '')
		{	
		 	$filter = true;
		 	if(count($agents) > 0)
		 		$agents = $agents->UserRating($rating);
		 	else
		 		$agents = Agent::UserRating($rating);

		 	$q['rating'] = $rating;
		}

		if($filter)
		 	$products = $products->paginate($perPage);
		else
		{
			$products = Product::paginate($perPage);

			//dd(count($agents));
			
		} 	

		 $message = "Successfully.";

		return Response::json(array(
		'error' => false,
		'result' => View::make('search._subproduct',compact('products','MainProductId','q'))->render(),
		'message' => $message),200);
	
	}


	public function showProduct(){

		$perPage = 12;
		$filter = false;
		$q = array();

		$keyword = Input::get("keyword",'');

		$service = Input::get("service",'');
		
		$bank = Input::get("bank",'');
		
		$location = Input::get("location",'');
		
		$rating = Input::get('rating','');

		$categories = ProductCategory::all();
		
		$banks = Bank::all();

		$products = array();
		
		if($keyword != '')
		{	
		 	$filter = true;
		 	if(count($products) > 0)
		 		$products = $products->ProductKeyword($keyword);
		 	else
		 		$products = Product::ProductKeyword($keyword);
			
			$q['keyword'] = $keyword;
		}

		if($service != ''){

		 	$filter = true;
		 	if(count($agents) > 0)
		 		$agents = $agents->ServiceName($service);
		 	else
		 		$agents = Agent::ServiceName($service);
			
			$q['service'] =  $service;
		}

		if($bank != ''){

		 	$filter = true;
		 	if(count($agents) > 0)
		 		$agents = $agents->BankName($bank);
		 	else
		 		$agents = Agent::BankName($bank);

		 	$q['bank'] = $bank;
		}

		if($location != ''){

		 	$filter = true;
		 	if(count($agents) > 0)
		 		$agents = $agents->LocationName($location);
		 	else
		 		$agents = Agent::LocationName($location);
			
			$q['location'] = $location;
		}


		if($rating != '')
		{	
		 	$filter = true;
		 	if(count($agents) > 0)
		 		$agents = $agents->UserRating($rating);
		 	else
		 		$agents = Agent::UserRating($rating);

		 	$q['rating'] = $rating;
		}

		if($filter)
		 	$products = $products->paginate($perPage);
		else
		{
			$products = Product::paginate($perPage);

			//dd(count($agents));
			
		} 	

/*		     $queries = DB::getQueryLog();
             $last_query = end($queries);
             Log::info($last_query);
*/
		 $message = "Successfully.";

		return Response::json(array(
		'error' => false,
		'result' => View::make('search._subproduct',compact('categories','products','q'))->render(),
		'message' => $message),200);
		//return View::make('search.subproduct',compact('categories','banks','products','q'));
	}

    

	public function showAgentSearch(){

		$perPage = 12;
		$filter = false;
		$q = array();


		$MainProductId = Input::get("primary",'');

		$SubProductId = Input::get("sub",'');

		$products = ProductCategory::all();
		
		$banks = Bank::all();

		$agents = array();


		if($MainProductId != '')
		{	
		 	$filter = true;
		 	if(count($agents) > 0)
		 		$agents = $agents->PrimaryProduct($MainProductId);
		 	else
		 		$agents = Agent::PrimaryProduct($MainProductId);
			
			$q['primary'] = $MainProductId;
		}


		if($SubProductId != '')
		{	
		 	$filter = true;
		 	if(count($agents) > 0)
		 		$agents = $agents->SubProduct($SubProductId);
		 	else
		 		$agents = Agent::SubProduct($SubProductId);
			
			$q['subproduct'] = $SubProductId;
		}
        
        
        if(Auth::check() && Auth::user()->isAgent()){
            
            if(count($agents) > 0)
                $agents = $agents->where('agents.id','<>', Auth::user()->agent->id);
            else
                $agents = Agent::where('agents.id','<>', Auth::user()->agent->id);
        }



		if($filter)
		 	$agents = $agents->paginate($perPage);
		else
		{
			$agents = Agent::paginate($perPage);
		} 	

/*		     $queries = DB::getQueryLog();
             $last_query = end($queries);
             Log::info($last_query);
*/
		return View::make('search.agent2',compact('products','banks','agents','MainProductId','SubProductId','q'));
	}



// 	public function showAgentSearch(){
//
// 		$perPage = 12;
// 		$filter = false;
// 		$q = array();

// 		$keyword = Input::get("keyword",'');

// 		$service = Input::get("service",'');
		
// 		$bank = Input::get("bank",'');
		
// 		$location = Input::get("location",'');
		
// 		$rating = Input::get('rating','');

// 		$products = ProductCategory::all();
		
// 		$banks = Bank::all();

// 		$agents = array();


// 		if($primary != '')
// 		{	
// 		 	$filter = true;
// 		 	if(count($agents) > 0)
// 		 		$agents = $agents->UserKeyword($keyword);
// 		 	else
// 		 		$agents = Agent::UserKeyword($keyword);
			
// 			$q['keyword'] = $keyword;
// 		}



		
// 		if($keyword != '')
// 		{	
// 		 	$filter = true;
// 		 	if(count($agents) > 0)
// 		 		$agents = $agents->UserKeyword($keyword);
// 		 	else
// 		 		$agents = Agent::UserKeyword($keyword);
			
// 			$q['keyword'] = $keyword;
// 		}

// 		if($service != ''){

// 		 	$filter = true;
// 		 	if(count($agents) > 0)
// 		 		$agents = $agents->ServiceName($service);
// 		 	else
// 		 		$agents = Agent::ServiceName($service);
			
// 			$q['service'] =  $service;
// 		}

// 		if($bank != ''){

// 		 	$filter = true;
// 		 	if(count($agents) > 0)
// 		 		$agents = $agents->BankName($bank);
// 		 	else
// 		 		$agents = Agent::BankName($bank);

// 		 	$q['bank'] = $bank;
// 		}

// 		if($location != ''){

// 		 	$filter = true;
// 		 	if(count($agents) > 0)
// 		 		$agents = $agents->LocationName($location);
// 		 	else
// 		 		$agents = Agent::LocationName($location);
			
// 			$q['location'] = $location;
// 		}


// 		if($rating != '')
// 		{	
// 		 	$filter = true;
// 		 	if(count($agents) > 0)
// 		 		$agents = $agents->UserRating($rating);
// 		 	else
// 		 		$agents = Agent::UserRating($rating);

// 		 	$q['rating'] = $rating;
// 		}

// 		if($filter)
// 		 	$agents = $agents->paginate($perPage);
// 		else
// 		{
// 			$agents = Agent::paginate($perPage);

// 			//dd(count($agents));
			
// 		} 	

// /*		     $queries = DB::getQueryLog();
//              $last_query = end($queries);
//              Log::info($last_query);
// */
// 		return View::make('search.agent2',compact('products','banks','agents','q'));
// 	}


	public function doAgentSearch(){


		$data = Input::all();
		$status = false;
		$message = '';
		$pageNumber = Input::get('page', 1);
		$perPage = 2;

		if(Request::ajax())
	    {

	    	$agentName = Input::get('agentName');

	    	$query = "
						select 
						u.id as UserId,
						p.id as ProfileId,
						p.first_name,
						p.last_name,
						p.name as AgentName,
						p.biography,
						p.photo_url,
						p.photointernal,
						b.name as BankName,
						l.address 
						from users as u 
						inner join agents as a on u.id = a.user_id
						left join profiles as p on u.id = p.user_id
						left join banks as b on a.bank_id = b.id
						left join locations as l on l.user_id = u.id
						where 
						p.name like '$agentName%'";

			$results = DB::select(DB::raw($query));


			$slice = array_slice($results, $perPage * ($pageNumber - 1), $perPage);
			$results = Paginator::make($slice, count($results), $perPage);

	    	$message = "Successfully.";

	    	return Response::json(array(
			        'error' => false,
			        'result' => View::make('result',compact('results'))->render(),
			        'message' => $message),200);
	  
		}else
		{
			$message = "Search Failed.";

			return Response::json(array(
			        'error' => true,
			        'message' => $message), 200);
		}
		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

	}



}
