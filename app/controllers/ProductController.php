<?php

class ProductController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	
	public function showRecommend(){

		$user = Auth::user();
		
		try{

			$input = Input::all();

			$productids = Input::get("products");

			$products = Product::whereIn('id',$productids)->get();

			
			if(Request::ajax())
	    	{
		    	$message = "Successfully.";

		    	return Response::json(array(
				        'error' => false,
				        'result' => View::make('shared._recommend',compact('products'))->render(),
				        'message' => $message),200);
	    	}
		    else
		    {
		    	return View::make('shared._recommend',compact('products'));
		    }
		   
		}
	    catch(Exception $e)
		{
			Log::info($e->getMessage());

			$message = $e->getMessage();

			return Response::json(array(
			        'error' => true,
			        'result' => null,
			        'message' => $message),200);
		}

	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$user = Auth::user();
		$productcategory = ProductCategory::orderBy('name','asc')->lists('name', 'id');
		$product = new Product;
		if(Request::ajax())
	    {
	    	$agent = $user->agent;

	    	$message = "Successfully.";

	    	return Response::json(array(
			        'error' => false,
			        'result' => View::make('product.create',compact('product','agent','productcategory'))->render(),
			        'message' => $message),200);
	    }
	    else{
	    	return View::make('product.create');
	    }
		
	}


	public function showProduct(){

        $MainProductId = Input::get('primary','');
        $SubProductId = Input::get('sub','');
        
        if($MainProductId != '' && $SubProductId != ''){
            $product = Product::whereUid($SubProductId)->first();
            return View::make('product.show',compact('product','MainProductId','SubProductId'));
        }else
        {
             return  Redirect::to('search/product');
        }
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = Auth::user();
		$productcategory = ProductCategory::orderBy('name','asc')->lists('name', 'id');
		$product = Product::find($id);
		if(Request::ajax())
	    {
	    	$agent = $user->agent;

	    	$message = "Successfully.";

	    	return Response::json(array(
			        'error' => false,
			        'result' => View::make('product.edit',compact('product','agent','productcategory'))->render(),
			        'message' => $message),200);
	    }
	    else{
	    	return View::make('product.edit');
	    }
	}




	public function addProduct($id = 0){
		

		$messages = array(
        'productname.required'=>'Please enter product name.',
        'description.required' => 'Please enter description.',
        'categories.required'=>'Please choose category.'
	    );

		
		$rules = array(
			'productname'    => 'required',
			'description' => 'required',
			'categories' => 'required'
		);

		$agent = Agent::find($id);

		try{
			if($id != 0 && $agent != null){

				$product = new Product;

				$input = Input::all();

				$validation = Validator::make($input, $rules,$messages);

				if ($validation->passes())
		        {

					$product->storeAgentProduct($id,
						Input::get('productname'),
						Input::get('description'),
						Input::get('categories',null),
						Input::get('primary',false),   
						Input::file('productimage'));

					return Redirect::route('profile.show', array('profile' => $agent->user->profile->uid)); 
				}
				else
				{
					
					return Redirect::route('profile.show', array('profile' => $agent->user->profile->uid))
		            ->withInput()
		            ->withErrors($validation)
		            ->with('message', 'There were validation errors.');
				}
			}
			else{

			}
		}catch(Exception $e){
			Log::info($e->getMessage());
		}

		
	}

		/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//

//		dd(Input::all());
		$messages = array(
        'productname.required'=>'Please enter product name.',
        'description.required' => 'Please enter description.',
        'categories.required'=>'Please choose category.'
	    );

		
		$rules = array(
			'productname'    => 'required',
			'description' => 'required',
			'categories' => 'required'
		);

		try{

			$input = Input::all();

			$validation = Validator::make($input, $rules,$messages);

			$product = Product::find($id);

			//dd($validation->messages());

			if ($validation->passes())
	        {
			


				$product->storeAgentProduct($product->agent->id,
											Input::get('productname',null),
											Input::get('description',null),
											Input::get('categories',null),
											Input::get('primary',false),   
											Input::file('productimage'));

				return Redirect::route('profile.show', array('profile' => $product->agent->user->profile->id)); 
			}
			else
			{
				
				return Redirect::route('profile.show', array('profile' => $product->agent->user->profile->id))
	            ->withInput()
	            ->withErrors($validation)
	            ->with('message', 'There were validation errors.');
			}


		}catch(Exception $e){


				return Redirect::to('error')
	            ->with('message', $e->getMessage());
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//

		try{
			$product = Product::find($id);

			$profileId = $product->agent->user->profile->id;
		   	
		   	if($product->isProductOwner()){		
					$product->delete();	
		   	}else{
		   		return Redirect::to('error')
	            ->with('message', "Unauthorize access.");
		   	}

		   	return Redirect::route('profile.show', array('profile' => $profileId)); 

	   	}catch(Exception $e){


				return Redirect::to('error')
	            ->with('message', $e->getMessage());
		}
	}



}
