<?php

class CalculatorController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function showCalculator()
	{
		//

		
	}
    
    
    public function doCalculation(){
        $score = null;
        $message = '';
        
		try{	
            
            $productCategory = Input::get("productCategories");
            $monthlyincome = floatval(str_replace(",","",Input::get("MonthlyIncome")));
			$loantenure = Input::get("LoanTenure");
			$interestrate = Input::get("InterestRate");
			$totalhousefinance = floatval(str_replace(",","",Input::get("TotalHouseFinance")));
			$totalvehiclefinance = floatval(str_replace(",","",Input::get("TotalVehicleFinance")));
			$totalpersonalfinance = floatval(str_replace(",","",Input::get("TotalPersonalFinance")));
			$totalcreditcardfinance = floatval(str_replace(",","",Input::get("TotalCreditCard")));
            
            //do calculation here
            //call web api
          
            $score = new UserScore;

            $score->monthlyrepayment = 999.90;
            $score->maximumloan = 999.00;
            $score->maximummonthlyrepayment = 999.00;

            $message = "Successfully.";
            $products = Product::where('id','>',0)->lists('id');
            
             return Response::json(array(
				        'error' => false,
				        'result' => array( 'success' => true, 'products' => $products, 'count' => count($products) ,'score' => $score),
				        'message' => $message),200);
        }
		catch(Exception $e)
		{
				Log::info($e->getMessage());

				$message = $e->getMessage();

				return Response::json(array(
				        'error' => true,
				        'result' => null,
				        'message' => $message),200);
		}
    }
    
    
	public function doSaveCalculation()
	{
		//
		//$user = Auth::user();
        $score = null;
        
		try{
            
			$input = Input::all();        

			$productCategory = Input::get("productCategories");
			$monthlyincome = floatval(str_replace(",","",Input::get("MonthlyIncome")));
			$loantenure = Input::get("LoanTenure");
			$interestrate = Input::get("InterestRate");
			$totalhousefinance = floatval(str_replace(",","",Input::get("TotalHouseFinance")));
			$totalvehiclefinance = floatval(str_replace(",","",Input::get("TotalVehicleFinance")));
			$totalpersonalfinance = floatval(str_replace(",","",Input::get("TotalPersonalFinance")));
			$totalcreditcardfinance = floatval(str_replace(",","",Input::get("TotalCreditCard")));
            
            $monthlyRepayment = floatval(str_replace(",","",Input::get("MonthlyRepayment")));
			$maximumLoan = floatval(str_replace(",","",Input::get("MaximumLoan")));
			$maximumMonthlyRepayment = floatval(str_replace(",","",Input::get("MaximumMonthlyRepayment")));
            
            $userName = Auth::user()->profile->getDisplayName();
			$status = true;
            
            
            $appCode = Input::get("appCode",'');
            
            $application = Application::whereCode($appCode)->first();
            
            $args = array( 
                'productCategoryId' => $productCategory,
                'monthlyincome' => $monthlyincome,
                'loantenure' => $loantenure,
                'interestrateId' => $interestrate,
                'totalhousefinance' =>$totalhousefinance,
                'totalvehiclefinance'=>$totalvehiclefinance,
                'totalpersonalfinance'=>$totalpersonalfinance,
                'totalcreditcardfinance'=>$totalcreditcardfinance,
                'monthlyrepayment'=>$monthlyRepayment,
                'maximumloan'=>$maximumLoan,
                'maximummonthlyrepayment'=>$maximumMonthlyRepayment,
                'user'=>$userName,
                'status'=> $status);
            
            if($application != null){
                
                if($monthlyRepayment > 0 && $maximumLoan > 0 && $maximumMonthlyRepayment> 0){
                    $score = $application->getUserScore();
                    $score->saveUserScore($args);

                    if(!$application->hasUserScore()){

                         $application->userscore_id = $score->id;
                         $application->save();
                    }

                    $message = "Saved.";
                }else{
                    $message = "Calculation Failed.";
                }
                
            }else{
                $message = "No application.Cannot save.";
            }

		    return Response::json(array(
				        'error' => false,
				        'result' => array( 'success' => true),
				        'message' => $message),200);
		}
		catch(Exception $e)
		{
				Log::info($e->getMessage());

				$message = $e->getMessage();

				return Response::json(array(
				        'error' => true,
				        'result' => null,
				        'message' => $message),200);
		}
	}



}
