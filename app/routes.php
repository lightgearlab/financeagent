<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/updateapp', function()
{
    exec('composer dump-autoload');
    echo 'composer dump-autoload complete';
});


Route::get('/myapp/install/{key?}',  array('as' => 'install', function($key = null)
{
    if($key == "KR6jq0si2Pjl85xNEDR5qT3gtemxLthC"){
    try {
      echo '<br>init migrate:install...';
      Artisan::call('migrate:install');
      echo 'done migrate:install';
      
      echo '<br>init with Sentry tables migrations...';
      Artisan::call('migrate', array(
        '--package'=>'cartalyst/sentry',
        '--force' => true
        ));
      echo 'done with Sentry';
      echo '<br>init with app tables migrations...';
      Artisan::call('migrate', array(
        '--path'     => "app/database/migrations",
        '--force' => true
        ));
      echo '<br>done with app tables migrations';
      echo '<br>init with Sentry tables seader...';
      Artisan::call('db:seed', array(
        '--force' => true
        ));
      echo '<br>done with Sentry tables seader';
    } catch (Exception $e) {
      Log::info($e->getMessage());
      Response::make($e->getMessage(), 500);
    }
  }else{
    App::abort(404);
  }
}
));


Route::get('/', 'HomeController@showMain');


Route::get('/login', 'AuthController@showLogin');

Route::post('/login', 'AuthController@doLogin');

Route::get('/logout', 'AuthController@doLogout');

Route::get('/register', 'AuthController@showRegister');

Route::post('/register', 'AuthController@doRegister');


Route::get('/agent/login', 'AuthController@showAgentLogin');

Route::post('/agent/login', 'AuthController@doLogin');

Route::get('/agent/register', 'AuthController@showAgentRegister');

Route::post('/agent/register', 'AuthController@doAgentRegister');


Route::post('/agent/remove/{id}', 'ReserveController@doRemove');

Route::resource('/home', 'HomeController');

Route::resource('profile', 'ProfileController');


Route::post('profile/updateProfile/{id}', 'ProfileController@updateProfile');

Route::get('/profile/message/{id}', 'ProfileController@showMessage');

Route::post('/profile/message/{id}', 'ProfileController@doMessage');

Route::post('/review/{id}', 'ReviewController@addReview');

Route::get('/review/{id}', 'ReviewController@showList');

Route::post('/deleteReview/{id}', 'ReviewController@destroy');


//product page

//Route::resource('product', 'ProductController');

Route::get('/product', ['as' => 'product.show', 'uses' => 'ProductController@showProduct']);
Route::post('/{id}', 'ProductController@addProduct');


Route::resource('messages', 'MessagesController');

Route::get('/conversations','ConversationController@showConversations');

Route::get('/conversation/{id}','ConversationController@showMessages');

Route::get('/conversation/participant/{id}','ConversationController@showParticipant');

Route::post('/conversation/message/{id}','ConversationController@doSendMessage');

// Route::get('/conversations',function(){

//    $conversations = Auth::user()->conversations;

//    dd(count($conversations));
// });


Route::group(array('prefix' => 'manage','before' => 'manage'), function () {

	/*Route::get('/category','AdminController@showProductCategoryList');
	Route::get('/category/{id}','AdminController@showProductCategory');
	Route::get('/category/{id}/edit','AdminController@editProductCategory');*/

  Route::get('/','AdminController@index');
  
	Route::resource('category', 'ProductCategoryController');

	Route::resource('user', 'UserController');

  Route::resource('bank', 'BankController');


  Route::post('/user/remove/{id}', 'UserController@destroy');

  Route::post('/bank/remove/{id}', 'BankController@destroy');

  Route::post('/category/remove/{id}', 'ProductCategoryController@destroy');

});

Route::get('/email/welcome',function(){

  return View::make("emails.welcome");
});

Route::get('/error','HomeController@showError');

Route::get('/demo',function(){

    $productCategories =  ProductCategory::orderBy('name','asc')->lists('name', 'id');

    return View::make("product.demo",compact('productCategories'));

});


//search page
Route::get('/search/product','SearchController@showProductPage');
Route::get('/search/product/main','SearchController@showMainProduct');
Route::get('/search/product/sub','SearchController@showSubProduct');
Route::get('/search/agent','SearchController@showAgentSearch');
Route::post('/search/agent', 'SearchController@doAgentSearch');


//check if user authenticate 
Route::group(array('before' => 'auth'),function(){
    

    Route::get('/application','ApplicationController@showApplication');
    Route::get('/application/document','ApplicationController@showDocuments');
    Route::get('/application/product','ApplicationController@showProducts');  
    Route::get('/application/product/recommend','ApplicationController@showRecommend');  
    Route::get('/application/submission','ApplicationController@showSubmission');  
    
    Route::get('/saved/product', ['as' => 'product.saved', 'uses' => 'ReserveController@showProductReserve']);
    Route::get('/saved/agent', 'ReserveController@showAgentReserve');
});

Route::group(array('before' => 'auth|csrf'), function () {

    Route::post('/score/calculate', 'CalculatorController@doCalculation');
    Route::post('/score/calculate/save', 'CalculatorController@doSaveCalculation');

    Route::post('/product/saved/add', 'ReserveController@doProductSave');
    Route::post('/agent/saved/add/{id}', 'ReserveController@doAgentSave');
    
    Route::post('/product/saved/remove/{id}', 'ReserveController@doProductSavedRemove');
    Route::post('/agent/saved/remove/{id}', 'ReserveController@doAgentSavedRemove');

    Route::post('/application/apply', 'ApplicationController@doApplyAgent');
    Route::post('/application/customer', 'ApplicationController@updateCustomer');
    Route::post('/application/document/upload', 'ApplicationController@doUploadDocument');
    Route::post('/application/product/add', 'ApplicationController@doAddApplicationProduct');
    Route::post('/application/product/remove', 'ApplicationController@doRemoveApplicationProduct');
    Route::post('/application/submit', 'ApplicationController@doSubmitApplication');
    
    Route::post('/profile/updatephoto', 'ProfileController@uploadProfileImg');
    Route::post('/profile/addproduct', 'ProfileController@addProduct');
    Route::post('/profile/agent/removeproduct/{id}', 'ProfileController@removeProduct');
   

});

  Route::get('/saved', 'ReserveController@showSavedList');

  Route::get('/profile/agentproducts/{id}', 'ProfileController@getProductList');


  Route::get('/test',function(){

     $application = Application::whereCode('Jvxpth6716wQ1Ry2G46gvkOUto9mn8')->first();
     dd($application->AgentAvailableProduct()->first());
      //dd(DB::getQueryLog());
  });


