

$(function(){



	 // (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	 // (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	 // m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	 // })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	 
	 // ga('create', 'UA-71798179-1', 'auto');
	 // ga('send', 'pageview');
	 
	 //set default ajax call header token
	 
	 $('.show-password').click(function(){

	 	var value = $(this).data('value');

	 	if(value == 'show'){
	 		$(this).prev('input').attr('type', 'text');
	 		$(this).data('value','hide');
	 	}else {

	 		$(this).prev('input').attr('type', 'password');
	 		$(this).data('value','show');
	 	}
	 	

	 });

});


    $('.currency').change(function(e){
        
        var value = 0;
        
        if($(this) != null)
            value = numeral($(this).val());
        
        $(this).val(value.format('0,0.00')); 
    });


 function displayUserRating(intRating){
	 	$('.user-rating').empty();

	 	var max = 5;
	 	var min = Math.floor(intRating);
	 	var blnHalf = true;
	 	var strHtml = '';

	 	for(var i = 0;i < max;i++){

	 		if(min > 0)
	 			strHtml += '<i class="fa fa-star star-yellow-color" aria-hidden="true"></i>';
	 		else{

	 			if(intRating % 1 != 0 && blnHalf){
					strHtml += '<i class="fa fa-star-half-o star-yellow-color" aria-hidden="true"></i>';
					blnHalf = false;
	 			}
	 
	 			else
	 				strHtml += '<i class="fa fa-star" aria-hidden="true"></i>';
	 		}
	 			

	 		min--;
	 	}

	 	$('.user-rating').append(strHtml);
	 }